# Shopify Simplee Membership App

### Installation
Install the dependencies and devDependencies.

For both environment
```sh
$ composer install
$ cp .env.example .env 
$ nano .env // set all credentials(ex: database, shopify api key and secret, mail credentials)
$ php artisan migrate
```

For development environments...

```sh
$ npm install
$ npm run dev
```
For production environments...

```sh
$ npm install --production
$ npm run prod
```
create superviser

Extra commands

```sh
$ php artisan db:seed
$ php artisan storage:link
$ sudo supervisorctl reread && sudo supervisorctl update && sudo supervisorctl restart [superviser-name]
```
### Used Shopify Tools

* Admin rest-api
* Shopify GraphQL api  
* App-bridge

### Installation with Lando (Docker Wrapper)

Requirements
  - macOS 10.13 or later
  - Windows 10 Pro+ or equivalent (eg Windows 10 Enterprise) with Hyper-V running
  - Linux with kernel version 4.x or higher
  - Docker Engine (Docker for Windows, Mac or Linux)
  - [Lando](https://docs.lando.dev/basics/installation.html) 

Getting Started

First step is to enable the .env file. For Lando, you simply should be able to copy .env.lando.example to .env and not
need to change any of the settings.

    cp .env.example .env
  
Setup env variables for database:
  
    DB_CONNECTION=mysql
    DB_HOST=database
    DB_PORT=3306
    DB_DATABASE=subscriptions
    DB_USERNAME=laravel
    DB_PASSWORD=laravel

To get started, from the project root, run:

    lando start

The first run will take some time as all the containers are downloaded and configured. Once the containers are up, you
can use the Lando CLI as a shortcut to run the project tooling:

    lando composer install
    lando npm install
    lando npm run dev
    lando artisan migrate
    lando artisan db:seed

To reset the db completely and reseed it:

    lando artisan migrate:fresh --seed

A fully clean installation flow would include:

    lando composer update --no-interaction --prefer-dist --optimize-autoloader
    lando artisan clear-compiled
    lando artisan migrate:fresh --seed 
    lando artisan cache:clear
    lando artisan route:clear
    lando artisan view:clear
    lando artisan config:clear
    lando artisan config:cache
    lando npm install

For hot reloading working with a JS framework, run:

    lando npm run watch

Update your App's base URL to:

    https://simplee-subscriptions.lndo.site/
