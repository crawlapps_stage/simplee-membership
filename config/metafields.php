<?php

return [
  'options' => [
    [
      'namespace' => 'simplee',
      'key' => 'widget_active_bg',
      'value' => '#1473E6',
      'value_type' => 'string'
    ],
    [
      'namespace' => 'simplee',
      'key' => 'widget_inactive_bg',
      'value' => '#FAFAFA',
      'value_type' => 'string'
    ],
    [
      'namespace' => 'simplee',
      'key' => 'widget_active_text',
      'value' => '#FFFFFF',
      'value_type' => 'string'
    ],
    [
      'namespace' => 'simplee',
      'key' => 'widget_inactive_text',
      'value' => '#000000',
      'value_type' => 'string'
    ],
    [
      'namespace' => 'simplee',
      'key' => 'widget_one_time_text',
      'value' => 'One-Time Purchase',
      'value_type' => 'string'
    ],
    [
      'namespace' => 'simplee',
      'key' => 'widget_heading_text',
      'value' => 'Purchase Options',
      'value_type' => 'string'
    ],
    [
      'namespace' => 'simplee',
      'key' => 'widget_default_selection',
      'value' => 'One-Time Purchase',
      'value_type' => 'string'
    ]
  ]
];
