<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Global constants
    |--------------------------------------------------------------------------
    */

    'HMAC_KEY'  => 'MRi-s7dL32W5neMvbVnp7rP5MAwS2AC6eHholWVu',

    'gtm_tag' => env('GTM_TAG', 'GTM-TGMRHPM'),

    'THEME' => [
        'EXPRESS' => [
            '1_9_1' => [
                'CART_FIND' => 'selling_plan.name',
                'ACCOUNT_PAGE_URL' => '<a href="/tools/portal" class="button simplee_express_msl">My Subscriptions</a>',
                'ACCOUNT_PAGE_PLACE' => '{{ customer.default_address | format_address }}',
                'PRODUCT_PAGE_PLACE' => '<div class="product-form__buttons">',
                'PRODUCT_FILE' => 'snippets/product-form.liquid',
                'PRICE_SALE' => '.price-item--regular',
                'PRICE_BADGE_SALE' => '.data-subscription-badge',
                'LIQUID_PLACE' => '{%- liquid'
            ],
            '*' => [
                'CART_FIND' => 'selling_plan.name',
                'ACCOUNT_PAGE_URL' => '<a href="/tools/portal" class="button simplee_express_msl">My Subscriptions</a>',
                'ACCOUNT_PAGE_PLACE' => '{{ customer.default_address | format_address }}',
                'PRODUCT_PAGE_PLACE' => '<div class="product-form__buttons">',
                'PRODUCT_FILE' => 'snippets/product-form.liquid',
                'PRICE_SALE' => '.price-item--regular',
                'PRICE_BADGE_SALE' => '.data-subscription-badge',
                'LIQUID_PLACE' => '{%- liquid'
            ],
        ],
        'MINIMAL' => [
            '12.2.0' => [
                'CART_FIND' => 'selling_plan.name',
                'ACCOUNT_PAGE_URL' => '<a href="/tools/portal" class="simplee_msl_box">My Subscriptions</a>',
                'ACCOUNT_PAGE_PLACE' => '{{ customer.default_address | format_address }}',
                'PRODUCT_PAGE_PLACE' => '<button type="submit" name="add" id="AddToCart"',
                'PRODUCT_FILE' => 'sections/product-template.liquid',
                'PRICE_SALE' => '.product-single__price',
                'PRICE_BADGE_SALE' => '.data-subscription-badge',
                'LIQUID_PLACE' => '{%- liquid'
            ],
            '*' => [
                'CART_FIND' => 'selling_plan.name',
                'ACCOUNT_PAGE_URL' => '<a href="/tools/portal" class="simplee_msl_box">My Subscriptions</a>',
                'ACCOUNT_PAGE_PLACE' => '{{ customer.default_address | format_address }}',
                'PRODUCT_PAGE_PLACE' => '<button type="submit" name="add" id="AddToCart"',
                'PRODUCT_FILE' => 'sections/product-template.liquid',
                'PRICE_SALE' => '.product-single__price',
                'PRICE_BADGE_SALE' => '.data-subscription-badge',
                'LIQUID_PLACE' => '{%- liquid'
            ],
        ],
        'BROOKLYN' => [
            '17.2.0' => [
                'CART_FIND' => 'selling_plan.name',
                'ACCOUNT_PAGE_URL' => '<a href="/tools/portal" class="text-link simplee_msl_box">My Subscriptions</a>',
                'ACCOUNT_PAGE_PLACE' => '{{ customer.default_address | format_address }}',
                'PRODUCT_PAGE_PLACE' => '<div class="product-single__add-to-cart',
                'PRODUCT_FILE' => 'sections/product-template.liquid',
                'PRICE_SALE' => '.product-single__price',
                'PRICE_BADGE_SALE' => '.data-subscription-badge',
                'LIQUID_PLACE' => '{%- liquid'
            ],
            '*' => [
                'CART_FIND' => 'selling_plan.name',
                'ACCOUNT_PAGE_URL' => '<a href="/tools/portal" class="text-link simplee_msl_box">My Subscriptions</a>',
                'ACCOUNT_PAGE_PLACE' => '{{ customer.default_address | format_address }}',
                'PRODUCT_PAGE_PLACE' => '<div class="product-single__add-to-cart',
                'PRODUCT_FILE' => 'sections/product-template.liquid',
                'PRICE_SALE' => '.product-single__price',
                'PRICE_BADGE_SALE' => '.data-subscription-badge',
                'LIQUID_PLACE' => '{%- liquid'
            ],
        ],
        'NARRATIVE' => [
            '10.2.1' => [
                'CART_FIND' => 'selling_plan.name',
                'ACCOUNT_PAGE_URL' => '<a href="/tools/portal" class="btn-link simplee_msl_box">My Subscriptions</a>',
                'ACCOUNT_PAGE_PLACE' => '{{ customer.default_address | format_address }}',
                'PRODUCT_PAGE_PLACE' => '<button class="btn btn--to-secondary btn--full product__add-to-cart-button',
                'PRODUCT_FILE' => 'snippets/product-form.liquid',
                'PRICE_SALE' => '.product__current-price',
                'PRICE_BADGE_SALE' => '.data-subscription-badge',
                'LIQUID_PLACE' => '{%- liquid'
            ],
            '*' => [
                'CART_FIND' => 'selling_plan.name',
                'ACCOUNT_PAGE_URL' => '<a href="/tools/portal" class="btn-link simplee_msl_box">My Subscriptions</a>',
                'ACCOUNT_PAGE_PLACE' => '{{ customer.default_address | format_address }}',
                'PRODUCT_PAGE_PLACE' => '<button class="btn btn--to-secondary btn--full product__add-to-cart-button',
                'PRODUCT_FILE' => 'snippets/product-form.liquid',
                'PRICE_SALE' => '.product__current-price',
                'PRICE_BADGE_SALE' => '.data-subscription-badge',
                'LIQUID_PLACE' => '{%- liquid'
            ],
        ],
        'SUPPLY' => [
            '10.2.1' => [
                'CART_FIND' => 'selling_plan.name',
                'ACCOUNT_PAGE_URL' => '<a href="/tools/portal" class="simplee_msl_box">My Subscriptions</a>',
                'ACCOUNT_PAGE_PLACE' => '<p><a href="{{ routes.account_addresses_url }}">{{ \'customer.account.view_addresses\' | t }} ({{ customer.addresses_count }})</a></p>',
                'PRODUCT_PAGE_PLACE' => '<div class="payment-buttons payment-buttons--{{ section.settings.add_to_cart_button_size }}">',
                'PRODUCT_FILE' => 'sections/product-template.liquid',
                'PRICE_SALE' => '#productPrice-product-template',
                'PRICE_BADGE_SALE' => '.data-subscription-badge',
                'LIQUID_PLACE' => '{%- liquid'
            ],
            '*' => [
                'CART_FIND' => 'selling_plan.name',
                'ACCOUNT_PAGE_URL' => '<a href="/tools/portal" class="simplee_msl_box">My Subscriptions</a>',
                'ACCOUNT_PAGE_PLACE' => '<p><a href="{{ routes.account_addresses_url }}">{{ \'customer.account.view_addresses\' | t }} ({{ customer.addresses_count }})</a></p>',
                'PRODUCT_PAGE_PLACE' => '<div class="payment-buttons payment-buttons--{{ section.settings.add_to_cart_button_size }}">',
                'PRODUCT_FILE' => 'sections/product-template.liquid',
                'PRICE_SALE' => '#productPrice-product-template',
                'PRICE_BADGE_SALE' => '.data-subscription-badge',
                'LIQUID_PLACE' => '{%- liquid'
            ],
        ],
        'VENTURE' => [
            '12.2.0' => [
                'CART_FIND' => 'selling_plan.name',
                'ACCOUNT_PAGE_URL' => '<a href="/tools/portal" class="simplee_msl_box">My Subscriptions</a>',
                'ACCOUNT_PAGE_PLACE' => '<p><a href="{{ routes.account_addresses_url }}">{{ \'customer.account.view_addresses\' | t }} ({{ customer.addresses_count }})</a></p>',
                'PRODUCT_PAGE_PLACE' => '<div class="product-form__item product-form__item--submit">',
                'PRODUCT_FILE' => 'sections/product-template.liquid',
                'PRICE_SALE' => '.product-single__price',
                'PRICE_BADGE_SALE' => '.data-subscription-badge',
                'LIQUID_PLACE' => '{%- liquid'
            ],
            '*' => [
                'CART_FIND' => 'selling_plan.name',
                'ACCOUNT_PAGE_URL' => '<a href="/tools/portal" class="simplee_msl_box">My Subscriptions</a>',
                'ACCOUNT_PAGE_PLACE' => '<p><a href="{{ routes.account_addresses_url }}">{{ \'customer.account.view_addresses\' | t }} ({{ customer.addresses_count }})</a></p>',
                'PRODUCT_PAGE_PLACE' => '<div class="product-form__item product-form__item--submit">',
                'PRODUCT_FILE' => 'sections/product-template.liquid',
                'PRICE_SALE' => '.product-single__price',
                'PRICE_BADGE_SALE' => '.data-subscription-badge',
                'LIQUID_PLACE' => '{%- liquid'
            ],
        ],
        'DEBUT' => [
            '17.8.0' => [
                'CART_FIND' => 'selling_plan.name',
                'ACCOUNT_PAGE_URL' => '<a href="/tools/portal" class="simplee_msl_box">My Subscriptions</a>',
                'ACCOUNT_PAGE_PLACE' => '<p><a href="{{ routes.account_addresses_url }}" class="btn btn--small">{{ \'customer.account.view_addresses\' | t }} ({{ customer.addresses_count }})</a></p>',
                'PRODUCT_PAGE_PLACE' => '<div class="product-form__error-message-wrapper product-form__error-message-wrapper--hidden',
                'PRODUCT_FILE' => 'sections/product-template.liquid',
                'PRICE_SALE' => '.price-item',
                'PRICE_BADGE_SALE' => '.data-subscription-badge',
                'LIQUID_PLACE' => '{%- liquid'
            ],
            '*' => [
                'CART_FIND' => 'selling_plan.name',
                'ACCOUNT_PAGE_URL' => '<a href="/tools/portal" class="simplee_msl_box">My Subscriptions</a>',
                'ACCOUNT_PAGE_PLACE' => '<p><a href="{{ routes.account_addresses_url }}" class="btn btn--small">{{ \'customer.account.view_addresses\' | t }} ({{ customer.addresses_count }})</a></p>',
                'PRODUCT_PAGE_PLACE' => '<div class="product-form__error-message-wrapper product-form__error-message-wrapper--hidden',
                'PRODUCT_FILE' => 'sections/product-template.liquid',
                'PRICE_SALE' => '.price-item',
                'PRICE_BADGE_SALE' => '.data-subscription-badge',
                'LIQUID_PLACE' => '{%- liquid'
            ],
        ],
        'SIMPLE' => [
            '12.5.1' => [
                'CART_FIND' => 'selling_plan.name',
                'ACCOUNT_PAGE_URL' => '<a href="/tools/portal" class="simplee_msl_box">My Subscriptions</a>',
                'ACCOUNT_PAGE_PLACE' => '<p><a href="{{ routes.account_addresses_url }}" class="btn btn--small">{{ \'customer.account.view_addresses\' | t }} ({{ customer.addresses_count }})</a></p>',
                'PRODUCT_PAGE_PLACE' => '<div class="product-single__cart-submit-wrapper',
                'PRODUCT_FILE' => 'sections/product-template.liquid',
                'PRICE_SALE' => '.price-item',
                'PRICE_BADGE_SALE' => '.data-subscription-badge',
                'LIQUID_PLACE' => '{%- liquid'
            ],
            '*' => [
                'CART_FIND' => 'selling_plan.name',
                'ACCOUNT_PAGE_URL' => '<a href="/tools/portal" class="simplee_msl_box">My Subscriptions</a>',
                'ACCOUNT_PAGE_PLACE' => '<p><a href="{{ routes.account_addresses_url }}" class="btn btn--small">{{ \'customer.account.view_addresses\' | t }} ({{ customer.addresses_count }})</a></p>',
                'PRODUCT_PAGE_PLACE' => '<div class="product-single__cart-submit-wrapper',
                'PRODUCT_FILE' => 'sections/product-template.liquid',
                'PRICE_SALE' => '.product-single__price',
                'PRICE_BADGE_SALE' => '.data-subscription-badge',
                'LIQUID_PLACE' => '{%- liquid'
            ],
        ],
        'BOUNDLESS' => [
            '10.2.3' => [
                'CART_FIND' => 'selling_plan.name',
                'ACCOUNT_PAGE_URL' => '<a href="/tools/portal" class="simplee_msl_box">My Subscriptions</a>',
                'ACCOUNT_PAGE_PLACE' => '<p><a href="{{ routes.account_addresses_url }}" class="btn btn--small">{{ \'customer.account.view_addresses\' | t }} ({{ customer.addresses_count }})</a></p>',
                'PRODUCT_PAGE_PLACE' => '{% if product.available %}',
                'PRODUCT_FILE' => 'sections/product-template.liquid',
                'PRICE_SALE' => '.product__price--reg',
                'PRICE_BADGE_SALE' => '.data-subscription-badge',
                'LIQUID_PLACE' => '{%- liquid'
            ],
            '*' => [
                'CART_FIND' => 'selling_plan.name',
                'ACCOUNT_PAGE_URL' => '<a href="/tools/portal" class="simplee_msl_box">My Subscriptions</a>',
                'ACCOUNT_PAGE_PLACE' => '<p><a href="{{ routes.account_addresses_url }}" class="btn btn--small">{{ \'customer.account.view_addresses\' | t }} ({{ customer.addresses_count }})</a></p>',
                'PRODUCT_PAGE_PLACE' => '{% if product.available %}',
                'PRODUCT_FILE' => 'sections/product-template.liquid',
                'PRICE_SALE' => '.product__price--reg',
                'PRICE_BADGE_SALE' => '.data-subscription-badge',
                'LIQUID_PLACE' => '{%- liquid'
            ],
        ],
        '*' => [
            'CART_FIND' => 'selling_plan.name',
            'ACCOUNT_PAGE_URL' => '<a href="/tools/portal" class="simplee_msl_box">My Subscriptions</a>',
            'ACCOUNT_PAGE_PLACE' => '{{ customer.default_address | format_address }}',
            'PRODUCT_PAGE_PLACE' => '<div class="product-form__error-message-wrapper product-form__error-message-wrapper--hidden',
            'PRODUCT_FILE' => 'sections/product-template.liquid',
            'PRICE_SALE' => '.price-item--sale',
            'PRICE_BADGE_SALE' => '.price__badge--sale',
            'PRICE_FILE' => 'snippets/product-price.liquid',
            'PRICE_SALE' => '.product__price--reg',
            'PRICE_BADGE_SALE' => '.data-subscription-badge',
            'LIQUID_PLACE' => '{%- liquid'
        ]
    ],

    'FILES' => [
        'PRODUCT' => 'sections/product-template.liquid',
        'CART' => 'templates/cart.liquid',
        'ACCOUNT' => 'templates/customers/account.liquid',
        'THEME' => 'layout/theme.liquid'
    ],

    'SNIPPETS' => [
        'SIMPLEE' => 'simplee',
        'SIMPLEE_WIDGET' => 'simplee-widget',
        'CART' => 'simplee-cart',
    ],

    'ASSETS' => [
        'CSS' => 'simplee',
        'JS' => 'simplee',
    ]

];
