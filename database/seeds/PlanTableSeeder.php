<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plans')->insert([
            [
                'id' => 1,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "Starter Plan"),
                'price' => env('PLAN_PRICE_1', 19.99),
                'interval' => 'EVERY_30_DAYS',
                'capped_amount' => 500.00,
                'terms' => env('PLAN_TERM_1', "Starter Plan"),
                'trial_days' => env('TRIAL_DAY', 7),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1),
                'transaction_fee' => 0.0015
            ],
            [
                'id' => 2,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "Enterprise Plan"),
                'price' => env('PLAN_PRICE_1', 299.99),
                'interval' => 'EVERY_30_DAYS',
                'capped_amount' => 1000.00,
                'terms' => env('PLAN_TERM_1', "Enterprise Plan"),
                'trial_days' => env('TRIAL_DAY', 7),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1),
                'transaction_fee' => 0.0005
            ],
        ]);
    }
}
