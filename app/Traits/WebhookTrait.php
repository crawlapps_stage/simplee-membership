<?php

namespace App\Traits;

use App\Events\CheckBillingAttemptFailure;
use App\Events\CheckBillingAttemptSuccess;
use App\Events\CheckCustomerPaymentMethodUpdate;
use App\Events\CheckProductUpdate;
use App\Events\CheckSubscriptionContract;
use App\Models\Shop;
use App\Models\SsCustomer;
use App\Models\SsContractLineItem;
use App\Models\SsDeletedProduct;
use App\Models\SsShippingProfile;
use App\User;

/**
 * Trait WebhookTrait
 * @package App\Traits
 */

use App\Traits\ShopifyTrait;
Trait WebhookTrait{
    use ShopifyTrait;
    public function webhookIndex($request){
        $requestData = $request->json()->all();

        $data = $requestData['detail'];
        $payload = $data['payload'];
        $metadata = $data['metadata'];

        $domain = $metadata['X-Shopify-Shop-Domain'];
        $topic = $metadata['X-Shopify-Topic'];
        logger("==========". $topic ."=========");

        $user = User::where('name', $domain)->first();
        $shop = Shop::where('user_id', $user->id)->first();

        $webhookId = $this->webhook($topic, $user->id, json_encode($payload));

        switch ($topic) {
            case 'customer_payment_methods/create':
                break;

            case 'customer_payment_methods/revoke':
                break;

            case 'customer_payment_methods/update':
                event(new CheckCustomerPaymentMethodUpdate($webhookId, $user->id, $shop->id));
                break;

            case 'customers/delete':
                $customer = SsCustomer::where('shop_id', $shop->id)->where('shopify_customer_id', $payload['id'])->first();
                if( $customer ){
                    $customer->delete();
                }
                break;

            case 'customers/update':
                $is_existcustomer = SsCustomer::where('shop_id', $shop->id)->where('shopify_customer_id', $payload['id'])->first();

                if( $is_existcustomer ){
                    $customer = $is_existcustomer;
                    $customer->first_name = $payload['first_name'];
                    $customer->last_name = $payload['last_name'];
                    $customer->email = $payload['email'];
                    $customer->phone = $payload['phone'];
                    $customer->save();
                }
                break;

            case 'locations/create':
                if( $payload['active'] ){
                    $profiles = SsShippingProfile::where('shop_id', $shop->id)->get();
                    foreach ($profiles as $pkey=>$pval){
                        $result = $this->createDeliveryProfile($user->id, $pval->id,  'gid://shopify/Location/'.$payload['id']);
                    }
                }
                break;

            case 'locations/update':
                if( $payload['active'] ){
                    $profiles = SsShippingProfile::where('shop_id', $shop->id)->get();
                    foreach ($profiles as $pkey=>$pval){
                        $result = $this->createDeliveryProfile($user->id, $pval->id, 'gid://shopify/Location/'.$payload['id']);
                    }
                }
                break;

            case 'orders/create':
                break;

            case 'orders/updated':
                break;

            case 'products/delete':
                $productCnt = SsContractLineItem::select('ss_contract_id')->distinct()->where('user_id',  $user->id)->where('shopify_product_id', $payload['id'])->count();
                logger($productCnt);
                if( $productCnt > 0 ){
                    $deleted_product = new SsDeletedProduct;
                    $deleted_product->shop_id = $shop->id;
                    $deleted_product->user_id = $user->id;
                    $deleted_product->shopify_product_id =  $payload['id'];
                    $deleted_product->subscriptions_impacted = $productCnt;
                    $deleted_product->active = 1;
                    $deleted_product->save();
                }
                break;

            case 'products/update':
                event(new CheckProductUpdate($webhookId, $user->id, $shop->id));
                break;

            case 'shop/redact':
                $this->sendGDPRMail($user, 'shop/redact');
                break;

            case 'customers/data_request':
                $this->sendGDPRMail($user, 'customers/data_request');
                break;

            case 'customers/redact':
                $this->sendGDPRMail($user, 'customers/redact');
                break;

            case 'shop/update':
                $this->updateshop($payload, $user, $shop);    
                break;

            case 'subscription_billing_attempts/failure':
                event(new CheckBillingAttemptFailure($webhookId, $user->id, $shop->id));
                break;

            case 'subscription_billing_attempts/success':
                event(new CheckBillingAttemptSuccess($webhookId, $user->id, $shop->id));
                break;

            case 'subscription_contracts/create':
                event(new CheckSubscriptionContract($webhookId, $user->id, $shop->id));
                break;

            case 'subscription_contracts/update':
                break;

            default:

        }

        return;
    }

    public function updateshop($payload, $user, $shop)
    {
        try{
             logger('============== START:: updateshop ===========');
            $db_shop = ( $shop ) ? $shop : new Shop;
            $sh_shop = $payload;

            $db_shop->user_id = $user->id;
            $db_shop->shopify_store_id = $sh_shop['id'];
            $db_shop->active = true;
            $db_shop->ts_last_deactivation = date('Y-m-d H:i:s');
            $db_shop->test_store = true;
            $db_shop->name = $sh_shop['name'];
            $db_shop->email = $sh_shop['email'];
            $db_shop->myshopify_domain = $sh_shop['myshopify_domain'];
            $db_shop->domain = $sh_shop['domain'];
            $db_shop->owner = $sh_shop['shop_owner'];
            $db_shop->shopify_plan = $sh_shop['plan_name'];
            $db_shop->timezone = $sh_shop['timezone'];
            $db_shop->address1 = $sh_shop['address1'];
            $db_shop->address2 = $sh_shop['address2'];
            $db_shop->checkout_api_supported = $sh_shop['checkout_api_supported'];
            $db_shop->city = $sh_shop['city'];
            $db_shop->country = $sh_shop['country'];
            $db_shop->country_code = $sh_shop['country_code'];
            $db_shop->country_name = $sh_shop['country_name'];
            $db_shop->country_taxes = $sh_shop['county_taxes'];
            $db_shop->ss_created_at = ($shop) ? $shop->ss_created_at : date('Y-m-d H:i:s');
            $db_shop->customer_email = $sh_shop['customer_email'];
            $db_shop->currency = $sh_shop['currency'];
            $db_shop->currency_symbol = currencyH($sh_shop['currency']);
            $db_shop->enabled_presentment_currencies = json_encode($sh_shop['enabled_presentment_currencies']);
            $db_shop->eligible_for_payments = $sh_shop['eligible_for_payments'];
            $db_shop->has_discounts = $sh_shop['has_discounts'];
            $db_shop->has_gift_cards = $sh_shop['has_gift_cards'];
            $db_shop->has_storefront = $sh_shop['has_storefront'];
            $db_shop->iana_timezone = $sh_shop['iana_timezone'];
            $db_shop->latitude = $sh_shop['latitude'];
            $db_shop->longitude = $sh_shop['longitude'];
            $db_shop->money_format = $sh_shop['money_format'];
            $db_shop->money_in_emails_format = $sh_shop['money_in_emails_format'];
            $db_shop->money_with_currency_format = $sh_shop['money_with_currency_format'];
            $db_shop->money_with_currency_in_emails_format = $sh_shop['money_with_currency_in_emails_format'];
            $db_shop->multi_location_enabled = $sh_shop['multi_location_enabled'];
            $db_shop->password_enabled = $sh_shop['password_enabled'];
            $db_shop->phone = $sh_shop['phone'];
            $db_shop->pre_launch_enabled = $sh_shop['pre_launch_enabled'];
            $db_shop->primary_locale = $sh_shop['primary_locale'];
            $db_shop->province = $sh_shop['province'];
            $db_shop->province_code = $sh_shop['province_code'];
            $db_shop->requires_extra_payments_agreement = $sh_shop['requires_extra_payments_agreement'];
            $db_shop->setup_required = $sh_shop['setup_required'];
            $db_shop->taxes_included = $sh_shop['taxes_included'];
            $db_shop->tax_shipping = $sh_shop['tax_shipping'];
            $db_shop->tbl_updated_at = date('Y-m-d H:i:s');
            $db_shop->weight_unit = $sh_shop['weight_unit'];
            $db_shop->zip = $sh_shop['zip'];
            $db_shop->save();
        }catch( \Exception $e ){
            logger('============== ERROR:: updateshop ===========');
            logger(json_encode($e));
        }
    }
}
