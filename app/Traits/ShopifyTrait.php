<?php

namespace App\Traits;

use App\Models\App;
use App\Models\Country;
use App\Models\ExchangeRate;
use App\Models\Shop;
use App\Models\SsActivityLog;
use App\Models\SsContract;
use App\Models\SsEvents;
use App\Models\SsOrder;
use App\Models\SsPlan;
use App\Models\SsPlanGroup;
use App\Models\SsSetting;
use App\Models\SsShippingProfile;
use App\Models\SsShippingZone;
use App\Models\SsWebhook;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use DateTime;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use LaravelFeature\Facade\Feature;
use Osiset\BasicShopifyAPI\Options;
use Osiset\ShopifyApp\Storage\Models\Plan;
use Symfony\Component\HttpFoundation\Response;

/**
 * Trait ShopifyTrait.
 */
trait ShopifyTrait
{
    use GraphQLTrait;
    private $locationIds = [];
    /**
     * @param $method
     * @param $endPoint
     * @param $parameter
     * @param $userID
     * @return false
     */
    public function request($method, $endPoint, $parameter, $userID)
    {
        try {
            $shop = User::where('id', $userID)->first();
            return $shop->api()->rest($method, $endPoint, $parameter);
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            return false;
        }
    }

    public function shopFeature($user_id)
    {
        try {
            $query = '
                {
                  shop {
                    features {
                      eligibleForSubscriptions
                    }
                  }
                }
            ';
            $sh_shop = $this->graphQLRequest($user_id, $query);
            if (!$sh_shop['errors']) {
                return $sh_shop['body']->container['data']['shop']['features']['eligibleForSubscriptions'];

            }
            return false;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            return false;
        }
    }

    /**
     * Add webhook while any of the webhook calling
     * @param $topic
     * @param $user_id
     * @param $data
     * @return false
     */
    public function webhook($topic, $user_id, $data)
    {
        try {
            $app = App::where('app_url', env('APP_URL'))->first();
            $shop = Shop::where('user_id', $user_id)->first();

            $ss_webhook = new SsWebhook();
            $ss_webhook->topic = $topic;
            $ss_webhook->user_id = $user_id;
            $ss_webhook->shop_id = $shop->id;
            $ss_webhook->api_version = ($app) ? $app['api_version'] : env('SHOPIFY_SAPI_VERSION');
            $ss_webhook->body = $data;
            $ss_webhook->status = 'new';
            $ss_webhook->save();
            return $ss_webhook->id;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            return false;
        }
    }

    public function sendGDPRMail($user, $topic)
    {
        try {
            logger("====================== START:: sendGDPRMail ======================");

            $shop = Shop::where('user_id', $user->id)->first();

            if ($shop) {
                $setting = SsSetting::select('email_from_email', 'email_from_name')->where('shop_id', $shop->id)->first();

                $from = $setting->email_from_email;
                $fromname = $setting->email_from_name;
                $to = env('GDPR_MAIL_TO');
                $subject = 'GDPR webhook submitted!';

                $data['topic'] = $topic;
                $data['shop'] = $shop->domain;

                $res = Mail::send('mail.gdpr', $data, function ($message) use ($subject, $from, $to, $fromname) {
                    $message->from($from, $fromname);
                    $message->to($to);
                    $message->subject($subject);
                });
                logger(json_encode($res));
            }
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger("====================== ERROR:: sendGDPRMail ======================");
            logger(json_encode($e));
        }
    }

    public function saveActivity($user_id, $customer_id, $contract_id, $user_type, $msg)
    {
        try {
            $shop = Shop::where('user_id', $user_id)->first();
            $activity = new SsActivityLog();
            $activity->shop_id = $shop->id;
            $activity->user_id = $shop->user_id;
//                $activity->ss_plan_id = $user->plan_id;
            $activity->ss_customer_id = $customer_id;
            $activity->ss_contract_id = $contract_id;
            $activity->user_type = $user_type;
            $activity->user_name = $shop->owner;
            $activity->message = $msg;
            $activity->save();
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger("====================== ERROR:: saveActivity ======================");
            logger(json_encode($e));
        }
    }

    public function subscriptionUpdateActivity()
    {
        try {

            logger("====================== START:: subscriptionUpdateActivity ======================");
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger("====================== ERROR:: subscriptionUpdateActivity ======================");
            logger(json_encode($e));
        }
    }

    public function createWebhook($webhooks, $user_id)
    {
        try {
            foreach ($webhooks as $key => $value) {
                $endPoint = 'admin/api/' . env('SHOPIFY_SAPI_VERSION') . '/webhooks/count.json';
                $parameter['topic'] = $key;
                $result = $this->request('GET', $endPoint, $parameter, $user_id);

                if (!$result['errors']) {
                    $count = $result['body']->container['count'];
                    if ($count == 0) {
                        $parameter = [
                            'webhook' => [
                                'topic' => $key,
                                'address' => env('AWS_ARN_WEBHOOK_ADDRESS'),
//                                'address' => env('APP_URL') . '/webhook/' . $value,
                                'format' => 'json',
                            ],
                        ];

                        logger(json_encode($parameter));
                        $endPoint = 'admin/api/' . env('SHOPIFY_SAPI_VERSION') . '/webhooks.json';
                        $result = $this->request('POST', $endPoint, $parameter, $user_id);

                        logger(json_encode($result));
                    }
                }
            }
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('============= ERROR:: createWebhook =============');
            logger(json_encode($e));
            return false;
        }
    }

    /**
     * @param $user_id
     * @param $category
     * @param $subCategory
     * @param $des
     * @return bool|string
     */
    public function event($user_id, $category, $subCategory, $des)
    {
        try {
            $shop = Shop::where('user_id', $user_id)->first();
            $events = new SsEvents;
            $events->shop_id = $shop->id;
            $events->user_id = $user_id;
            $events->myshopify_domain = $shop->myshopify_domain;
            $events->category = $category;
            $events->subcategory = $subCategory;
            $events->description = $des;
            $events->save();
            return true;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            return $e->getMessage();
        }
    }

    /**
     * @return array
     */
    public function getThemes()
    {
        try {
            $user = Auth::user();
            $parameter['fields'] = 'id,name,role';
            $sh_themes = $user->api()->rest('GET', 'admin/themes.json', $parameter);

            $theme = [];
            if (!$sh_themes['errors']) {
                $themes = $sh_themes['body']->container['themes'];
                foreach ($themes as $key => $val) {
                    $theme[$key]['id'] = $val['id'];
                    $theme[$key]['name'] = $val['name'];
                    $theme[$key]['role'] = $val['role'];
                }
            }

            return $theme;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger($e->getMessage());
        }
    }

    public function getPublishTheme()
    {
        try {
            $user = Auth::user();
            $parameter['role'] = 'main';
            $sh_themes = $user->api()->rest('GET', 'admin/themes.json', $parameter);

            if (!$sh_themes['errors']) {
                return (@$sh_themes['body']->container['themes'][0]['id']) ? $sh_themes['body']->container['themes'][0]['id'] : '';
            }
            return '';
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger($e->getMessage());
        }
    }

    public function getThemeSchema($user_id, $theme_id)
    {
        try {
            $user = User::find($user_id);
            $parameter['fields'] = 'id,name';
            $endPoint = '/admin/api/' . env('SHOPIFY_API_VERSION') . '/themes/' . $theme_id . '/assets.json';
            $theme = $user->api()->rest('GET', $endPoint, ['asset' => ['key' => 'config/settings_schema.json']]);

            if (!$theme['errors']) {
                $res['status'] = true;
                $res['data'] = json_decode($theme['body']->container['asset']['value']);
            } else {
                $res['status'] = false;
                $res['data'] = $theme['body'];
            }
            return $res;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger($e->getMessage());
        }
    }

    /**
     * @param $user_id
     * @param $query
     * @param  array  $parameter
     * @return array|\GuzzleHttp\Promise\Promise
     */
    public function graphQLRequest($user_id, $query, $parameters = [])
    {
        try {
            logger("====================== QUERY ======================");
            logger($query);
            $user = User::find($user_id);
            return $this->graph($user, $query, $parameters);
//            $options = new Options();
            //            $options->setVersion(env('SHOPIFY_SAPI_VERSION'));
            //            $api = new BasicShopifyAPI($options);
            //            $api->setSession(new Session(
            //                $user->name, $user->password));
            //            return $api->graph($query, $parameter);
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('============ ERROR:: graphQLRequest ===========');
            logger(json_encode($e));
        }
    }

    /**
     * Make GraphQL api request
     * @param $user_id
     * @param $planData
     */
    public function ShopifySellingPlan($user_id, $planData)
    {
        try {
            $shop = Shop::Where('user_id', $user_id)->first();
            $plan_gp_id = $planData->ss_plan_group_id;
            $action = (SsPlan::where('ss_plan_group_id', $plan_gp_id)->count() <= 1) && !$planData->shopify_plan_id ? 'create' : 'update';
            $query = ($action == 'create') ? $this->createSellingPlanQuery($planData) : $this->updateSellingPlanQuery($planData);
            $result = $this->graphQLRequest($user_id, $query);

            if (!$result['errors']) {
                if ($action == 'create') {
                    $sh_sellingplan = $result['body']->container['data']['sellingPlanGroupCreate'];
                } else {
                    $sh_sellingplan = $result['body']->container['data']['sellingPlanGroupUpdate'];
                }

                if (empty($sh_sellingplan['userErrors'])) {
                    $sh_sellingPlanGroup = $sh_sellingplan['sellingPlanGroup'];
                    $planGroupData = SsPlanGroup::where('id', $plan_gp_id)->first();

                    if (!$planGroupData->shopify_plan_group_id) {
                        $planGroupData->shopify_plan_group_id = str_replace('gid://shopify/SellingPlanGroup/', '', $sh_sellingPlanGroup['id']);
                        $planGroupData->save();
                    }

                    $sh_sellingPlan = $sh_sellingPlanGroup['sellingPlans']['edges'][$planData->position]['node'];

//                    json_encode($sh_sellingPlan);

                    if (!$planData->shopify_plan_id) {
                        $planData->shopify_plan_id = str_replace('gid://shopify/SellingPlan/', '', $sh_sellingPlan['id']);
                        $planData->save();
                    }

                    if ($action == 'create') {
                        $profiles = SsShippingProfile::where('shop_id', $shop->id)->get();

                        if (count($profiles) > 0) {
                            foreach ($profiles as $pkey => $pval) {
                                $gpIds = json_decode($pval->plan_group_ids);
                                $gpIds[] = $planGroupData->shopify_plan_group_id;
                                $pval->plan_group_ids = json_encode($gpIds);
                                $pval->save();
                                $result = $this->createDeliveryProfile($user_id, $pval->id, '');
                            }
                            return $result;
                        } else {
                            return 'success';
                        }
                    } else {
                        return 'success';
                    }
                } else {
                    logger('============ ERROR:: ShopifySellingPlan User Errors ============');
                    logger(json_encode($sh_sellingplan['userErrors'][0]));

                    if ($sh_sellingplan['userErrors'][0]['code'] == 'SELLING_PLAN_DUPLICATE_OPTIONS') {
                        return 'Each plan must have unique option names.';
                    } else {
                        return $sh_sellingplan['userErrors'][0]['message'];
                    }
                }
            } else {
                logger(json_encode($result));
            }
            return false;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('============ ERROR:: ShopifySellingPlan ===========');
            logger(json_encode($e));
        }
    }

    /**
     * @param $planData
     * @return string
     */
    public function createSellingPlanQuery($planData)
    {
        try {
            $plan_gp_id = $planData->ss_plan_group_id;
            $planGroupData = SsPlanGroup::where('id', $plan_gp_id)->first();

            $policies = $this->getPolicies($planData, $planData->user_id);

            return '
                mutation {
                      sellingPlanGroupCreate(input: {
                            name: "' . $planGroupData->name . '",
                            description: "' . $planGroupData->description . '",
                            merchantCode: "' . $planGroupData->merchantCode . '",
                            position: ' . $planGroupData->position . ',
                            options: ["' . $planGroupData->options . '"],
                                sellingPlansToCreate: [
                                {
                                  name: "' . $planData->name . '"
                                  description: "' . $planData->description . '"
                                  options: ["' . $planData->options . '"],
                                  position: ' . $planData->position . '
                                  billingPolicy: ' . $policies['billingPolicy'] . '
                                  deliveryPolicy: ' . $policies['deliveryPolicy'] . '
                                  pricingPolicies: [' . $policies['pricingPolicy'] . ']
                                }]
                        }) {
                            sellingPlanGroup {
                              id
                              sellingPlans(first: ' . ($planData->position + 1) . ') {
                                edges {
                                  node {
                                    id
                                  }
                                }
                              }
                            }
                            userErrors {
                              code
                              field
                              message
                            }
                        }
                    }
            ';
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('============ ERROR:: createSellingPlanQuery ===========');
            logger(json_encode($e));
        }
    }

    /**
     * @param $planData
     * @return string
     */
    public function updateSellingPlanQuery($planData)
    {
        try {
            $plan_gp_id = $planData->ss_plan_group_id;
            $planGroupData = SsPlanGroup::where('id', $plan_gp_id)->first();

            $action = ($planData->shopify_plan_id) ? 'sellingPlansToUpdate' : 'sellingPlansToCreate';

            $policies = $this->getPolicies($planData, $planData->user_id);

            $query = '
                mutation {
                      sellingPlanGroupUpdate(id: "gid://shopify/SellingPlanGroup/' . $planGroupData->shopify_plan_group_id . '", input: {
                            name: "' . $planGroupData->name . '",
                            description: "' . $planGroupData->description . '",
                            merchantCode: "' . $planGroupData->merchantCode . '",
                            position: ' . $planGroupData->position . ',
                            options: ["' . $planGroupData->options . '"],
                                ' . $action . ': [
                                {';

            $query .= ($planData->shopify_plan_id) ? 'id: "gid://shopify/SellingPlan/' . $planData->shopify_plan_id . '", ' : '';
            $query .= 'name: "' . $planData->name . '"
                                  description: "' . $planData->description . '"
                                  options: ["' . $planData->options . '"]
                                  position: ' . $planData->position . '
                                  billingPolicy: ' . $policies['billingPolicy'] . '
                                  deliveryPolicy: ' . $policies['deliveryPolicy'] . '
                                  pricingPolicies: [' . $policies['pricingPolicy'] . ']
                                }]
                        }) {
                            sellingPlanGroup {
                              id
                              sellingPlans(first: ' . ($planData->position + 1) . ') {
                                edges {
                                  node {
                                    id
                                  }
                                }
                              }
                            }
                            userErrors {
                              code
                              field
                              message
                            }
                        }
                    }
            ';
            return $query;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('============ ERROR:: updateSellingPlanQuery ===========');
            logger(json_encode($e));
        }
    }

    public function getPolicies($planData, $user_id)
    {
        logger($planData);
        $policies = [];
        $user = User::find($user_id);
        // fixed adjustment
        $is_percentage = ($planData->pricing_adjustment_type == '%');
        $adjustmentType = ($is_percentage) ? 'PERCENTAGE' : 'FIXED_AMOUNT';
        $adjustmentTypeValue = ($is_percentage) ? 'percentage: ' : 'fixedValue: ';

        // // recurring adjustment
        // $is_rpercentage = ($planData->pricing2_adjustment_type == '%');
        // $rAdjustmentType = ($is_rpercentage) ? 'PERCENTAGE' : 'FIXED_AMOUNT';
        // $rAdjustmentValue = ($is_rpercentage) ? 'percentage: ' : 'fixedValue: ';

        $min_cycle = ($planData['billing_min_cycles']) ? ', minCycles: ' . $planData->billing_min_cycles : '';
        $max_cycle = ($planData['billing_max_cycles']) ? ', maxCycles: ' . $planData->billing_max_cycles : '';

        if (\LaravelFeature\Model\Feature::where('name', 'Prepaid subscriptions')->count() > 0) {
            if (Feature::isEnabledFor('Prepaid subscriptions', $user)) {
                $delivery_interval = ($planData->is_prepaid) ? $planData->delivery_interval : $planData->billing_interval;
                $delivery_interval_count = ($planData->is_prepaid) ? $planData->delivery_interval_count : $planData->billing_interval_count;
            } else {
                $delivery_interval = $planData->billing_interval;
                $delivery_interval_count = $planData->billing_interval_count;
            }
        } else {
            $delivery_interval = $planData->billing_interval;
            $delivery_interval_count = $planData->billing_interval_count;
        }

        if ($planData->delivery_intent == 'A fixed day each delivery cycle') {
            if ($planData->billing_interval == 'day') {
                $policies['billingPolicy'] = '{ recurring: { interval: ' . strtoupper($planData->billing_interval) . ', intervalCount: ' . $planData->billing_interval_count . $min_cycle . $max_cycle . ' } }';

                $policies['deliveryPolicy'] = '{ recurring: { interval: ' . strtoupper($delivery_interval) . ', intervalCount: ' . $delivery_interval_count . ', cutoff: ' . $planData->delivery_cutoff . ', preAnchorBehavior: ' . $planData->delivery_pre_cutoff_behaviour . ', intent: FULFILLMENT_BEGIN} }';
                // if(\LaravelFeature\Model\Feature::where('name', 'Anchor days')->count() > 0){
                //     if( Feature::isEnabledFor('Anchor days', $user) ){
                //          $policies['deliveryPolicy'] = '{ recurring: { interval: '. strtoupper($delivery_interval) .', intervalCount: '. $delivery_interval_count .', cutoff: '. $planData->delivery_cutoff .', preAnchorBehavior: '. $planData->delivery_pre_cutoff_behaviour .', intent: FULFILLMENT_BEGIN} }';
                //     }else{
                //         $policies['deliveryPolicy'] = '{ recurring: { interval: '. strtoupper($delivery_interval) .', intervalCount: '. $delivery_interval_count .', intent: FULFILLMENT_BEGIN} }';
                //     }
                // }else{
                //     $policies['deliveryPolicy'] = '{ recurring: { interval: '. strtoupper($delivery_interval) .', intervalCount: '. $delivery_interval_count .', intent: FULFILLMENT_BEGIN} }';
                // }
            } else {
                $delivery_interval = ($planData->is_prepaid) ? $planData->delivery_interval : $planData->billing_interval;
                $delivery_interval_count = ($planData->is_prepaid) ? $planData->delivery_interval_count : $planData->billing_interval_count;

                if ($planData->billing_anchor_type == 'YEARDAY') {
                    $policies['billingPolicy'] = '{ recurring: { interval: ' . strtoupper($planData->billing_interval) . ', intervalCount: ' . $planData->billing_interval_count . $min_cycle . $max_cycle . ', anchors: { day: ' . $planData->billing_anchor_day . ', month: ' . $planData->billing_anchor_month . ', type: ' . $planData->billing_anchor_type . '} } }';

                    // if(\LaravelFeature\Model\Feature::where('name', 'Anchor days')->count() > 0){
                    //     if( Feature::isEnabledFor('Anchor days', $user) ){
                    //         $policies['deliveryPolicy'] = '{ recurring: { interval: '. strtoupper($delivery_interval) .', intervalCount: '. $delivery_interval_count .', cutoff: '. $planData->delivery_cutoff .', preAnchorBehavior: '. $planData->delivery_pre_cutoff_behaviour .', intent: FULFILLMENT_BEGIN, anchors: { day: '. $planData->billing_anchor_day .', month: '. $planData->billing_anchor_month .', type: '. $planData->billing_anchor_type .'} } }';
                    //     }else{
                    //          $policies['deliveryPolicy'] = '{ recurring: { interval: '. strtoupper($delivery_interval) .', intervalCount: '. $delivery_interval_count .', intent: FULFILLMENT_BEGIN, anchors: { day: '. $planData->billing_anchor_day .', month: '. $planData->billing_anchor_month .', type: '. $planData->billing_anchor_type .'} } }';
                    //     }
                    // }else{
                    //     $policies['deliveryPolicy'] = '{ recurring: { interval: '. strtoupper($pdelivery_interval) .', intervalCount: '. $delivery_interval_count .', intent: FULFILLMENT_BEGIN, anchors: { day: '. $planData->billing_anchor_day .', month: '. $planData->billing_anchor_month .', type: '. $planData->billing_anchor_type .'} } }';
                    // }
                    $policies['deliveryPolicy'] = '{ recurring: { interval: ' . strtoupper($delivery_interval) . ', intervalCount: ' . $delivery_interval_count . ', cutoff: ' . $planData->delivery_cutoff . ', preAnchorBehavior: ' . $planData->delivery_pre_cutoff_behaviour . ', intent: FULFILLMENT_BEGIN, anchors: { day: ' . $planData->billing_anchor_day . ', month: ' . $planData->billing_anchor_month . ', type: ' . $planData->billing_anchor_type . '} } }';
                } else {
                    $policies['billingPolicy'] = '{ recurring: { interval: ' . strtoupper($planData->billing_interval) . ', intervalCount: ' . $planData->billing_interval_count . $min_cycle . $max_cycle . ', anchors: { day: ' . $planData->billing_anchor_day . ', type: ' . $planData->billing_anchor_type . '} } }';
                    $policies['deliveryPolicy'] = '{ recurring: { interval: ' . strtoupper($delivery_interval) . ', intervalCount: ' . $delivery_interval_count . ', cutoff: ' . $planData->delivery_cutoff . ', preAnchorBehavior: ' . $planData->delivery_pre_cutoff_behaviour . ', intent: FULFILLMENT_BEGIN, anchors: {day: ' . $planData->billing_anchor_day . ', type: ' . $planData->billing_anchor_type . '} } }';
                    // if(\LaravelFeature\Model\Feature::where('name', 'Anchor days')->count() > 0){
                    //     if( Feature::isEnabledFor('Anchor days', $user) ){
                    //         $policies['deliveryPolicy'] = '{ recurring: { interval: '. strtoupper($delivery_interval) .', intervalCount: '. $delivery_interval_count .', cutoff: '. $planData->delivery_cutoff .', preAnchorBehavior: '. $planData->delivery_pre_cutoff_behaviour .', intent: FULFILLMENT_BEGIN, anchors: {day: '. $planData->billing_anchor_day .', type: '. $planData->billing_anchor_type .'} } }';
                    //     }else{
                    //          $policies['deliveryPolicy'] = '{ recurring: { interval: '. strtoupper($delivery_interval) .', intervalCount: '. $delivery_interval_count .', intent: FULFILLMENT_BEGIN, anchors: { day: '. $planData->billing_anchor_day .', type: '. $planData->billing_anchor_type .'} } }';
                    //     }
                    // }else{
                    //     $policies['deliveryPolicy'] = '{ recurring: { interval: '. strtoupper($delivery_interval) .', intervalCount: '. $delivery_interval_count .', intent: FULFILLMENT_BEGIN, anchors: { day: '. $planData->billing_anchor_day .', type: '. $planData->billing_anchor_type .'} } }';
                    // }
                }
            }
        } else {
            $policies['billingPolicy'] = '{ recurring: { interval: ' . strtoupper($planData->billing_interval) . ', intervalCount: ' . $planData->billing_interval_count . $min_cycle . $max_cycle . ' } }';

            if ($planData->is_prepaid) {
                $policies['deliveryPolicy'] = '{ recurring: { interval: ' . strtoupper($planData->delivery_interval) . ', intervalCount: ' . $planData->delivery_interval_count . ', intent: FULFILLMENT_BEGIN } }';
            } else {
                $policies['deliveryPolicy'] = '{ recurring: { interval: ' . strtoupper($planData->billing_interval) . ', intervalCount: ' . $planData->billing_interval_count . ', intent: FULFILLMENT_BEGIN } }';
            }
        }

        $adjustmentValue = ($planData->pricing_adjustment_value != '') ? $planData->pricing_adjustment_value : 0;
        // if( $planData->pricing_adjustment_value != '' ){
        $policies['pricingPolicy'] = '{
                fixed: {
                  adjustmentType: ' . $adjustmentType . '
                  adjustmentValue: { ' . $adjustmentTypeValue . $adjustmentValue . '}
                }
              }';
        // }
        // else if ( $planData->pricing2_adjustment_value != '' ){
        //    $policies['pricingPolicy'] = '{
        //     recurring: {
        //       adjustmentType: '. $rAdjustmentType .'
        //       adjustmentValue: { '. $rAdjustmentValue . $planData->pricing2_adjustment_value .'}
        //       afterCycle: '. $planData->pricing2_after_cycle .'
        //     }
        //   }';
        // }

        return $policies;
    }

    public function updateSellingPlanGroup($user_id, $planGroupData)
    {
        try {

            $query = '
                mutation {
                    sellingPlanGroupUpdate(id: "gid://shopify/SellingPlanGroup/' . $planGroupData->shopify_plan_group_id . '", input: {
                            name: "' . $planGroupData->name . '",
                            description: "' . $planGroupData->description . '",
                            position: ' . $planGroupData->position . ',
                             options: ["' . $planGroupData->options . '"],
                    }){
                      userErrors {
                        code
                        field
                        message
                      }
                    }
                }
            ';

            $result = $this->graphQLRequest($user_id, $query);
            $msg = $this->getReturnMessage($result, 'sellingPlanGroupUpdate');
            return $msg;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('============ ERROR:: updateSellingPlanGroup ===========');
            logger(json_encode($e));
        }
    }

    public function updateSellingPlan($user_id, $planGroupID, $planData)
    {
        try {

            $query = '
                mutation {
                    sellingPlanGroupUpdate(id: "gid://shopify/SellingPlanGroup/' . $planGroupID . '", input: {
                            sellingPlansToUpdate: [{
                                id: "gid://shopify/SellingPlan/' . $planData->shopify_plan_id . '", position: ' . $planData->position . '
                            }]
                    }){
                      userErrors {
                        code
                        field
                        message
                      }
                    }
                }
            ';

            $result = $this->graphQLRequest($user_id, $query);
            $msg = $this->getReturnMessage($result, 'sellingPlanGroupUpdate');
            return $msg;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('============ ERROR:: updateSellingPlan ===========');
            logger(json_encode($e));
        }
    }

    public function updateSellingPlanGroupProduct($user_id, $productIds, $planGroupID)
    {
        try {
            $query = '
                mutation {
                    sellingPlanGroupAddProducts(id: "gid://shopify/SellingPlanGroup/' . $planGroupID . '", productIds: ["gid://shopify/Product/' . implode('","gid://shopify/Product/', $productIds) . '"]) {
                      userErrors {
                        code
                        field
                        message
                      }
                    }
                }
            ';

            $result = $this->graphQLRequest($user_id, $query);
            $msg = $this->getReturnMessage($result, 'sellingPlanGroupAddProducts');
            return $msg;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('============ ERROR:: updateSellingPlanGroupProduct ===========');
            logger(json_encode($e));
        }
    }

    public function updateSellingPlanGroupRemoveProduct($user_id, $productIds, $planGroupID)
    {
        try {
            $query = '
                mutation {
                    sellingPlanGroupRemoveProducts(id: "gid://shopify/SellingPlanGroup/' . $planGroupID . '", productIds: ["gid://shopify/Product/' . implode('","gid://shopify/Product/', $productIds) . '"]) {
                      userErrors {
                        code
                        field
                        message
                      }
                    }
                }
            ';

            $result = $this->graphQLRequest($user_id, $query);
            $msg = $this->getReturnMessage($result, 'sellingPlanGroupRemoveProducts');
            return $msg;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('============ ERROR:: updateSellingPlanGroupRemoveProduct ===========');
            logger(json_encode($e));
        }
    }

    public function deleteSellingPlan($user_id, $planGroupID, $planID, $planCount)
    {
        try {
            $query = '
                mutation {
                    sellingPlanGroupUpdate(id: "gid://shopify/SellingPlanGroup/' . $planGroupID . '", input: {
                            sellingPlansToDelete: "gid://shopify/SellingPlan/' . $planID . '",
                    }){
                      userErrors {
                        code
                        field
                        message
                      }
                    }
                }
            ';

            $result = $this->graphQLRequest($user_id, $query);

            $msg = $this->getReturnMessage($result, 'sellingPlanGroupUpdate');
            if ($planCount == 1) {
                $msg = $this->deleteSellingPlanGroup($user_id, $planGroupID);
            }

            return $msg;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('============ ERROR:: deleteSellingPlan ===========');
            logger(json_encode($e));
        }
    }

    public function deleteSellingPlanGroup($user_id, $planGroupID)
    {
        try {
            $shop = Shop::Where('user_id', $user_id)->first();
            $query = '
                mutation {
                    sellingPlanGroupDelete(id: "gid://shopify/SellingPlanGroup/' . $planGroupID . '") {
                    userErrors {
                      code
                      field
                      message
                    }
                  }
                }
            ';

            $result = $this->graphQLRequest($user_id, $query);
            $msg = $this->getReturnMessage($result, 'sellingPlanGroupDelete');

            if ($msg == 'success') {
                $profiles = SsShippingProfile::where('shop_id', $shop->id)->get();

                if (count($profiles) > 0) {
                    foreach ($profiles as $pkey => $pval) {
                        $result = $this->createDeliveryProfile($user_id, $pval->id, '');
                        if ($result == 'success') {
                            $gpIds = json_decode($pval->plan_group_ids);
                            unset($gpIds[array_search($planGroupID, $gpIds)]);
                            $pval->plan_group_ids = json_encode($gpIds);
                            $pval->save();
                        }
                    }
                    return $result;
                }

            }
            return $msg;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('============ ERROR:: deleteSellingPlanGroup ===========');
            logger(json_encode($e));
        }
    }

    public function getReturnMessage($result, $action)
    {
        logger('=============== ' . $action . ' ==============');
        logger(json_encode($result));
        if (!$result['errors']) {
            $data = $result['body']->container['data'][$action];
            if (empty($data['userErrors'])) {
                return 'success';
            } else {
                return $data['userErrors'][0]['message'];
            }
        } else {
            return (@$result['errors'][0]['message']) ? $result['errors'][0]['message'] : false;
        }
        return false;
    }

    public function createOrder($user_id, $shop_id, $shopify_order_id, $customer_id, $contract_id)
    {
        $user = User::find($user_id);
        $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/orders/' . $shopify_order_id . '.json';
        $result = $user->api()->rest('GET', $endPoint);
        if (!$result['errors']) {
            $data = $result['body']['order'];
            logger(json_encode($data));
            $db_order = new SsOrder;
            $plan = Plan::find($user->plan_id);

            $db_rates = ExchangeRate::orderBy('created_at', 'desc')->first();
            $rate = json_decode($db_rates->conversion_rates);
            $currencyCode = $data->currency;

            $tx_fee = $plan->transaction_fee;
            $db_order->shop_id = $shop_id;
            $db_order->user_id = $user->id;
            $db_order->shopify_order_id = $shopify_order_id;
            $db_order->ss_customer_id = $customer_id;
            $db_order->ss_contract_id = $contract_id;
            $db_order->shopify_order_name = $data->name;
            $db_order->order_currency = $data->currency;
            $db_order->currency_symbol = currencyH($data->currency);
            $db_order->order_amount = calculateCurrency($data->currency, 'USD', $data->total_price);
            $db_order->conversion_rate = $rate->$currencyCode;
            $db_order->tx_fee_status = 'pending';
            $db_order->tx_fee_percentage = $tx_fee;
            $db_order->tx_fee_amount = number_format(($db_order->order_amount * $tx_fee), 4);
            $db_order->is_test = ($data->test);
            $db_order->save();
            return $db_order;
        }
        return null;
    }

    public function getSubscriptionTime($shop_id)
    {
        $setting = SsSetting::select('subscription_daily_at')->where('shop_id', $shop_id)->first();
        $subscription_daily_at = $setting->subscription_daily_at;
        $timeformat = (substr($subscription_daily_at, -2));
        $time = substr($subscription_daily_at, 0, 5);

        if ($timeformat == 'PM' && $time == '11:59') {
            $addtime = 23 . ':59:00';
        } elseif ($timeformat == 'AM' && $time == '12:01') {
            $addtime = '00:01:00';
        } else {
            if ($timeformat == 'PM' && $time == '12:01') {
                $addtime = 1 . ':00:00';
            } else {
                if ($timeformat == 'PM' && $time != '12:00') {
                    $addtime = (12 + substr($subscription_daily_at, 0, 2)) . ':00:00';
                } else {
                    $addtime = substr($subscription_daily_at, 0, 2) . ':00:00';
                }
            }
        }
        return $addtime;
    }

    public function getSubscriptionTimeDate($nextDate, $shop_id, $customTime = '', $customTimeZone = '')
    {
        $shop = Shop::find($shop_id);

        $tz = $shop->timezone;
        $ianatz = $shop->iana_timezone;

        // $gmt = str_replace('GMT', '', substr($tz, (strpos($tz, '(') + 1), (strpos($tz, ')') - 1)));

        $gmt = $this->getUTCOffset($ianatz);

        $gmtSign = substr($gmt, 0, 1);

        if ($customTime == '') {
            $deductSign = ($gmtSign == '+') ? '-' : '+';
        } else {
            $deductSign = $gmtSign;
        }
        $gmtHour = explode(':', str_replace($gmtSign, '', $gmt));

        $fdate = new DateTime();

        if ($customTime == '') {
            $customTime = $this->getSubscriptionTime($shop->id);
        }

//        $mydate = date('Y-m-d ' . $customTime);
        $mydate = $nextDate . ' ' . $customTime;

        $default_timezone = date_default_timezone_get();
        // dump($default_timezone .' :: ' . date('Y-m-d H:i:s'));
        ($customTimeZone == '') ? date_default_timezone_set($shop->iana_timezone) : date_default_timezone_set($customTimeZone);

        $sdate = $fdate = new DateTime();
        // dump($shop->iana_timezone .' :: ' . date('Y-m-d H:i:s'));

        $dateTime = new DateTime($mydate);
        $dateTime->modify("$deductSign$gmtHour[1] minutes");
        $dateTime->modify("$deductSign$gmtHour[0] hours");

        // if( $customTimeZone == '' ){
        //     $interval = $fdate->diff($sdate);
        //     $dateTime = new DateTime($mydate);
        //     $dateTime->modify("$deductSign$interval->m minutes");
        //     $dateTime->modify("$deductSign$interval->h hours");
        // }else{
        //     $dateTime = new DateTime($mydate);
        //     $dateTime->modify("$deductSign$gmtHour[1] minutes");
        //     $dateTime->modify("$deductSign$gmtHour[0] hours");
        // }

        $returnD = date_format($dateTime, 'Y-m-d H:i:s');
        date_default_timezone_set($default_timezone);

        return $returnD;
    }

    public function getUTCOffset($timezone)
    {
        $current = timezone_open($timezone);
        $utcTime = new \DateTime('now', new \DateTimeZone('UTC'));
        $offsetInSecs = timezone_offset_get($current, $utcTime);
        $hoursAndSec = gmdate('H:i', abs($offsetInSecs));
        return stripos($offsetInSecs, '-') === false ? "+{$hoursAndSec}" : "-{$hoursAndSec}";
    }

//     public function getSubscriptionTimeDate($nextDate, $shop_id, $customTime = '', $customTimeZone = ''){
    //         $shop = Shop::find($shop_id);

//         $tz = $shop->timezone;

//         $gmt = str_replace('GMT', '', substr($tz, (strpos($tz, '(') + 1), (strpos($tz, ')') - 1)));
    //         $gmtSign = substr($gmt, 0, 1);

//         if( $customTime == '' ){
    //             $deductSign = ( $gmtSign == '+' ) ? '-' : '+';
    //         }else{
    //             $deductSign = $gmtSign;
    //         }
    //         $gmtHour = explode(':', str_replace($gmtSign, '', $gmt));

//         // dump('$gmtHour :: ' );
    //         // dump($gmtHour);

//         if( $customTime == ''){
    //             $customTime = $this->getSubscriptionTime($shop->id);
    //         }

// //        $mydate = date('Y-m-d ' . $customTime);
    //         $mydate = $nextDate . ' ' . $customTime;

//         $default_timezone = date_default_timezone_get();
    //          // dump($default_timezone .' :: ' . date('Y-m-d H:i:s'));
    //         ( $customTimeZone == '' ) ? date_default_timezone_set($shop->iana_timezone) : date_default_timezone_set($customTimeZone);

//          // dump($shop->iana_timezone .' :: ' . date('Y-m-d H:i:s'));

//         $dateTime = new DateTime($mydate);
    //         $dateTime->modify("$deductSign$gmtHour[1] minutes");
    //         $dateTime->modify("$deductSign$gmtHour[0] hours");

//         $returnD = date_format($dateTime, 'Y-m-d H:i:s');
    //         date_default_timezone_set($default_timezone);

//         return $returnD;
    //     }

    public function getShopifyOrder($user, $orderId, $fields = 'id,total_price')
    {
        $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/orders/' . $orderId . '.json';
        $parameter['fields'] = $fields;
        $result = $user->api()->rest('GET', $endPoint, $parameter);
        if (!$result['errors']) {
            return $result['body']->container['order'];
        } else {
            return [];
        }
    }

    public function updateSubscriptionContract($user_id, $contractID, $update = 'status')
    {
        try {
            logger('========= START:: updateSubscriptionContract =========');
            $contract = SsContract::find($contractID);
            $shop = Shop::where('user_id', $user_id)->first();
            $draftId = $this->getSubscriptionDraft($user_id, $contract->shopify_contract_id);
            if ($draftId) {
                $countryCode = Country::select('code')->where('name', $contract->ship_country)->first();
                $code = ($countryCode) ? $countryCode->code : $shop->country_code;

                $draftQuery = '
                  mutation{
                    subscriptionDraftUpdate(draftId: "' . $draftId . '",
                    input: {';

                if ($update == 'status') {
                    $draftQuery .= '
                         status: ' . strtoupper($contract->status) . '
                    ';
                } else {
                    $draftQuery .= '
                         deliveryMethod: {
                            shipping: {
                                address: {
                                    address1: "' . $contract->ship_address1 . '",
                                    address2: "' . $contract->ship_address2 . '",
                                    city: "' . $contract->ship_city . '",
                                    company: "' . $contract->ship_company . '",
                                    country: "' . $contract->ship_country . '",
                                    firstName: "' . $contract->ship_firstName . '",
                                    lastName: "' . $contract->ship_lastName . '",
                                    province: "' . $contract->ship_province . '",
                                    provinceCode: "' . $contract->ship_provinceCode . '",
                                    zip: "' . $contract->ship_zip . '",
                                    countryCode: ' . $code . ',
                                }
                            }
                        }
                    ';
                }

                $draftQuery .= '
                    }) {
                      userErrors {
                        code
                        field
                        message
                      }
                      draft {
                        status
                      }
                    }
                  }
              ';

                $resultSubscriptionDraftContract = $this->graphQLRequest($user_id, $draftQuery);
                $message = $this->getReturnMessage($resultSubscriptionDraftContract, 'subscriptionDraftUpdate');

                if ($message == 'success') {
                    $message = $this->commitDraft($user_id, $draftId);
                }
                return $message;
            }
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('========= ERROR:: updateSubscriptionContract =========');
            logger(json_encode($e));
        }
    }

    public function commitDraft($user_id, $draftID)
    {
        try {
            logger('========= START:: commitDraft =========');
            $commitDraft = '
                  mutation{
                    subscriptionDraftCommit(draftId: "' . $draftID . '") {
                    userErrors {
                      code
                      field
                      message
                    }
                  }
                }';
            $result = $this->graphQLRequest($user_id, $commitDraft);
            return $this->getReturnMessage($result, 'subscriptionDraftCommit');
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('========= ERROR:: commitDraft =========');
            logger(json_encode($e));
        }
    }

    public function getSubscriptionDraft($user_id, $contractID)
    {
        $user = User::find($user_id);
        // get draft id from contract id
        $query = 'mutation {
          subscriptionContractUpdate(contractId: "gid://shopify/SubscriptionContract/' . $contractID . '") {
              draft {
                id
              }
              userErrors {
                  message
              }
            }
          }';
        $resultSubscriptionContract = $this->graphQLRequest($user->id, $query);
        $message = $this->getReturnMessage($resultSubscriptionContract, 'subscriptionContractUpdate');
        if ($message == 'success') {
            $subscriptionContract = $resultSubscriptionContract['body']->container['data']['subscriptionContractUpdate'];
            return (@$subscriptionContract['draft']['id']) ? $subscriptionContract['draft']['id'] : '';
        } else {
            return $message;
        }
    }

    public function subscriptionContractSetNextBillingDate($user_id, $contractID)
    {
        try {
            logger('========= START:: subscriptionContractSetNextBillingDate =========');
            $contract = SsContract::where('shopify_contract_id', $contractID)->first();
            $query = 'mutation {
                 subscriptionContractSetNextBillingDate(contractId: "gid://shopify/SubscriptionContract/' . $contractID . '", date: "' . date('Y-m-d\TH:i:s\Z', strtotime($contract->next_order_date)) . '") {
                    userErrors {
                      code
                      field
                      message
                    }
                  }
                }';

            $result = $this->graphQLRequest($user_id, $query);
            return $this->getReturnMessage($result, 'subscriptionContractSetNextBillingDate');
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('========= ERROR:: subscriptionContractSetNextBillingDate =========');
            logger(json_encode($e));
        }
    }

    public function subscriptionDraftLineAdd($user_id, $lineItem)
    {
        try {
            logger('========= START:: subscriptionDraftLineAdd =========');
            $draftId = $this->getSubscriptionDraft($user_id, $lineItem->shopify_contract_id);
            $adjustmentType = ($lineItem->discount_type == '%') ? 'PERCENTAGE' : 'FIXED_AMOUNT';
            $adjustmentValue = ($lineItem->discount_type == '%') ? 'percentage' : 'fixedValue';

            if ($draftId) {
                $query = '
                 mutation{
                      subscriptionDraftLineAdd(
                            draftId: "' . $draftId . '",
                            input: {
                                currentPrice: "' . $lineItem->price . '"
                                pricingPolicy: {
                                    basePrice: "' . $lineItem->price . '"
                                    cycleDiscounts: {
                                        adjustmentType: ' . $adjustmentType . ',
                                        adjustmentValue: {
                                            ' . $adjustmentValue . ': ' . $lineItem->discount_amount . '
                                        },
                                        computedPrice: "' . $lineItem->final_amount . '",
                                        afterCycle: 0
                                    }
                                },
                                productVariantId: "gid://shopify/ProductVariant/' . $lineItem->shopify_variant_id . '",
                                quantity: ' . $lineItem->quantity . '
                            },
                        )
                        {
                            lineAdded {
                              id
                              sellingPlanId
                              sellingPlanName
                            }
                            userErrors {
                              code
                              field
                              message
                            }
                        }
                    }';

                $subscriptionDraftResult = $this->graphQLRequest($user_id, $query);
                $message = $this->getReturnMessage($subscriptionDraftResult, 'subscriptionDraftLineAdd');

                if ($message == 'success') {
                    $message = $this->commitDraft($user_id, $draftId);
                    if ($message == 'success') {
                        $res['id'] = $subscriptionDraftResult['body']->container['data']['subscriptionDraftLineAdd']['lineAdded']['id'];
                        $res['sellingPlanId'] = $subscriptionDraftResult['body']->container['data']['subscriptionDraftLineAdd']['lineAdded']['sellingPlanId'];
                        $res['sellingPlanName'] = $subscriptionDraftResult['body']->container['data']['subscriptionDraftLineAdd']['lineAdded']['sellingPlanName'];
                        return $res;
                    }
                }

                return $message;
//                return $this->getReturnMessage($result, 'subscriptionDraftLineUpdate');
            }
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('========= ERROR:: subscriptionDraftLineAdd =========');
            logger(json_encode($e));
        }
    }

    public function subscriptionDraftLineUpdate($user_id, $lineItem)
    {
        try {
            logger('========= START:: subscriptionDraftLineUpdate =========');
            $draftId = $this->getSubscriptionDraft($user_id, $lineItem->shopify_contract_id);
            $adjustmentType = ($lineItem->discount_type == '%') ? 'PERCENTAGE' : 'FIXED_AMOUNT';
            $adjustmentValue = ($lineItem->discount_type == '%') ? 'percentage' : 'fixedValue';

            if ($draftId) {
                $query = '
                 mutation{
                      subscriptionDraftLineUpdate(
                            draftId: "' . $draftId . '",
                            input: {
                                pricingPolicy: {
                                    basePrice: "' . $lineItem->price . '"
                                    cycleDiscounts: {
                                        adjustmentType: ' . $adjustmentType . ',
                                        adjustmentValue: {
                                            ' . $adjustmentValue . ': ' . $lineItem->discount_amount . '
                                        },
                                        computedPrice: "' . $lineItem->final_amount . '",
                                        afterCycle: 0
                                    }
                                },
                                productVariantId: "gid://shopify/ProductVariant/' . $lineItem->shopify_variant_id . '",
                                quantity: ' . $lineItem->quantity . '
                            },
                            lineId: "gid://shopify/SubscriptionLine/' . $lineItem->shopify_line_id . '"
                        ){
                            userErrors {
                              code
                              field
                              message
                            }
                        }
                    }';

                $subscriptionDraftResult = $this->graphQLRequest($user_id, $query);
                $message = $this->getReturnMessage($subscriptionDraftResult, 'subscriptionDraftLineUpdate');

                if ($message == 'success') {
                    $message = $this->commitDraft($user_id, $draftId);
                }

                return $message;
//                return $this->getReturnMessage($result, 'subscriptionDraftLineUpdate');
            }
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('========= ERROR:: subscriptionDraftLineUpdate =========');
            logger(json_encode($e));
        }
    }

    public function subscriptionDraftLineRemove($user_id, $lineItem)
    {
        try {
            logger('========= START:: subscriptionDraftLineRemove =========');
            $draftId = $this->getSubscriptionDraft($user_id, $lineItem->shopify_contract_id);

            if ($draftId) {
                $query = '
                mutation{
                     subscriptionDraftLineRemove(draftId: "' . $draftId . '", lineId: "gid://shopify/SubscriptionLine/' . $lineItem->shopify_line_id . '") {
                        userErrors {
                          code
                          message
                          field
                        }
                      }
                    }';

                $subscriptionDraftResult = $this->graphQLRequest($user_id, $query);
                $message = $this->getReturnMessage($subscriptionDraftResult, 'subscriptionDraftLineRemove');

                if ($message == 'success') {
                    $message = $this->commitDraft($user_id, $draftId);
                }
                return $message;
//                return $this->getReturnMessage($result, 'subscriptionDraftLineRemove');
            }
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('========= ERROR:: subscriptionDraftLineRemove =========');
            logger(json_encode($e));
        }
    }

    public function getCustomerPaymentMethodID($user_id, $contractID)
    {
        try {
            logger('========= START:: getCustomerPaymentMethodID =========');
            $paymentMethodID = '';
            $query = '{
                     subscriptionContract(id: "gid://shopify/SubscriptionContract/' . $contractID . '") {
                        customerPaymentMethod {
                          id
                        }
                    }
                }';

            $result = $this->graphQLRequest($user_id, $query);
            if (!$result['errors']) {
                $subContract = $result['body']->container['data']['subscriptionContract'];
                $paymentMethodID = (@$subContract['customerPaymentMethod']['id']) ? $subContract['customerPaymentMethod']['id'] : '';
            } else {
                logger('============== getCustomerPaymentMethodID ===============');
                logger(json_encode($result));
            }
            return $paymentMethodID;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('========= ERROR:: getCustomerPaymentMethodID =========');
            logger(json_encode($e));
        }
    }

    public function createCustomerPaymentMethodSendUpdateEmail($user_id, $contractID)
    {
        try {
            logger('========= START:: createCustomerPaymentMethodSendUpdateEmail =========');
            $paymentMethodID = $this->getCustomerPaymentMethodID($user_id, $contractID);
            if ($paymentMethodID != '') {
                $query = 'mutation{
                          customerPaymentMethodSendUpdateEmail(customerPaymentMethodId: "' . $paymentMethodID . '") {
                            customer {
                              id
                            }
                            userErrors {
                              field
                              message
                            }
                        }
                      }';
                $result = $this->graphQLRequest($user_id, $query);
                $message = $this->getReturnMessage($result, 'customerPaymentMethodSendUpdateEmail');
            } else {
                $message = 'Customer payment method not found';
            }

            return $message;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('========= ERROR:: createCustomerPaymentMethodSendUpdateEmail =========');
            logger(json_encode($e));
        }
    }

    public function getPrepaidFulfillments($user_id, $last_billing_order)
    {
        try {
            logger('========= START:: getPrepaidFulfillments =========');
            $query = '{
                    order(id: "gid://shopify/Order/' . $last_billing_order . '") {
                      fulfillmentOrders(first: 250) {
                        edges {
                          node {
                            fulfillAt
                            status
                            id
                            order {
                              name
                              legacyResourceId
                            }
                          }
                        }
                      }
                    }
                  }';
            $result = $this->graphQLRequest($user_id, $query);
            if (!$result['errors']) {
                $fulfillmentOrders = $result['body']->container['data']['order']['fulfillmentOrders'];
                return ($fulfillmentOrders) ? $fulfillmentOrders['edges'] : [];
            } else {
                logger('========= USER ERROR:: getPrepaidFulfillments =========');
                logger(json_encode($result));
                return [];
            }
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('========= ERROR:: getPrepaidFulfillments =========');
            logger(json_encode($e));
        }
    }

    public function createDeliveryProfile($user_id, $ss_profile_id, $shp_location_id = '')
    {
        try {
            logger('========= START:: createDeliveryProfile =========');

            $user = User::find($user_id);
            $shop = Shop::select('currency')->where('user_id', $user_id)->first();

            if ($shp_location_id == '') {
                $this->getActiveLocations($user, '');
            } else {
                $this->locationIds = (array) $shp_location_id;
            }
            $activeLocations = $this->locationIds;
            // $activeLocations = (array)$activeLocations[0];
            foreach ($activeLocations as $lkey => $lval) {
                $location = str_replace('gid://shopify/Location/', '', $lval);
                $profile = SsShippingProfile::with('ShippingZones')->where('id', $ss_profile_id)->first()->toArray();

                $profileAction = ($profile['shopify_profile_id']) ? 'deliveryProfileUpdate' : 'deliveryProfileCreate';
                $locationGroup = (json_decode($profile['shopify_location_group_id'])) ? (array) json_decode($profile['shopify_location_group_id']) : [];

                $locationAction = (array_key_exists($location, $locationGroup)) ? 'locationGroupsToUpdate' : 'locationGroupsToCreate';
                // $locationAction = ( $profile['shopify_location_group_id'] ) ? 'locationGroupsToUpdate' : 'locationGroupsToCreate';

                if ($profileAction == 'deliveryProfileCreate') {
                    $query = 'mutation MyMutation {
                      ' . $profileAction . '(profile: { name: "' . $profile['name'] . '"';
                } else {
                    $query = 'mutation MyMutation {
                      ' . $profileAction . '(id: "gid://shopify/DeliveryProfile/' . $profile['shopify_profile_id'] . '", profile: {';
                }

                logger(json_encode($profile));
                logger(json_decode($profile['plan_group_ids']));
                if (!empty(json_decode($profile['plan_group_ids']))) {
                    $query .= ' sellingPlanGroupsToAssociate: ["gid://shopify/SellingPlanGroup/' . implode('","gid://shopify/SellingPlanGroup/', json_decode($profile['plan_group_ids'])) . '"],';
                }
                // $query .= ( $profile['shopify_profile_id'] ) ? ', id: "gid://shopify/DeliveryProfile/' . $profile['shopify_profile_id'] .'", ' : '';
                $zonesToCreate = [];
                $zonesToUpdate = [];
                foreach ($profile['shipping_zones'] as $zkey => $zval) {
                    $zoneIDs = (array) json_decode($zval['shopify_zone_id']);
                    $zoneAction = (empty($zoneIDs) || !array_key_exists($location, $zoneIDs)) ? 'zonesToCreate' : 'zonesToUpdate';

                    $country = implode(',', json_decode($zval['countries']));
                    if ($country == 'Rest of World') {
                        $countries = '{ restOfWorld: true }';
                    } else {
                        $country = explode(',', $country);
                        $countries = '[';
                        foreach ($country as $ckey => $cval) {
                            $countries .= "{code: $cval, includeAllProvinces: true}";
                        }
                        $countries .= ']';
                    }
                    $zactive = ($zval['active']) ? 'true' : 'false';

                    $zoneArryQ = '{';
                    $zoneArryQ .= ($zoneAction == 'zonesToCreate') ? '' : 'id: "gid://shopify/DeliveryZone/' . $zoneIDs[$location] . '" ';
                    $zoneArryQ .= 'name: "' . $zval['zone_name'] . '",
                      countries: ' . $countries . '
                      methodDefinitionsToCreate: {
                        name: "' . $zval['rate_name'] . '",
                        active: ' . $zactive . ',
                        rateDefinition: {
                            price: {
                                amount: "' . $zval['rate_value'] . '",
                                currencyCode: ' . $shop['currency'] . '
                            }
                        },
                      }
                  }';

                    ($zoneAction == 'zonesToCreate') ? array_push($zonesToCreate, $zoneArryQ) : array_push($zonesToUpdate, $zoneArryQ);

                }

                $query .= $locationAction . ': {';
                $query .= ($locationAction == 'locationGroupsToUpdate') ? 'id: "gid://shopify/DeliveryLocationGroup/' . $locationGroup[$location] . '", ' : '';

                $query .= '
                          locations: "' . $lval . '",';
                $query .= (!empty($zonesToCreate)) ? 'zonesToCreate: [' : '';
                $query .= (!empty($zonesToCreate)) ? implode('', $zonesToCreate) : '';
                $query .= (!empty($zonesToCreate)) ? ']' : '';

                $query .= (!empty($zonesToUpdate)) ? 'zonesToUpdate: [' : '';
                $query .= (!empty($zonesToUpdate)) ? implode('', $zonesToUpdate) : '';
                $query .= (!empty($zonesToUpdate)) ? ']' : '';

                $query .= '}';
                $query .= '}) {
                      profile {
                        id
                        profileLocationGroups {
                          locationGroup {
                            id
                          }
                          locationGroupZones(first: ' . count($profile['shipping_zones']) . ', reverse: false) {
                            edges {
                              node {
                                zone {
                                  id
                                  name
                                }
                              }
                            }
                          }
                        }
                      }
                      userErrors {
                        field
                        message
                      }
                    }
                  }';

                $result = $this->graphQLRequest($user->id, $query);
                if (!$result['errors']) {
                    $message = $this->getReturnMessage($result, $profileAction);

                    if ($message == 'success') {
                        $deliveryProfile = $result['body']->container['data'][$profileAction]['profile'];
                        $locationGroup = $deliveryProfile['profileLocationGroups'][$lkey];

                        $saveProfile = SsShippingProfile::where('id', $ss_profile_id)->first();
                        $saveProfile->shopify_profile_id = str_replace('gid://shopify/DeliveryProfile/', '', $deliveryProfile['id']);

                        $locationGids = (json_decode($saveProfile->shopify_location_group_id)) ? (array) json_decode($saveProfile->shopify_location_group_id) : [];
                        $locationGid = str_replace('gid://shopify/DeliveryLocationGroup/', '', $locationGroup['locationGroup']['id']);
                        // if( !in_array($location, $locationGids) ){
                        //   $locationGids[] = $location;
                        // }
                        if (@$locationGids[$location]) {
                        } else {
                            $locationGids[$location] = '';
                        }
                        $locationGids[$location] = $locationGid;
                        $saveProfile->shopify_location_group_id = json_encode($locationGids);
                        $saveProfile->save();

                        $locationGZone = $locationGroup['locationGroupZones'];

                        $saveZones = SsShippingZone::where('ss_shipping_profile_id', $ss_profile_id)->get();
                        foreach ($saveZones as $zkey => $zvalue) {
                            $zones = (array) json_decode($zvalue->shopify_zone_id);

                            if (@$zones[$location]) {
                            } else {
                                $zones[$location] = '';
                            }
                            $zones[$location] = str_replace('gid://shopify/DeliveryZone/', '', $locationGZone['edges'][$zkey]['node']['zone']['id']);
                            $zvalue->shopify_zone_id = json_encode($zones);
                            $zvalue->save();
                        }

                    } else {
                        return $message;
                    }

                } else {
                    logger('========= USER ERROR:: createDeliveryProfile =========');
                    logger(json_encode($result));
                    return [];
                }
            }
            return 'success';

        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('========= ERROR:: createDeliveryProfile =========');
            logger(json_encode($e));
        }
    }

    public function removeDeliveryProfile($user_id, $ss_profile_id)
    {
        try {
            logger('========= START:: removeDeliveryProfile =========');
            $profile = SsShippingProfile::with('ShippingZones')->where('id', $ss_profile_id)->first()->toArray();
            $query = 'mutation MyMutation{
                    deliveryProfileRemove(id: "gid://shopify/DeliveryProfile/' . $profile['shopify_profile_id'] . '") {
                      userErrors {
                        field
                        message
                      }
                    }
                  }';
            $result = $this->graphQLRequest($user_id, $query);
            return $this->getReturnMessage($result, 'deliveryProfileRemove');
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('========= ERROR:: removeDeliveryProfile =========');
            logger(json_encode($e));
        }
    }

    public function getSubscriptionDiscount($user_id, $shopify_contract_id)
    {
        try {
            logger('========= START:: getSubscriptionDiscount =========');
            $user = User::find($user_id);

            $query = ' {
            subscriptionContract(id: "gid://shopify/SubscriptionContract/' . $shopify_contract_id . '") {
              discounts(first: 10) {
                edges {
                  node {
                    id
                    rejectionReason
                    type
                    title
                    value {
                      ... on SubscriptionDiscountPercentageValue {
                        __typename
                        percentage
                      }
                      ... on SubscriptionDiscountFixedAmountValue {
                        __typename
                        amount {
                          amount
                          currencyCode
                        }
                      }
                    }
                  }
                }
              }
            }
          }
         ';

            $result = $this->graphQLRequest($user->id, $query);
            $discount['title'] = '';
            $discount['amount'] = '';

            logger(json_encode($result));
            if (!$result['errors']) {
                $subContract = $result['body']->container['data']['subscriptionContract'];
                $discounts = (@$subContract['discounts']['edges']) ? $subContract['discounts']['edges'] : [];
                if (!empty($discounts)) {
                    foreach ($discounts as $dkey => $dvalue) {
                        $dnode = $dvalue['node'];
                        if ($dnode['rejectionReason'] == null && $dnode['type'] == 'CODE_DISCOUNT') {
                            $discount['title'] = $dnode['title'];

                            if (@$dnode['value']['__typename'] == 'SubscriptionDiscountPercentageValue') {
                                $discount['amount'] = $dnode['value']['percentage'] . '%';
                            } elseif (@$dnode['value']['__typename'] == 'SubscriptionDiscountFixedAmountValue') {
                                $discount['amount'] = currencyH($dnode['value']['amount']['currencyCode']) . $dnode['value']['amount']['amount'];
                            }

                        }
                    }
                }
            }
            return $discount;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('========= ERROR:: getSubscriptionDiscount =========');
            logger(json_encode($e));
        }
    }
//    public function createDeliveryProfile($user_id, $ss_profile_id){
    //       try{
    //           logger('========= START:: createDeliveryProfile =========');
    //           $profile = SsShippingProfile::with('ShippingZones')->where('id', $ss_profile_id)->first()->toArray();
    //           $profileAction = ( $profile['shopify_profile_id'] ) ? 'deliveryProfileUpdate' : 'deliveryProfileCreate';
    //           $locationAction = ( $profile['shopify_location_group_id'] ) ? 'locationGroupsToUpdate' : 'locationGroupsToCreate';
    //           $user = User::find($user_id);
    //           $shop = Shop::select('currency')->where('user_id', $user_id)->first();
    //
    //           $this->getActiveLocations($user, '');
    //           $activeLocations = $this->locationIds;
    //
    //           $query = 'mutation MyMutation {
    //                    '. $profileAction .'(profile: { name: "'. $profile['name'] . '"';
    //
    //           $query .= ( $profile['shopify_profile_id'] ) ? 'id: "gid://shopify/DeliveryProfile/' . $profile['shopify_profile_id'] .'", ' : '';
    //
    //           $query .= ' sellingPlanGroupsToAssociate: ["gid://shopify/SellingPlanGroup/'. implode('","gid://shopify/SellingPlanGroup/', json_decode($profile['plan_group_ids'])) .'"],';
    //
    //           $query .=  $locationAction .': ';
    //
    //           foreach ( $activeLocations as $lkey=>$lval ){
    //               $query .=  '{
    //                        locations: "'. $lval .'",
    //                            zonesToCreate: [';
    //               foreach ( $profile['shipping_zones'] as $zkey=>$zval ) {
    //                   $country = implode(',', json_decode($zval['countries']));
    //                   if( $country == 'Rest of World' ){
    //                       $countries = '{ restOfWorld: true}';
    //                   }else{
    //                       $country = explode(',', $country);
    //                       $countries = '';
    //                       foreach ( $country as $ckey=>$cval ){
    //                           $countries .= "{code: $cval, includeAllProvinces: true}";
    //                       }
    //                   }
    //                   $zactive = ( $zval ) ? 'true' : 'false';
    //                   $query .= '{
    //                                name: "'. $zval['zone_name'] .'",
    //                                countries: ['.$countries.']
    //                                methodDefinitionsToCreate: {
    //                                    name: "'. $zval['rate_name'] .'",
    //                                    active: '. $zactive .',
    //                                    rateDefinition: {
    //                                        price: {
    //                                            amount: "'. $zval['rate_value'] .'",
    //                                            currencyCode: '.$shop['currency'].'
    //                                        }
    //                                    },
    //                                 }
    //                             }';
    //               }
    //               $query .=  ']';
    //           }
    //           $query .= '}) {
    //                    userErrors {
    //                      field
    //                      message
    //                    }
    //                  }
    //                }';
    //           dump($query);
    //           $result = $this->graphQLRequest($user->id, $query);
    //           dd($result);
    //       }catch( \Exception $e ){
    //          logger('========= ERROR:: createDeliveryProfile =========');
    ////          logger(json_encode($e));
    //           dd($e);
    //       }
    //    }

    public function getActiveLocations($user, $after)
    {
        try {
            logger('========= START:: getActiveLocations =========');
            $this->locationIds = [];
            $afterK = ($after) ? "after: " : '';
            $after = ($after) ? '"' . $after . '", ' : $after;
            $query = '{
              locations(first: 250, ' . $afterK . $after . 'includeInactive: false) {
                edges {
                  node {
                    id
                  }
                  cursor
                }
                pageInfo {
                  hasNextPage
                }
              }
            }';

            $result = $this->graphQLRequest($user->id, $query);

            if (!$result['errors']) {
                $locations = $result['body']->container['data']['locations'];

                $edges = $locations['edges'];
                foreach ($edges as $key => $value) {
                    $this->locationIds[] = $value['node']['id'];
                }

                if ($locations['pageInfo']['hasNextPage']) {
                    $after = end($locations['edges'])['cursor'] ?? null;
                    $this->getActiveLocations($user, $after);
                }
            } else {
                logger(json_encode($result));
            }
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            logger('========= ERROR:: getActiveLocations =========');
            logger(json_encode($e));
        }
    }

    public function calculateNextOrderDate($sellingPlan, $shop)
    {
      $orderDate = Carbon::now();
      
      logger("=======================  Order Date ======================");
      logger($orderDate);
      
      $deliveryCutOff = $sellingPlan->delivery_cutoff;
      $billingAnchorType = $sellingPlan->billing_anchor_type;
      $billingAnchoMonth = $sellingPlan->billing_anchor_month;
      $billingAnchorDay = $sellingPlan->billing_anchor_day;

      if ($billingAnchorType == 'WEEKDAY') {
        $range = CarbonPeriod::create($orderDate, 7);
        
        logger("======================= Weekday Order Range ======================");
        logger(json_encode($range));
        
        foreach ($range as $carbon) { //This is an iterator
          if ($carbon->dayOfWeekIso == $billingAnchorDay) {
              $nextOrderDate = $carbon;
              logger("======================= Next Order Date ======================");
              logger($nextOrderDate);
          }
        }

        if ($orderDate >= $nextOrderDate->copy()->subDays($deliveryCutOff)) {
          $nextOrderDate = $nextOrderDate->addDays(7);
          logger("======================= Next Order Date ======================");
          logger($nextOrderDate);
        }
      } elseif ($billingAnchorType == 'MONTHDAY') {
        $range = CarbonPeriod::create(Carbon::now(), Carbon::now()->addMonths(1));
        foreach ($range as $carbon) { 
          if ($carbon->day == $billingAnchorDay) {
              $nextOrderDate = Carbon::create($this->getSubscriptionTimeDate(date("Y-m-d",
              strtotime($carbon)), $shop->id)); 
          }
        }
        if ($orderDate >= $nextOrderDate->copy()->subDay($deliveryCutOff)) {
          $nextOrderDate = $this->getSubscriptionTimeDate(date("Y-m-d",
          strtotime($nextOrderDate->addMonth(1))), $shop->id);
        }
      } else {
        $range = CarbonPeriod::create(Carbon::now(), Carbon::now()->addYears(1));
        foreach ($range as $carbon) {
          if ($carbon->day == $billingAnchorDay && $carbon->month == $billingAnchoMonth) {
            $nextOrderDate = Carbon::create($this->getSubscriptionTimeDate(date("Y-m-d",
            strtotime($carbon)), $shop->id)); 
          }
        }

        if ($orderDate >= $nextOrderDate->copy()->subDay($deliveryCutOff)) {
          $nextOrderDate = $this->getSubscriptionTimeDate(date("Y-m-d",
          strtotime($nextOrderDate->addYears(1))), $shop->id);
        }
      }

      return $nextOrderDate;
    }
}
