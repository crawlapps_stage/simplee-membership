<?php
namespace App\Traits;

use Illuminate\Support\Facades\DB;

/**
 * Trait MigrateTrait
 * @package App\Traits
 */
trait MigrateTrait{
    use ShopifyTrait;

    public function migrateSubscriptions($user){
        try{
            logger("====================== START:: migrateSubscriptions ======================");

            $customer['email'] = 'ruchita12.crawlapps@gmail.com';
            $customer['first_name'] = 'Ruchi';
            $customer['last_name'] = 'Gelani';
            $customerID = $this->getCustomer($customer, $user);

            $subscription = [
                'next_billing_date' => '2020-06-01',
                'currencyCode' => 'USD',
                'paymentMethodId' => "869e7a39",
                'billingPolicy' => [
                    'interval'=> 'MONTH',
                    'intervalCount'=> 1,
                    'minCycles'=> 3
                ],
                'deliveryPolicy'=> [
                    'interval'=> 'MONTH',
                    'intervalCount'=> 1
                ],
                'deliveryMethodShippingAddress'=> [
                    'firstName'=> "John",
                    'lastName'=> "McDonald",
                    'address1'=> "33 New Montgomery St",
                    'address2'=> "#750",
                    'city'=> "San Francisco",
                    'provinceCode'=> "CA",
                    'countryCode'=> 'US',
                    'zip'=> "94105"
                ],
                'deliveryPrice'=> 14.99
            ];

            $this->createSubscriptionContract($subscription, $user, $customerID);
        }catch ( \Exception $e ){
            logger("====================== ERROR:: migrateSubscriptions ======================");
            logger(json_encode($e));
        }
    }
}
