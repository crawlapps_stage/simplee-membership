<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Shop;
use App\Models\SsContract;
use App\Models\SsContractLineItem;
use App\Models\SsDeletedProduct;
use App\Models\SsMetric;
use App\Models\SsOrder;
use App\Models\SsPlan;
use App\Traits\ShopifyTrait;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Osiset\ShopifyApp\Storage\Models\Plan;

class DashboardController extends Controller
{
    use ShopifyTrait;

    public function appBladeindex(Request $request)
    {
        try {
            $id = 0;
            $current_user = $this->getCurrentUser();
//            if( empty($current_user) ){
            //                return redirect('/login');
            //            }
            return view('layouts.app', compact('id', 'current_user'));
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function subscriptionsBladeindex(Request $request)
    {
        try {
            $contract = SsContract::where('shopify_contract_id', $request->id)->where('shopify_customer_id', $request->customer_id)->first();
            $id = ($contract) ? $contract->id : -1;
            $current_user = $this->getCurrentUser();
            return view('layouts.app', compact('id', 'current_user'));
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function plansBladeindex()
    {
        try {
            $id = -2;
            $current_user = $this->getCurrentUser();
            return view('layouts.app', compact('id', 'current_user'));
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function getCurrentUser()
    {
        $user = Auth::user();

        $shop = getShopH();
        $plan = Plan::find($user->plan_id);
        $current_user['user_id'] = $user->id;

        $current_user['hmac'] = hash_hmac('sha256', $user->id, config('const.HMAC_KEY'));
        $current_user['name'] = $shop->owner;
        $current_user['email'] = $shop->email;
        $current_user['created_at'] = $user->created_at;
        $current_user['myshopify_domain'] = $shop->myshopify_domain;
        $current_user['simplee_shop_id'] = $shop->id;
        $current_user['simplee_plans_created'] = SsPlan::where('user_id', $user->id)->where('shop_id', $shop->id)->where('status', 'active')->count();
        $current_user['simplee_subscriptions'] = SsContract::where('user_id', $user->id)->where('shop_id', $shop->id)->where('status', 'active')->count();
        $current_user['simplee_install_tried'] = 0;
        $current_user['simplee_plan'] = $plan->name;

        return $current_user;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $user = Auth::user();
            $shop = Shop::where('user_id', $user->id)->first();

            $active_subscription = SsContract::where('shop_id', $shop->id)->where('user_id', $user->id)->where('status', 'active')->count();
            $pause_subscription = SsContract::where('shop_id', $shop->id)->where('user_id', $user->id)->where('status', 'paused')->count();

            $default_timezone = date_default_timezone_get();
            $timezone = $shop->iana_timezone;

            date_default_timezone_set($shop->iana_timezone);
            $date = date("Y-m-d");

            $merchantTime = $this->getSubscriptionTimeDate(date("Y-m-d",
                strtotime($date)),
                $shop->id);
            $dateTime = new DateTime($merchantTime);
            $dateTime->modify("+24 hours");

            $AddedTime = date_format($dateTime, 'Y-m-d');
            $timeH = date_format($dateTime, 'H');

            $from = date('Y-m-d H', strtotime($merchantTime)) . ':00:00';
            $to = $AddedTime . ' ' . ($timeH - 1) . ':59:59';

            $new_subscription = SsContract::where('shop_id', $shop->id)->where('user_id', $user->id)->whereBetween('created_at', [$from, $to])->count();

            $today_order = SsOrder::where('shop_id', $shop->id)->where('user_id', $user->id)->whereBetween('created_at', [$from, $to])->count();

            $today_sales = SsOrder::where('shop_id', $shop->id)->where('user_id', $user->id)->whereBetween('created_at', [$from, $to])->sum('order_amount');

            $order = SsOrder::where('shop_id', $shop->id)->where('user_id', $user->id)->first();
            $deletedProduct = SsDeletedProduct::where('user_id', $user->id)->where('shop_id', $shop->id)->where('active', 1)->get()->toarray();

            if ($order) {
                $currencyS = $order->currency_symbol;
                $currency = preg_replace("/[A-Za-z]/", "", $currencyS);
            }

            $matrics = SsMetric::select('id', 'active_subscriptions', 'paused_subscriptions', 'orders_processed', 'amount_processed')->where('shop_id', $shop->id)->orderBy('created_at', 'desc')->take('30')->get();

            $data['matrics']['active'] = array_fill(0, 30, 0);
            $data['matrics']['paused'] = array_fill(0, 30, 0);
            $data['matrics']['orders'] = array_fill(0, 30, 0);
            $data['matrics']['amount'] = array_fill(0, 30, 0);

            if (count($matrics) > 0) {
                $matrics = $matrics->toArray();

                $actives = array_column($matrics, 'active_subscriptions');
                $paused = array_column($matrics, 'paused_subscriptions');
                $orders = array_column($matrics, 'orders_processed');
                $amount = array_column($matrics, 'amount_processed');

                $data['matrics']['active'] = $this->fillArray($actives);
                $data['matrics']['paused'] = $this->fillArray($paused);
                $data['matrics']['orders'] = $this->fillArray($orders);
                $data['matrics']['amount'] = $this->fillArray($amount);

            }
            $data['today_sales'] = ($order) ? $currency . $today_sales . ' ' . $order->order_currency : 0;
            $data['today_order'] = $today_order;
            $data['new_subscription'] = $new_subscription;
            $data['active_subscription'] = $active_subscription;
            $data['paused_subscription'] = $pause_subscription;
            $data['currency'] = $shop->currency;
            $data['currency_symbol'] = $shop->currency_symbol;
//            $data['matrics']['active'] = ;
            // $data['deletedProducts'] = $this->getShopifyVariant($deletedProduct, $user->id);

            date_default_timezone_set($default_timezone);
            return response()->json(['data' => $data], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param $deletedProduct
     * @param $userID
     * @return array
     */
    public function getShopifyVariant($deletedProduct, $userID)
    {
        $res = [];
        if (!empty($deletedProduct)) {
            foreach ($deletedProduct as $key => $val) {
                // fetch product
                $endPoint = '/admin/products/' . $val['shopify_product_id'] . '.json';
                $parameter['fields'] = 'id,title';
                $result = $this->request('GET', $endPoint, $parameter, $userID);
                $res[$key]['display_name'] = '';
                if (!$result['errors']) {
                    $sh_variant = $result['body']->container['product'];
                    $res[$key]['display_name'] = $sh_variant['title'];
                    $res[$key]['subscriptions_impacted'] = $val['subscriptions_impacted'];
                    $res[$key]['id'] = $val['id'];
                } else {
                    $res[$key]['display_name'] = '#' . $val['shopify_product_id'];
                    $res[$key]['subscriptions_impacted'] = $val['subscriptions_impacted'];
                    $res[$key]['id'] = $val['id'];
                }

                if ($val['shopify_variant_id']) {
                    $endPoint = '/admin/variants/' . $val['shopify_variant_id'] . '.json';
                    $parameter['fields'] = 'id,title';
                    $result = $this->request('GET', $endPoint, $parameter, $userID);
                    if (!$result['errors']) {
                        $sh_variant = $result['body']->container['variant'];
                        $res[$key]['display_name'] = $res[$key]['display_name'] . '/' . $sh_variant['title'];
                    }
                }
            }
        }
        return $res;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function replaceLineItem(Request $request)
    {
        try {
            $user = Auth::user();
            $shop = Shop::where('user_id', $user->id)->first();
            $data = $request->data;
            $deleted_id = $data['deleted_id'];
            $new_resource = $data['resource'][0];

            $deletedProduct = SsDeletedProduct::where('id', $deleted_id)->where('user_id', $user->id)->first();
            $lineItems = SsContractLineItem::where('user_id', $user->id)->where('shopify_product_id', $deletedProduct->shopify_product_id)->where('shopify_variant_id', $deletedProduct->shopify_variant_id)->get();

            if (count($lineItems) > 0) {
                foreach ($lineItems as $key => $val) {
//                    $result = $this->subscriptionDraftLineRemove($user->id, $val);

//                    if( $result == 'success' ){
                    $val->shopify_product_id = $new_resource['product_id'];
                    $val->shopify_variant_id = $new_resource['variant_id'];
                    $val->price = $new_resource['price'];

                    if ($val->discount_type == $shop->currency_symbol) {
                        $final_amt = number_format(($new_resource['price'] - $val->discount_amount), 2);
                    } elseif ($val->discount_type == '%') {
                        $nwprice = $new_resource['price'];
                        $cal_price = ($nwprice * $val->quantity);
                        $final_amt = number_format(($cal_price - (($cal_price * $val->discount_amount) / 100)), 2);
                    } else {
                        $final_amt = number_format($new_resource['price'], 2);
                    }
                    $val->final_amount = $final_amt;
                    $val->save();
//                    }else{
                    //                        logger($result);
                    //                    }

                    $result = $this->subscriptionDraftLineUpdate($user->id, $val);
                }
            }
            $deletedProduct->active = 0;
            $deletedProduct->save();
            return response()->json(['data' => 'Product updated successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param $data
     */
    public function fillArray($data)
    {
        if (count($data) < 30) {
            $len = count($data);
            $diff = 30 - $len;
            $diffFill = array_fill(0, $diff, 0);
            return array_merge($diffFill, array_reverse($data));
        }
        return array_reverse($data);
    }
}
