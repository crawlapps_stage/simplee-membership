<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use App\Models\SsCustomer;
use App\User;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Response;
use Osiset\ShopifyApp\Objects\Values\NullableShopDomain;
use function Osiset\ShopifyApp\createHmac;
use function Osiset\ShopifyApp\parseQueryString;
use App\Http\Controllers\Subscriber\SubscriberController;

class PortalController extends Controller
{

    public function index( Request $request){
        try{
            $data['shop'] = $request->shop;
             return Response()->view('portal.index', compact('data'), 200)->withHeaders([
                'Content-Type'=>'application/liquid',
            ]);
        }catch( \Exception $e ){
            dd($e);
        }
    }
}
