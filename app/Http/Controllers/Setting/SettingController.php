<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\PlanRequest;
use App\Models\SsEmail;
use App\Models\SsSetting;
use App\Traits\ImageTrait;
use App\Traits\ShopifyTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Log;
use Osiset\ShopifyApp\Storage\Models\Charge;
use Osiset\ShopifyApp\Storage\Models\Plan;

class SettingController extends Controller
{
    use ShopifyTrait;
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $shopID = get_shopID_H();
            $shop = getShopH();
            $metafields = getShopMetaFields();

            $setting = SsSetting::where('shop_id', $shopID)->first();

            $failedPaymentNoti = SsEmail::where('shop_id', $shopID)->where('category', 'failed_payment_to_customer')->first();
            $newSubNoti = SsEmail::where('shop_id', $shopID)->where('category', 'new_subscription_to_customer')->first();

            $charge = Charge::where('user_id', $shop->user_id)->where('status', 'ACTIVE')->orderBy('created_at', 'desc')->first();

            $plans = Plan::select('name', 'price', 'transaction_fee')->get()->toArray();

            $portal = SsSetting::where('shop_id', $shopID)->select(['portal_can_cancel', 'portal_can_skip', 'portal_can_change_qty'])->first()->toArray();

            $curr_date = date('Y-m-d H:i:s');
            $trial_end_date = date("Y-m-d H:i:s", strtotime($charge->trial_ends_on));

            // $new = array_column($metafields, 'value', 'key');
            foreach ($metafields as $key => $field) {
                Log::info(print_r($field, true));
                $settingKey = $field['key'];
                $metaIDKey = $field['key'] . '_mi';
                $setting[$settingKey] = $field['value'];
                $setting[$metaIDKey] = $field['id'];
            }

            $data['setting'] = $setting;
            $data['failedPaymentNoti'] = $failedPaymentNoti;
            $data['newSubNoti'] = $newSubNoti;
            $data['timezone'] = $shop->iana_timezone;
            $data['portal'] = $portal;
            $data['plan']['active_plan_id'] = $charge->plan_id;
            $data['plan']['trial_ends'] = ($curr_date > $trial_end_date) ? '' : strtoupper(date("M d, Y", strtotime($charge->trial_ends_on)));
            $data['plan']['current_txn_fee'] = number_format($setting->transaction_fee * 100, 2);;
            $data['plans'] = $plans;

            $response['data'] = $data;
            $response['themes'] = $this->getThemes();

            return response()->json(['data' => $response], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePortal(Request $request)
    {
        try {
            $shopID = get_shopID_H();
            $key = $request->key;
            $customerP = SsSetting::where('shop_id', $shopID)->first();
            $customerP->$key = $request->v;
            $customerP->save();

            return response()->json(['data' => 'Status Changed successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * store settings
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $shopID = get_shopID_H();
            $shop = getShopH();
//            save settings
            $data = $request->data['setting'];

            $setting = SsSetting::where('shop_id', $shopID)->first();

            $setting->dunning_retries = $data['dunning_retries'];
            $setting->dunning_daysbetween = $data['dunning_daysbetween'];
            $setting->dunning_failedaction = $data['dunning_failedaction'];

            if (($setting->dunning_email_enabled == 0) && ($data['dunning_email_enabled'])) {
                $this->event($shop->user_id, 'Onboarding', 'Dunning Email Enabled', '
                                Merchant enabled their dunning email');
            }
            $setting->dunning_email_enabled = $data['dunning_email_enabled'];
            $setting->new_subscription_email_enabled = $data['new_subscription_email_enabled'];
            $setting->email_from_name = $data['email_from_name'];
            $setting->email_from_email = $data['email_from_email'];
            $setting->subscription_daily_at = $data['subscription_daily_at'];

            $setting->mailgun_method = $data['mailgun_method'];
            // $setting->tag_customer = $data['tag_customer'];
            // $setting->tag_order = $data['tag_order'];
            $setting->save();

            $user = Auth::user();

            if ($data['widget_active_bg'] && $data['widget_active_bg_mi']) {
                $payload = [
                    "metafield" => [
                        "id" => $data['widget_active_bg_mi'],
                        "value" => $data['widget_active_bg'],
                        "value_type" => "string",
                    ],
                ];
                $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/metafields/' . $data['widget_active_bg_mi'] . '.json';
                $response = $user->api()->rest('PUT', $endPoint, $payload);
                if (!$response['errors']) {
                }
            }
            if ($data['widget_active_text'] && $data['widget_active_text_mi']) {
                $payload = [
                    "metafield" => [
                        "id" => $data['widget_active_text_mi'],
                        "value" => $data['widget_active_text'],
                        "value_type" => "string",
                    ],
                ];
                $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/metafields/' . $data['widget_active_text_mi'] . '.json';
                $response = $user->api()->rest('PUT', $endPoint, $payload);
                if (!$response['errors']) {
                }
            }
            if ($data['widget_inactive_bg'] && $data['widget_inactive_bg_mi']) {
                $payload = [
                    "metafield" => [
                        "id" => $data['widget_inactive_bg_mi'],
                        "value" => $data['widget_inactive_bg'],
                        "value_type" => "string",
                    ],
                ];
                $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/metafields/' . $data['widget_inactive_bg_mi'] . '.json';
                $response = $user->api()->rest('PUT', $endPoint, $payload);
                if (!$response['errors']) {
                }
            }
            if ($data['widget_inactive_text'] && $data['widget_inactive_text_mi']) {
                $payload = [
                    "metafield" => [
                        "id" => $data['widget_inactive_text_mi'],
                        "value" => $data['widget_inactive_text'],
                        "value_type" => "string",
                    ],
                ];
                $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/metafields/' . $data['widget_inactive_text_mi'] . '.json';
                $response = $user->api()->rest('PUT', $endPoint, $payload);
                if (!$response['errors']) {
                }
            }
            if ($data['widget_heading_text'] && $data['widget_active_text']) {
                $payload = [
                    "metafield" => [
                        "id" => $data['widget_heading_text_mi'],
                        "value" => $data['widget_heading_text'],
                        "value_type" => "string",
                    ],
                ];
                $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/metafields/' . $data['widget_heading_text_mi'] . '.json';
                $response = $user->api()->rest('PUT', $endPoint, $payload);
                if (!$response['errors']) {
                }
            }
            if ($data['widget_one_time_text'] && $data['widget_one_time_text_mi']) {
                $payload = [
                    "metafield" => [
                        "id" => $data['widget_one_time_text_mi'],
                        "value" => $data['widget_one_time_text'],
                        "value_type" => "string",
                    ],
                ];
                $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/metafields/' . $data['widget_one_time_text_mi'] . '.json';
                $response = $user->api()->rest('PUT', $endPoint, $payload);
                if (!$response['errors']) {
                }
            }
            if ($data['widget_default_selection'] && $data['widget_default_selection_mi']) {
                $payload = [
                    "metafield" => [
                        "id" => $data['widget_default_selection_mi'],
                        "value" => $data['widget_default_selection'],
                        "value_type" => "string",
                    ],
                ];
                $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/metafields/' . $data['widget_default_selection_mi'] . '.json';
                $response = $user->api()->rest('PUT', $endPoint, $payload);
                if (!$response['errors']) {
                }
            }

//            save email
            //            $data = $request->data['email'];
            //            $email = SsEmail::where('shop_id', $shopID)->where('category', 'failed_payment_to_customer')->first();
            //            $email->subject = $data['subject'];
            //            $email->html_body = $data['html_body'];
            //            $email->save();

//            save portal
            //            $data = $request->data['portal'];
            //            $portal = SsSetting::where('shop_id', $shopID)->first();
            //            $portal->portal_content = $data['portal_content'];
            //            $portal->save();
            DB::commit();
            return response()->json(['data' => 'Saved!'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * Send test mail
     * @param  PlanRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendMail(PlanRequest $request)
    {
        try {
            $shopID = get_shopID_H();
            $setting = SsSetting::where('shop_id', $shopID)->first();
            $data = $request->data;

            $res = sendMailH($data['subject'], $data['html_body'], $setting->email_from_email, $data['mailto'], $setting->email_from_name, $shopID, '');

            $msg = ($res == 'success') ? 'Test email sent successfully' : $res;
            return response()->json(['data' => $msg], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param  PlanRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function emailBody(PlanRequest $request)
    {
        try {
            $shopID = get_shopID_H();
            $data = $request->data;
            $email = SsEmail::where('shop_id', $shopID)->where('category', $data['category'])->first();
            $email->subject = $data['subject'];
            $email->html_body = $data['html_body'];
            $email->save();
            return response()->json(['data' => 'Saved!'], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadImage(Request $request)
    {
        try {
            $image = ImageTrait::makeImage($request->image, 'uploads/mail');
            $url = Storage::disk('public')->url('uploads/mail/') . $image;
            return response()->json(['data' => $url], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    public function installTheme(Request $request)
    {
        try {
            $user = Auth::user();
            installThemeH($request['data']['id'], $user->id);
            return response()->json(['data' => 'Theme installed successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }
}
