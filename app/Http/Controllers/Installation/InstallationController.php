<?php

namespace App\Http\Controllers\Installation;

use App\Http\Controllers\Controller;
use App\Models\SsThemeInstall;
use Illuminate\Http\Request;
use App\Traits\ShopifyTrait;

class InstallationController extends Controller
{
    use ShopifyTrait;
    public function index(){
        $shop = getShopH();
        $response['eligibleForSubscriptions'] = $this->shopFeature($shop->user_id);
        $response['themes'] = $this->getThemes();
        return response()->json(['data' => $response], 200);
    }

    public function installWidget(Request $request){
        try{
            $shop = getShopH();
            $data = $request->data;
            $schema = $this->getThemeSchema($shop->user_id, $data['id']);

            if( $schema['status']){
                $schema = $schema['data'][0];

                $themeInstall = new SsThemeInstall;
                $themeInstall->shop_id = $shop->id;
                $themeInstall->theme_id =  $data['id'];
                $themeInstall->theme_name =  $schema->theme_name;
                $themeInstall->theme_version =  $schema->theme_version;
                $themeInstall->theme_author =  $schema->theme_author;
                $themeInstall->install_status = 'started';
                $themeInstall->save();

                $data['version'] = $schema->theme_version;

                installWidgetH($data, $shop->user_id, 'default');

                $themeInstall->install_status = 'files created';
                $themeInstall->save();

                updateFilesH($data, $shop->user_id);
                return response()->json(['data' => 'Theme was updated', 'status' => true], 200);
            }else
                return response()->json(['data' => $schema['data'], 'status' => false], 200);

        }catch( \Exception $e ){
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }
}
