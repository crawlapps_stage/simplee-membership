<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Traits\WebhookTrait;
use Illuminate\Http\Request;
use App\Traits\ShopifyTrait;
use App\User;
use App\Models\Shop;

class WebhookController extends Controller
{
     use WebhookTrait;
    public function index(Request $request){
        try{
            logger("=========== START:: webhook index ===========");
            $this->webhookIndex($request);

            return response()->json(['data' => 'success'], 200);
        }catch( \Exception $e ){
            logger("=========== ERROR:: webhook index ===========");
            logger(json_encode($e));
        }
    }
}

