<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Subscriber\SubscriberController;
use App\Models\Shop;
use App\Models\SsContract;
use App\Models\SsLanguage;
use App\Models\SsSetting;
use App\Models\SsCustomer;
use App\Traits\ShopifyTrait;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class PortalController extends Controller
{
    use ShopifyTrait;
    public function index(Request $request)
    {
        try {
            $customer_id = $request->customer;
            $is_initLoad = (bool) $request->init_load;

            $initcustomer = SsCustomer::select('id', 'shop_id', 'email', 'phone')->where('shopify_customer_id', $customer_id)->first();
            if ($is_initLoad) {

                if ($initcustomer) {
                    $shop = Shop::find($initcustomer->shop_id);
                    $this->saveActivity($shop->user_id, $initcustomer->id, null, 'customer', 'Customer logged into the customer portal');
                }
            }

            $subscriber = ($request->contract == '') ? SsContract::with('LineItems')->where('shopify_customer_id', $customer_id)->orderBy('created_at', 'desc')->first() : SsContract::with('LineItems')->where('shopify_customer_id', $customer_id)->where('id', $request->contract)->first();
            $subscriberC = new SubscriberController();

            $data['contract'] = [];
            $data['otherContracts'] = [];
            $data['languages'] = [];
            if ($subscriber) {
                $user_id = $subscriber->user_id;
                $shop_id = $subscriber->shop_id;
                $shop = Shop::find($shop_id);
                $user = User::find($user_id);

                $id = $subscriber->id;
                $next = SsContract::select('id')->where('id', '>', $id)->orderBy('id')->first();
                $previous = SsContract::select('id')->where('id', '<', $id)->orderBy('id', 'desc')->first();

                $nextOD = $this->getSubscriptionTimeDate(date('Y-m-d', strtotime($subscriber->next_order_date)), $shop->id, date('H:i:s', strtotime($subscriber->next_order_date)));
                $nextProcessD = $this->getSubscriptionTimeDate(date('Y-m-d', strtotime($subscriber->next_processing_date)), $shop->id, date('H:i:s', strtotime($subscriber->next_processing_date)));
                $subscriber->next_order_date = date('M d, Y', strtotime($nextOD));
                $subscriber->next_processing_date = date('M d, Y', strtotime($nextProcessD));

                $data['contract'] = $subscriber;
                $data['contract']['lineItems'] = (!empty($subscriber['LineItems'])) ? $subscriberC->getLineItemsData($subscriber, $user) : [];
                $data['contract']['fulfillmentOrders'] = ($subscriber['last_billing_order_number']) ? $subscriberC->getFulfillments($user->id, $subscriber['last_billing_order_number']) : [];
                $data['shop']['domain'] = $shop->myshopify_domain;
                $data['shop']['currency'] = $shop->currency_symbol;
                $data['contract']['prev'] = ($previous) ? '/subscriber/' . $previous->id . '/edit?page=1' : null;
                $data['contract']['next'] = ($next) ? '/subscriber/' . $next->id . '/edit?page=1' : null;
                $data['otherContracts'] = SsContract::select('id', 'status', 'shopify_contract_id')->where('shop_id', $shop_id)->where('shopify_customer_id', $customer_id)->orderBy('created_at', 'desc')->get()->toArray();

                $data['settings'] = SsSetting::where('shop_id', $shop_id)->select(['portal_can_cancel', 'portal_can_skip', 'portal_can_ship_now', 'portal_can_change_qty', 'portal_can_change_nod', 'portal_can_change_freq', 'portal_can_add_product', 'portal_show_content', 'portal_content'])->first();
                $data['customer'] = ($initcustomer) ? $initcustomer : [];

                $orderFields = 'id, current_subtotal_price, current_total_price, currency, shipping_lines';
                $data['order'] = $this->getShopifyOrder($user, $subscriber->origin_order_id, $orderFields);
                $subscriber->unsetRelation('LineItems');

                $lang = SsLanguage::where('shop_id', $shop_id)->first();
                $data['languages'] = ($lang) ? $lang : [];
            }
            $data['images']['img'] = asset('images/static/cards');
            $data['images']['no_img'] = asset('images/static/no-image-box.png');

            return response()->json(['data' => $data], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    public function store(Request $request)
    {
        try {
            $subscriberC = new SubscriberController();
            $result = $subscriberC->update($request);
            return response()->json($result, 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }
}
