<?php

namespace App\Http\Controllers\Migrate;

use App\Http\Controllers\Controller;
use App\Traits\MigrateTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MigrateController extends Controller
{
    use MigrateTrait;
    public function index()
    {
       try{
           $user = Auth::user();
            $this->migrateSubscriptions($user);
       }catch( \Exception $e ){

       }
    }
}
