<?php

namespace App\Http\Controllers\Subscriber;

use Carbon\Carbon;
use App\Exports\SubscribersExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubscriberRequest;
use App\Http\Resources\SubscriptionResource;
use App\Models\Shop;
use App\Models\SsContract;
use App\Models\SsContractLineItem;
use App\Models\SsOrder;
use App\Models\SsCustomer;
use App\Traits\ShopifyTrait;
use App\User;
use Carbon\CarbonPeriod;
// use Bugsnag\DateTime\Date;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\Date;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Response;

class SubscriberController extends Controller
{
    use ShopifyTrait;

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $shopID = get_shopID_H();
            $shop = getShopH();
            $s = $request->s;
            $f = $request->f;

            if ($f == 'all') {
                $subscriber = SsContract::select('ss_contracts.id', 'ss_contracts.shop_id', 'ss_contracts.ss_customer_id', 'ss_contracts.status', 'next_order_date', 'ss_contracts.order_count', 'ss_customers.first_name', 'ss_customers.last_name', 'ss_customers.email', 'ss_customers.phone', 'ss_customers.date_first_order', 'ss_contracts.created_at')->join('ss_customers', 'ss_contracts.ss_customer_id', '=', 'ss_customers.id')->where(function ($query) use ($s) {
                    $query->where('ss_contracts.status', 'LIKE', '%' . $s . '%')->orWhere('order_count', 'LIKE', '%' . $s . '%')->orWhere('first_name', 'LIKE', '%' . $s . '%')->orWhere('last_name', 'LIKE', '%' . $s . '%')->orWhere('email', 'LIKE', '%' . $s . '%')->orWhere('phone', 'LIKE', '%' . $s . '%');
                })->where('ss_contracts.shop_id', $shopID)->where('ss_customers.shop_id', $shopID)->orderBy('ss_contracts.created_at', 'desc')->paginate(10);

            } else {
                $subscriber = SsContract::select('ss_contracts.id', 'ss_contracts.shop_id', 'ss_contracts.ss_customer_id', 'ss_contracts.shopify_contract_id', 'ss_contracts.status', 'next_order_date', 'ss_contracts.order_count', 'ss_customers.first_name', 'ss_customers.last_name', 'ss_customers.email', 'ss_customers.phone', 'ss_customers.date_first_order', 'ss_contracts.created_at')->join('ss_customers', 'ss_contracts.ss_customer_id', '=', 'ss_customers.id')
                    ->where(function ($query) use ($f) {
                        $query->where('ss_contracts.status', 'LIKE', '%' . $f . '%');
                    })
                    ->where(function ($query) use ($s) {
                        $query->Where('order_count', 'LIKE', '%' . $s . '%')->orWhere('shopify_contract_id', 'LIKE', '%' . $s . '%')->orWhere('first_name', 'LIKE', '%' . $s . '%')->orWhere('last_name', 'LIKE', '%' . $s . '%')->orWhere('email', 'LIKE', '%' . $s . '%')->orWhere('phone', 'LIKE', '%' . $s . '%');
                    })
                    ->where('ss_contracts.shop_id', $shopID)->where('ss_customers.shop_id', $shopID)->orderBy('ss_contracts.created_at', 'desc')->paginate(10);
            }

            $res = paginateH($subscriber);
            $res['data'] = SubscriptionResource::collection($subscriber);

            return response()->json(['data' => $res], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id, Request $request)
    {
        try {
            $shop = getShopH();
            $user = User::where('id', $shop->user_id)->first();
            $user = Auth::user();

//            $subscriber = SsCustomer::with('ActivityLog')->select('id', 'shopify_customer_id', 'first_name', 'last_name',
            //                'active', 'phone', 'email', 'total_orders', 'total_spend', 'total_spend_currency', 'notes',
            //                'avg_order_value')->where('shop_id', $shop['id'])->where('id', $id)->first();
            $subscriber = SsContract::with('ActivityLog', 'LineItems', 'Customer', 'BillingAttempt')->where('shop_id', $shop['id'])->where('id', $id)->first();

            $next = SsContract::select('id')->where('id', '>', $id)->where('shop_id', $shop['id'])->orderBy('id')->first();
            $previous = SsContract::select('id')->where('id', '<', $id)->where('shop_id', $shop['id'])->orderBy('id', 'desc')->first();

            $data['contract'] = [];
            if ($subscriber) {

                $nextOD = $this->getSubscriptionTimeDate(date('Y-m-d', strtotime($subscriber->next_order_date)), $shop->id, date('H:i:s', strtotime($subscriber->next_order_date)));
                $data['availableDates'] = [];

                // Check if anchor day
                $anchorDateType = $subscriber->billing_anchor_type;

                if (isset($anchorDateType)) {
                  $anchorDay = $subscriber->billing_anchor_day;
                  $anchorMonth = $subscriber->billing_anchor_month;
                  $data['availableDates'] = $this->getAvailableDates($anchorDateType, $anchorDay, $anchorMonth, $subscriber->next_order_date);
                  $data['contract']['anchorDay'] = $anchorDay;
                  $data['contract']['anchorMonth'] = $anchorMonth;
                  $data['contract']['anchorDateType'] = $anchorDateType;
                }

                $subscriber->next_order_date = date('M d, Y', strtotime($nextOD));
                // $subscriber->next_processing_date = date('M d, Y H:i', strtotime($this->getSubscriptionTimeDate(date('Y-m-d', strtotime($subscriber->next_processing_date)), $shop->id, date('H:i:s', strtotime($subscriber->next_processing_date)))));
                $subscriber->next_processing_date = date('M d, Y H:i', strtotime($this->getSubscriptionTimeDate(date('Y-m-d', strtotime($subscriber->next_processing_date)), $shop->id, date("H:i:s", strtotime($subscriber->next_processing_date)), 'UTC')));

                $subscriber->customer->date_first_order = date('M d, Y', strtotime($this->getSubscriptionTimeDate(date('Y-m-d', strtotime($subscriber->customer->date_first_order)), $shop->id, date('H:i:s', strtotime($subscriber->customer->date_first_order)))));

                $data['contract'] = $subscriber;
                // $data['contract']['lineItems'] = (!empty($subscriber['LineItems'])) ? $subscriber['LineItems'] : [];
                $data['contract']['lineItems'] = (!empty($subscriber['LineItems'])) ? $this->getLineItemsData($subscriber, $user) : [];
                $data['contract']['activityLog'] = $this->getActivity($subscriber['ActivityLog'], $shop->id, $shop->iana_timezone);
                $data['contract']['billingAttempt'] = $this->billingAttempt($subscriber->BillingAttempt()->paginate(5, ['*'], 'page', $request->page), $shop->id);
                $data['contract']['fulfillmentOrders'] = ($subscriber->last_billing_order_number) ? $this->getFulfillments($user->id, $subscriber->last_billing_order_number) : [];
                $data['contract']['discount'] = $this->getSubscriptionDiscount($user->id, $subscriber->shopify_contract_id);


                $data['otherContracts'] = $this->getOtherContracts($subscriber);
                $subscriber->unsetRelation('ActivityLog');
                $subscriber->unsetRelation('LineItems');
                $subscriber->unsetRelation('BillingAttempt');

                $data['contract']['prev'] = ($previous) ? '/subscriber/' . $previous->id . '/edit?page=1' : null;
                $data['contract']['next'] = ($next) ? '/subscriber/' . $next->id . '/edit?page=1' : null;
            }
//            $data['customer'] = $this->orderSubscriber($subscriber, 'all', $user);
            $data['shop']['domain'] = $shop['myshopify_domain'];
            $data['shop']['currency'] = $shop->currency_symbol;

            return response()->json(['data' => $data], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
            $result = 'success';
            $data = $request->data;

            if (@$data['mode'] == 'api') {
                $domain = $data['shop'];
                $user = User::where('name', $domain)->first();
                $shop = Shop::where('user_id', $user->id)->first();
                $user_type = 'customer';
            } else {
                $shop = getShopH();
                $user_type = 'user';
            }

            $Contract = SsContract::find($data['contract_id']);

            if (@$data['type'] == 'all' || @$data['type'] == 'remove_lineitem' || @$data['type'] == 'portal_all') {
                $deleted = array_filter($data['deleted']);

                $lineitems = ($data['type'] == 'all' || $data['type'] == 'portal_all') ? $data['line_items'] : [];
                if (!empty($deleted)) {
                    foreach ($deleted as $key => $val) {
                        $lineitem = SsContractLineItem::find($val);
                        $result = $this->subscriptionDraftLineRemove($shop->user_id, $lineitem);
                        ($result == 'success') ? SsContractLineItem::find($val)->delete() : '';
                    }
                }
                if ($data['type'] == 'all' || $data['type'] == 'portal_all') {
                    if (!empty($lineitems)) {
                        foreach ($lineitems as $key => $val) {
                            $lineitem = SsContractLineItem::where('id', $val['id'])->first();

                            if ($val['selected_variant'] != $val['shopify_variant_id']) {
                                foreach ($val['sh_variants'] as $skey => $svalue) {
                                    if ($svalue['id'] == $val['selected_variant']) {
                                        $selected_variant = $svalue;
                                        break;
                                    }
                                }
                                if (!empty($selected_variant)) {
                                    $lineitem->shopify_variant_id = $selected_variant['id'];
                                    $lineitem->shopify_variant_title = $selected_variant['title'];
                                    $lineitem->sku = $selected_variant['sku'];
                                    $lineitem->price = $selected_variant['price'];
                                }

                            }
                            $lineitem->quantity = $val['quantity'];
                            $lineitem->discount_amount = $val['discount_amount'];
                            $lineitem->discount_type = $val['discount_type'];
                            $lineitem->final_amount = $val['final_amount'];
                            $lineitem->price_discounted = $val['final_amount'];
                            $lineitem->save();

                            $result = $this->subscriptionDraftLineUpdate($shop->user_id, $lineitem);
                        }
                    }

                    if ($data['type'] == 'all') {
                        $contract = SsContract::find($data['contract_id']);
                        $contract->prepaid_renew = $data['prepaid_renew'];
                        $contract->save();
                        //                save customer note
                        $subscriber = SsCustomer::find($data['customer_id']);
                        $subscriber->update(['notes' => $data['note']]);
                        $subscriber->save();
                    }
                }

                $msg = 'Subscription saved';
            } elseif ($data['type'] == 'paused' || $data['type'] == 'resumed' || $data['type'] == 'cancelled') {
                $Contract->update(['status' => ($data['type'] == 'resumed') ? 'active' : $data['type']]);
                $Contract->save();
                $msg = 'Subscription ' . $data['type'] . ' successfully';

                $activityMsg = ($user_type == 'user') ? 'Subscription was ' . $data['type'] . ' manually' : 'Customer ' . $data['type'] . ' their subscription';
                $this->saveActivity($shop->user_id, $Contract->ss_customer_id, $Contract->id, $user_type, $activityMsg);

            } elseif (@$data['type'] == 'reactive') {
                $next_date = $this->getSubscriptionTimeDate(date("Y-m-d", strtotime($data['reactiveDate'])), $shop->id);
                $Contract->update([
                    'status' => 'active', 'next_order_date' => $next_date, 'next_processing_date' => $next_date,
                ]);
                $Contract->save();
                $msg = 'Subscription active successfully';

                if ($user_type == 'user') {
                    $this->saveActivity($shop->user_id, $Contract->ss_customer_id, $Contract->id, $user_type, 'Subscription was reactivated manually');
                }
            } elseif (@$data['type'] == 'edit_shipping_address') {
                $Contract->update([
                    'ship_firstName' => $data['contract']['ship_firstName'],
                    'ship_lastName' => $data['contract']['ship_lastName'],
                    'ship_company' => $data['contract']['ship_company'],
                    'ship_address1' => $data['contract']['ship_address1'],
                    'ship_address2' => $data['contract']['ship_address2'],
                    'ship_city' => $data['contract']['ship_city'],
                    'ship_province' => $data['contract']['ship_province'],
                    'ship_provinceCode' => $data['contract']['ship_provinceCode'],
                    'ship_zip' => $data['contract']['ship_zip'],
                    'ship_country' => $data['contract']['ship_country'],
                ]);
                $Contract->save();
                $msg = 'Address updated successfully';

                if ($user_type == 'user') {
                    $this->saveActivity($shop->user_id, $Contract->ss_customer_id, $Contract->id, $user_type, 'Subscription’s shipping address was updated manually');
                }
            } elseif (@$data['type'] == 'skip_next_order') {

                $next_order_date = $Contract->next_order_date;
                $interval_count = $Contract->delivery_interval_count;
                $interval = $Contract->delivery_interval;

                $new_next_order = date('Y-m-d', strtotime($next_order_date . ' + ' . $interval_count . ' ' . $interval . 's'));

                $time = date('H:i:s', strtotime($next_order_date));
                $dateInMerchatTime = $this->getSubscriptionTimeDate(date("Y-m-d",
                    strtotime($new_next_order)), $shop->id);

                $saveDate = date('Y-m-d', strtotime($new_next_order)) . ' ' . date('H:i:s', strtotime($dateInMerchatTime));
                $Contract->update([
                    'next_order_date' => $saveDate,
                    'next_processing_date' => $saveDate,
                ]);
                $Contract->save();
                $msg = 'Skip order successfully';

                if ($user_type == 'customer') {
                    $this->saveActivity($shop->user_id, $Contract->ss_customer_id, $Contract->id, $user_type, 'Customer skipped their next order');
                }
            } elseif( @$data['type'] == 'next_order_date' ) {
                $merchant_next_date = $this->getSubscriptionTimeDate(date("Y-m-d",
                    strtotime($data['contract']['next_order_date'])), $shop->id);

                $Contract->update([
                    'next_order_date' => $merchant_next_date,
                    'next_processing_date' => $merchant_next_date,
                    'error_state' => null,
                    'failed_payment_count' => 0,
                ]);
                $Contract->save();

                $activityDate = date("Y-m-d", strtotime($this->getSubscriptionTimeDate(date('Y-m-d', strtotime($Contract->next_order_date)), $shop->id, date("H:i:s", strtotime($Contract->next_order_date)), 'UTC')));
                $activityMsg = 'Updated next billing date to ' . $activityDate;
//                $this->saveActivity($shop->user_id, $data['customer_id'], $data['contract_id'], 'user', $activityMsg);

                if ($user_type == 'user') {
                    $this->saveActivity($shop->user_id, $Contract->ss_customer_id, $Contract->id, $user_type, 'Subscription’s next order date was set to ' . $activityDate . ' manually');
                }
            } elseif (@$data['type'] == 'updatePaymentDetailEmail') {
                $result = $this->createCustomerPaymentMethodSendUpdateEmail($shop->user_id, $Contract->shopify_contract_id);
                if ($user_type == 'user') {
                    $this->saveActivity($shop->user_id, $Contract->ss_customer_id, $Contract->id, $user_type, 'Email was sent to the customer to reset their payment information');
                }
            }

            // update contract in shopify
            if ($data['type'] == 'paused' || $data['type'] == 'resumed' || $data['type'] == 'cancelled' || @$data['type'] == 'reactive' || @$data['type'] == 'edit_shipping_address') {
                $update = (@$data['type'] == 'edit_shipping_address') ? 'address' : 'status';
                $result = $this->updateSubscriptionContract($shop->user_id, $data['contract_id'], $update);
            }

            if ($data['type'] == 'skip_next_order' || $data['type'] == 'next_order_date' || @$data['type'] == 'reactive') {
                $result = $this->subscriptionContractSetNextBillingDate($shop->user_id, $Contract->shopify_contract_id);
            }

            if ($result != '') {
                if ($result == 'success') {
                    DB::commit();
                    $msg = (@$data['type'] == 'updatePaymentDetailEmail') ? 'Email sent' : 'Subscription updated';
                    $success = true;
                } else if (!$result) {
                    DB::rollBack();
                    $msg = 'Error - please try again';
                    $success = false;
                } else {
                    DB::rollBack();
                    $msg = $result;
                    $success = false;
                }
            } else {
                DB::commit();
                $msg = 'Saved!';
                $success = true;
            }

            if ($success) {

            }

            if (@$data['mode'] == 'api') {
                $ret['data'] = $msg;
                $ret['isSuccess'] = $success;
                return $ret;
            } else {
                return response()->json(['data' => $msg, 'isSuccess' => $success], 200);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            return (@$data['mode'] == 'api') ? $e : response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param $type
     * @param  string  $s
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export($type, $s = '')
    {
        $shopID = get_shopID_H();
        return Excel::download(new SubscribersExport($type, $s, $shopID), 'subscriptions.xlsx');
    }

    /**
     * @param  SubscriberRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveComment(SubscriberRequest $request)
    {
        try {
            $shop = getShopH();
            $data = $request->data;
            $user = Auth::user();

            $this->saveActivity($shop->user_id, $data['customer_id'], $data['id'], 'user', $data['msg']);
            $msg = 'Activity saved!';

            return response()->json(['data' => $msg], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param $subscriber
     * @return false
     */
    public function orderSubscriber($subscriber, $type, $user)
    {
        try {
            if (!empty($subscriber)) {
                if ($type == 'all') {
                    $contracts = SsContract::with('LineItems')->where('ss_customer_id', $subscriber->id)->get()->toArray();
                } else {
                    $contracts = SsContract::with('LineItems')->where('ss_customer_id', $subscriber->id)->whereIn('status', ['active', 'paused'])->get()->toArray();
                }
                $c['active'] = [];
                $c['paused'] = [];
                $c['failed'] = [];
                $c['expired'] = [];
                $c['cancelled'] = [];
                if (count($contracts) > 0) {
                    foreach ($contracts as $key => $val) {
                        $val['created_at'] = date("M d, Y", strtotime($val['created_at']));
                        $val['next_order_date'] = date("M d, Y", strtotime($val['next_order_date']));
                        $val['line_items'] = (!empty($val)) ? $this->getLineItemsData($val, $user) : [];
                        if ($val['status'] == 'active') {
                            $c['active'][] = $val;
                        } else {
                            if ($val['status'] == 'paused') {
                                $c['paused'][] = $val;
                            } else {
                                if ($val['status'] == 'failed') {
                                    $c['failed'][] = $val;
                                } else {
                                    if ($val['status'] == 'expired') {
                                        $c['expired'][] = $val;
                                    } else {
                                        if ($val['status'] == 'cancelled') {
                                            $c['cancelled'][] = $val;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $cnt = array_merge($c['active'], $c['paused']);
                $cnt = array_merge($cnt, $c['failed']);
                $cnt = array_merge($cnt, $c['expired']);
                $cnt = array_merge($cnt, $c['cancelled']);
                $subscriber['Contracts'] = $cnt;
                $subscriber['Activities'] = $this->getActivity($subscriber['ActivityLog']);
            }
            return $subscriber;
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function billingAttempt($attempts, $shopID)
    {
        $attempt = [];
        if (count($attempts) > 0) {
            foreach ($attempts as $key => $value) {
                $attempt[$key]['status'] = $value->status;

                $attempt[$key]['completedAt'] = date('M d, Y', strtotime($this->getSubscriptionTimeDate(date('Y-m-d', strtotime($value->completedAt)), $shopID, date('H:i:s', strtotime($value->completedAt)))));
                $attempt[$key]['attempted_on'] = date('M d, Y H:i', strtotime($this->getSubscriptionTimeDate(date('Y-m-d', strtotime($value->created_at)), $shopID, date('H:i:s', strtotime($value->created_at)))));
                $attempt[$key]['created_at'] = date('M d, Y', strtotime($this->getSubscriptionTimeDate(date('Y-m-d', strtotime($value->created_at)), $shopID, date('H:i:s', strtotime($value->created_at)))));
                $attempt[$key]['shopify_order_id'] = $value->shopify_order_id;
                $attempt[$key]['errorMessage'] = $value->errorMessage;

                $ss_order = SsOrder::where('shopify_order_id', $value->shopify_order_id)->first();
                $attempt[$key]['shopify_order_name'] = (@$ss_order->shopify_order_name) ? $ss_order->shopify_order_name : '';
            }
        }

        $res = paginateH($attempts);
        $res['data'] = $attempt;
        return $res;
    }

    public function getFulfillments($user_id, $bill_number)
    {
        $fullfilment = [];
        $order = $this->getPrepaidFulfillments($user_id, $bill_number);
        if (!empty($order)) {
            foreach ($order as $key => $value) {
                $node = $value['node'];
                $node['fulfillAt'] = date('M d, Y', strtotime($node['fulfillAt']));
                $fullfilment[$key] = $node;
            }
        }
        return $fullfilment;
    }

    public function getActivity($ActivityLog, $shop_id, $timezone)
    {
        try {
            $default_timezone = date_default_timezone_get();

            $activity = [];
            if (!empty($ActivityLog)) {

                date_default_timezone_set($timezone);
                $curr_date = date('Y-m-d');
                date_default_timezone_set($default_timezone);

                foreach ($ActivityLog as $key => $val) {
                    $merchant_time = $this->getSubscriptionTimeDate(date('Y-m-d', strtotime($val['created_at'])), $shop_id, date('H:i:s', strtotime($val['created_at'])));

                    // $activity[$key]['create'] = ( $curr_date > $val['created_at'] ) ? date("F d", strtotime($val['created_at'])) : 'Today';
                    $activity[$key]['create'] = ($curr_date > $merchant_time) ? date("F d", strtotime($merchant_time)) : 'Today';
                    // $activity[$key]['time'] = date("H:i A", strtotime($val['created_at']));
                    $activity[$key]['time'] = date("H:i A", strtotime($merchant_time));
                    $activity[$key]['message'] = $val['message'];
                    $activity[$key]['user_type'] = $val['user_type'];
                    $activity[$key]['user_name'] = $val['user_name'];
                }
            }
            return $activity;
        } catch (\Exception $e) {
            dd($e);
        }

    }

    public function saveLineItem(Request $request)
    {
        try {
            DB::beginTransaction();
            $shop = getShopH();
            $data = $request->data;
            $contract_id = $data['contract_id'];
            $contract = SsContract::find($contract_id);
            $variants = $data['resource'];
            foreach ($variants as $key => $val) {
                $result = [];

                if ($contract->is_multicurrency) {
                    $val['price'] = calculateCurrency($shop->currency, $contract->currency_code, $val['price']);
                    $val['final_amount'] = $val['price'];
                    $currency = $contract->currency_code;
                    $currencySymbol = currencyH($currency);
                } else {
                    $currency = $shop->currency;
                    $currencySymbol = $shop->currency_symbol;
                }

                $lineItem = new SsContractLineItem;
                $lineItem->shopify_contract_id = $contract->shopify_contract_id;
                $lineItem->ss_contract_id = $contract_id;
                $lineItem->user_id = $shop->user_id;
                $lineItem->shopify_product_id = $val['product_id'];
                $lineItem->shopify_variant_id = $val['variant_id'];
                $lineItem->sku = $val['sku'];
                $lineItem->shopify_variant_image = $val['shopify_variant_image'];
                $lineItem->shopify_variant_title = $val['sku'];
                $lineItem->title = $val['product_title'];
                $lineItem->price = $val['price'];
                $lineItem->currency = $currency;
                $lineItem->currency_symbol = $currencySymbol;
                $lineItem->discount_type = '%';
                $lineItem->discount_amount = 0;
                $lineItem->final_amount = $val['final_amount'];
                $lineItem->price_discounted = $val['final_amount'];
                $lineItem->quantity = 1;
                $lineItem->save();

                $result = $this->subscriptionDraftLineAdd($shop->user_id, $lineItem);

                if (gettype($result) == 'array') {
                    $lineItem->shopify_line_id = str_replace('gid://shopify/SubscriptionLine/', '', $result['id']);
                    // $lineItem->shopify_line_id = $result['id'];
                    $lineItem->selling_plan_id = $result['sellingPlanId'];
                    $lineItem->selling_plan_name = $result['sellingPlanName'];
                    $lineItem->save();

                    DB::commit();
                    $msg = 'Subscription updated';
                    $success = true;
                } else {
                    DB::rollBack();
                    $msg = $result;
                    $success = false;
                }
            }

            return response()->json(['data' => $msg, 'isSuccess' => $success], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param $contract
     * @return array
     */
    public function getLineItemsData($contract, $user)
    {
        try {
            $res = [];
//                $lineItems = SsContractLineItem::where('ss_contract_id', $val['id'])->get()->toArray();
            $lineItems = $contract['LineItems'];
            if (!empty($lineItems)) {
                foreach ($lineItems as $lkey => $lval) {
                    $lval = $lval->toArray();
                    $sh_product = $this->getShopifyProduct($lval['shopify_product_id'], $lval['shopify_variant_id'], $user);
                    $sh_variants = $this->getShopifyVariants($lval['shopify_product_id'], $user);
                    $var['sh_variants'] = $sh_variants;
                    $var['selected_variant'] = $lval['shopify_variant_id'];
                    $var['sh_product'] = $sh_product;
                    $res[$lkey] = array_merge($lval, $var);
                }
            }
            return $res;
        } catch (\Exception $e) {
            dd($e);
        }
    }

    /**
     * @param $product_id
     * @param $variant_id
     * @return mixed
     */
    public function getShopifyProduct($product_id, $variant_id, $user)
    {
        $shop = $user;
        $endPoint = '/admin/api/' . env('SHOPIFY_API_VERSION') . '/products/' . $product_id . '.json';
        $parameter['fields'] = 'id,title,image,handle';
        $result = $this->request('GET', $endPoint, $parameter, $shop->id);
        $variant = $this->getShopifyVariant($variant_id, $user);
        if (!$result['errors']) {
            $sh_product = $result['body']->container['product'];
            $res['product_name'] = $sh_product['title'];
            $res['product_image'] = (@$sh_product['image']['src']) ? $sh_product['image']['src'] : noImagePathH();
            $res['variant_name'] = $variant['variant_name'];
            $res['sku'] = $variant['sku'];
            $res['handle'] = $sh_product['handle'];
        } else {
            $res['product_name'] = '';
            $res['product_image'] = '';
            $res['variant_name'] = '';
            $res['sku'] = '';
            $res['handle'] = '';
        }
        return $res;
    }

    /**
     * Get shopify variant sku,image
     * @param $variant_id
     * @return mixed
     */
    public function getShopifyVariant($variant_id, $user)
    {
        $shop = $user;
        $endPoint = '/admin/api/' . env('SHOPIFY_API_VERSION') . '/variants/' . $variant_id . '.json';
        $parameter['fields'] = 'id,title,sku';
        $result = $this->request('GET', $endPoint, $parameter, $shop->id);
        $res['variant_name'] = '';
        $res['sku'] = '';
        if (!$result['errors']) {
            $sh_variant = $result['body']->container['variant'];
            $res['variant_name'] = $sh_variant['title'];
            $res['sku'] = $sh_variant['sku'];
        }
        return $res;
    }

    /**
     * Get shopify variant sku,image
     * @param $variant_id
     * @return mixed
     */
    public function getShopifyVariants($product_id, $user)
    {
        $shop = $user;
        $endPoint = '/admin/api/' . env('SHOPIFY_API_VERSION') . '/products/' . $product_id . '/variants.json';
        $parameter['fields'] = 'id,title,sku, price';
        $result = $this->request('GET', $endPoint, $parameter, $shop->id);
        $res['variant_id'] = '';
        $res['title'] = '';
        $res['sku'] = '';
        $res['price'] = '';
        if (!$result['errors']) {
            $sh_variant = $result['body']->container['variants'];

        }
        return $sh_variant;
    }
    /**
     * @param $subscriber
     * @return mixed
     */
    public function getOtherContracts($subscriber)
    {
        try {
            $contracts = SsContract::select('id', 'status', 'created_at', 'updated_at')->where('ss_customer_id', $subscriber->ss_customer_id)->where('id', '!=', $subscriber->id)->where('shop_id', $subscriber->shop_id)->where('user_id', $subscriber->user_id)->get()->toArray();
            return $contracts;
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function getCountry($country)
    {
//        $data['countries'] = Country::select('name')->get()->toArray();
        //        $data['states'] = State::select('name')->get()->toArray();
        $data['countries'] = countryH();
        $data['states'] = stateFromCountryH(array_search($country, $data['countries'], true));
        return response()->json(['data' => $data], 200);
    }

    public function getAvailableDates($anchorDateType, $anchorDay, $anchorMonth, $nextOrderDate)
    {
      if ($anchorDateType == 'WEEKDAY') {
        $range = CarbonPeriod::create($nextOrderDate, Carbon::now()->addWeek(10));
        $availableDates = [];
        foreach ($range as $carbon) { //This is an iterator
          if ($carbon->dayOfWeekIso == $anchorDay) {
              $availableDates[] = date('M d, Y', strtotime($carbon));  
          }
        }
      } elseif ($anchorDateType == 'MONTHDAY') {
        $anchorDay == 31
        ? $endDate = Carbon::now()->addMonth(18)->lastOfMonth()
        : $endDate = Carbon::now()->addMonth(10)->lastOfMonth();
        $period = CarbonPeriod::create($nextOrderDate, $endDate);

        $availableDates = [];        
        foreach ($period as $carbon) {
          if ($carbon->day == $anchorDay) {
            $availableDates[] = date('M d, Y', strtotime($carbon));
          }
        }
      } else {
        $period = CarbonPeriod::create($nextOrderDate, Carbon::create($nextOrderDate)->addYears(9));
        $availableDates = [];
        foreach ($period as $carbon) {
          if ($carbon->day == $anchorDay && $carbon->month == $anchorMonth) {
            $availableDates[] = date('M d, Y', strtotime($carbon));
          }
        }
      }

      return $availableDates;
    }
}
