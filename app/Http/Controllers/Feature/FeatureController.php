<?php

namespace App\Http\Controllers\Feature;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use LaravelFeature\Facade\Feature;

class FeatureController extends Controller
{
    public function add($name){
        try{
            Feature::add($name, false);
            return 'Feature <b>'. $name .'</b> is added!';
        }catch( \Exception $e ){
            dd($e);
        }
    }

    public function enableFor($name, $id){
        try{
            $user = User::find($id);
            Feature::enableFor($name, $user);
            return 'Feature <b>'. $name .'</b> is enable for user <b>"' . $user->name . '"</b>';
        }catch( \Exception $e ){
            dd($e);
        }
    }

    public function disableFor($name, $id){
        try{
            $user = User::find($id);
            Feature::disableFor($name, $user);
            return 'Feature <b>'. $name .'</b> is disable for user <b>"' . $user->name . '"</b>';
        }catch( \Exception $e ){
            dd($e);
        }
    }

    public function remove($name, $id){
        try{
            Feature::add('my.feature', true);
        }catch( \Exception $e ){
            dd($e);
        }
    }
}
