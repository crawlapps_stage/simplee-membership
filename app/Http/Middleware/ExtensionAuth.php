<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use function Osiset\ShopifyApp\parseQueryString;

class ExtensionAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $this->getToken($request);
        $headerT = $request->header('X-session-token');

        if( $token == $headerT ){
            return $next($request);
        }else{
            return response()->json(['data' => 'Invalid extension token', 'isSuccess' => false], 422);
            // return Response::make('Invalid extension token.', 401);
        }

    }

    protected function getToken(Request $request)
    {
        // Create token header as a JSON string
        $header = json_encode(['alg' => 'HS256', 'typ' => 'JWT']);
        $headerT = $request->header('X-session-token');
        // Create token payload as a JSON string
//        $payload = json_encode($request->json()->all());
        $payload = base64_decode(substr($headerT, (strpos($headerT,".") + 1) , (strrpos($headerT,"." ) - strpos($headerT,".") - 1)));

        // Encode Header to Base64Url String
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

        // Encode Payload to Base64Url String
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        // Create Signature Hash
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, env('SHOPIFY_API_SECRET'), true);

        // Encode Signature to Base64Url String
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        // Create JWT
        $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

        return $jwt;

    }
}
