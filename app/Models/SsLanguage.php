<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SsLanguage extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'shop_id', 'created_at', 'updated_at'
    ];
}
