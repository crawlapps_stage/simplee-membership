<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SsShippingProfile extends Model
{
    public function ShippingZones(){
        return $this->hasMany(SsShippingZone::class, 'ss_shipping_profile_id', 'id' );
    }
}
