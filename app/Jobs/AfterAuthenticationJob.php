<?php

namespace App\Jobs;

use App\Models\SsLanguage;
use App\Traits\ShopifyTrait;
use App\Models\Shop;
use App\Models\SsEmail;
use App\Models\SsSetting;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Osiset\ShopifyApp\Storage\Models\Plan;

class AfterAuthenticationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use ShopifyTrait;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            logger('============== START:: AfterAuthenticationJob ===========');

            $user = Auth::user();
            logger(json_encode($user));
            $user->active = 1;
            $user->save();
            $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/shop.json';
            $result = $user->api()->rest('GET', $endPoint);
            if( !$result['errors'] ){
                $sh_shop = $result['body']->container['shop'];

                $is_exist_shop = Shop::where('user_id', $user->id)->first();
                $db_shop = ( $is_exist_shop ) ? $is_exist_shop : new Shop;
                $db_shop->user_id = $user['id'];
                $db_shop->shopify_store_id = $sh_shop['id'];
                $db_shop->active = true;
                $db_shop->ts_last_deactivation = date('Y-m-d H:i:s');
                $db_shop->test_store = ($sh_shop['plan_name'] == 'partner_test');
                $db_shop->name = $sh_shop['name'];
                $db_shop->email = $sh_shop['email'];
                $db_shop->myshopify_domain = $sh_shop['myshopify_domain'];
                $db_shop->domain = $sh_shop['domain'];
                $db_shop->owner = $sh_shop['shop_owner'];
                $db_shop->owner = $sh_shop['shop_owner'];
                $db_shop->shopify_plan = $sh_shop['plan_name'];
                $db_shop->timezone = $sh_shop['timezone'];
                $db_shop->address1 = $sh_shop['address1'];
                $db_shop->address2 = $sh_shop['address2'];
                $db_shop->checkout_api_supported = $sh_shop['checkout_api_supported'];
                $db_shop->city = $sh_shop['city'];
                $db_shop->country = $sh_shop['country'];
                $db_shop->country_code = $sh_shop['country_code'];
                $db_shop->country_name = $sh_shop['country_name'];
                $db_shop->country_taxes = $sh_shop['county_taxes'];
                $db_shop->ss_created_at = ($is_exist_shop) ? $is_exist_shop['ss_created_at'] : date('Y-m-d H:i:s');
                $db_shop->customer_email = $sh_shop['customer_email'];
                $db_shop->currency = $sh_shop['currency'];
                $db_shop->currency_symbol = currencyH($sh_shop['currency']);
                $db_shop->enabled_presentment_currencies = json_encode($sh_shop['enabled_presentment_currencies']);
                $db_shop->eligible_for_payments = $sh_shop['eligible_for_payments'];
                $db_shop->has_discounts = $sh_shop['has_discounts'];
                $db_shop->has_gift_cards = $sh_shop['has_gift_cards'];
                $db_shop->has_storefront = $sh_shop['has_storefront'];
                $db_shop->iana_timezone = $sh_shop['iana_timezone'];
                $db_shop->latitude = $sh_shop['latitude'];
                $db_shop->longitude = $sh_shop['longitude'];
                $db_shop->money_format = $sh_shop['money_format'];
                $db_shop->money_in_emails_format = $sh_shop['money_in_emails_format'];
                $db_shop->money_with_currency_format = $sh_shop['money_with_currency_format'];
                $db_shop->money_with_currency_in_emails_format = $sh_shop['money_with_currency_in_emails_format'];
                $db_shop->multi_location_enabled = $sh_shop['multi_location_enabled'];
                $db_shop->password_enabled = $sh_shop['password_enabled'];
                $db_shop->phone = $sh_shop['phone'];
                $db_shop->pre_launch_enabled = $sh_shop['pre_launch_enabled'];
                $db_shop->primary_locale = $sh_shop['primary_locale'];
                $db_shop->province = $sh_shop['province'];
                $db_shop->province_code = $sh_shop['province_code'];
                $db_shop->requires_extra_payments_agreement = $sh_shop['requires_extra_payments_agreement'];
                $db_shop->setup_required = $sh_shop['setup_required'];
                $db_shop->taxes_included = $sh_shop['taxes_included'];
                $db_shop->tax_shipping = $sh_shop['tax_shipping'];
                $db_shop->tbl_updated_at = date('Y-m-d H:i:s');
                $db_shop->weight_unit = $sh_shop['weight_unit'];
                $db_shop->zip = $sh_shop['zip'];
                $db_shop->save();

                $setting = SsSetting::where('shop_id', $db_shop->id)->first();
                logger(json_encode($setting));
                if( !$setting ){

                    $setting = new SsSetting;
                    $setting->shop_id = $db_shop->id;
                    $setting->dunning_retries = 7;
                    $setting->dunning_daysbetween = 1;
                    $setting->email_from_name = $sh_shop['name'];
                    $setting->email_from_email = $sh_shop['email'];
                    $setting->dunning_email_enabled = 1;
                    $setting->dunning_failedaction = 'cancel';
                    $setting->subscription_daily_at = '12:01 AM';
                    $setting->mailgun_method = 'Safe';
                    $setting->mailgun_verified = 0;
                    $setting->save();
                }

                $portalLang = SsLanguage::where('shop_id', $db_shop->id)->first();
                if( !$portalLang ){
                    $portalLang = new SsLanguage;
                    $portalLang->shop_id = $db_shop->id;
                    $portalLang->save();
                }

                $failedPaymentEmail = SsEmail::where('shop_id', $db_shop->id)->where('category', 'failed_payment_to_customer')->first();
                if( !$failedPaymentEmail ){
                    $failedPaymentEmail = new SsEmail;
                    $failedPaymentEmail->shop_id = $db_shop->id;
                    $failedPaymentEmail->category = 'failed_payment_to_customer';
                    $failedPaymentEmail->description = '';
                    $failedPaymentEmail->active = 1;
                    $failedPaymentEmail->subject = $db_shop->name.' - we were unable to process your subscription payment';
                    $failedPaymentEmail->plain_text = '';
                    $failedPaymentEmail->html_body = getPaymentFailedMailHtml();
                    $failedPaymentEmail->save();
                }

                $newSubscriptionEmail = SsEmail::where('shop_id', $db_shop->id)->where('category', 'new_subscription_to_customer')->first();
                if( !$newSubscriptionEmail ){
                    $newSubscriptionEmail = new SsEmail;
                    $newSubscriptionEmail->shop_id = $db_shop->id;
                    $newSubscriptionEmail->category = 'new_subscription_to_customer';
                    $newSubscriptionEmail->description = '';
                    $newSubscriptionEmail->active = 1;
                    $newSubscriptionEmail->subject = $db_shop->name.' - your subscription was created successfully!';
                    $newSubscriptionEmail->plain_text = '';
                    $newSubscriptionEmail->html_body = getNewSubscriptioMailHtml();
                    $newSubscriptionEmail->save();
                }

                if( !$is_exist_shop ){
                    $themeID = $this->getPublishTheme();
                    ($themeID != '') ? addSnippetH($themeID, $user->id, false) : '';

                    // Add user metafields
                    logger('============== START:: AddMetafields ===========');
                    $metafields = Config::get('metafields.options'); // TODO: add columns to ss_settings to store the values in our app
                    $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/metafields.json';
                    foreach ($metafields as $metafield) {
                        $user->api()->rest('POST', $endPoint, array('metafield' => $metafield));
                    }
                    logger('============== END:: AddMetafields ===========');
                }
            }
            // create webhooks
//            $webhooks = [
//                'subscription_billing_attempts/failure' => 'subscription-billing-attempts-failure',
//                'subscription_billing_attempts/success' => 'subscription-billing-attempts-success',
//                'subscription_contracts/create' => 'subscription-contracts-create',
//                'subscription_contracts/update' => 'subscription-contracts-update',
//                'customer_payment_methods/create' => 'customer-payment-methods-create',
//                'customer_payment_methods/revoke' => 'customer-payment-methods-revoke',
//                'customer_payment_methods/update' => 'customer-payment-methods-update',
//            ];
//            $this->createWebhook($webhooks, $user->id);

            logger('============== END:: AfterAuthenticationJob ===========');
        }catch( \Exception $e ){
            logger('============== ERROR:: AfterAuthenticationJob ===========');
            logger(json_encode($e));
            Bugsnag::notifyException($e);
        }
    }
}
