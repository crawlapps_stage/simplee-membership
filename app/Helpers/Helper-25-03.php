<?php

use App\Models\Shop;
use App\Models\ExchangeRate;
use App\Models\SsCustomer;
use App\Models\SsSetting;
use App\User;
use DougSisk\CountryState\CountryState;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\Intl\Currencies;

if (!function_exists('get_shopID_H')) {

    /**
     * @return mixed
     *
     */
    function get_shopID_H()
    {
        $user = Auth::user();
        $shop = Shop::where('user_id', $user->id)->first();
        return $shop['id'];
    }
}

if (!function_exists('check_decimal_H')) {

    /**
     * @return mixed
     *
     */
    function check_decimal_H($number)
    {
        return preg_replace('/[^0-9 .]/s', '', $number);
    }
}

if (!function_exists('getShopH')) {

    /**
     * @return mixed
     * @return mixed
     */
    function getShopH()
    {
        $user = Auth::user();
        $shop = Shop::where('user_id', $user->id)->first();
        return $shop;
    }
}

if (!function_exists('sendMailH')) {

    /**
     * @return mixed
     * @return mixed
     */
    function sendMailH($subject, $html, $from, $to, $fromname, $shopID, $customerID)
    {
        try{
            $customer = SsCustomer::find($customerID);
            $shop = Shop::find($shopID);
            if( $customer ){
                $newHtml = str_replace( '[FIRST_NAME]', $customer->first_name, $html);
                $newHtml = str_replace( '[LAST_NAME]', $customer->last_name, $newHtml);
                $newHtml = str_replace( '[STORE_NAME]', $shop->name, $newHtml);
                $newHtml = str_replace( '[STORE_URL]', $shop->domain, $newHtml);
            }else{
                $newHtml = $html;
                $newHtml = str_replace( '[STORE_NAME]', $shop->name, $html);
                $newHtml = str_replace( '[STORE_URL]', $shop->domain, $newHtml);

                $newHtml = str_replace( '[FIRST_NAME]', 'Jane', $newHtml);
                $newHtml = str_replace( '[LAST_NAME]', 'Smith', $newHtml);
                $newHtml = str_replace( '[CARD_TYPE]', 'Visa', $newHtml);
                $newHtml = str_replace( '[EXPIRY_DATE]', '01/29', $newHtml);
            }

            $data = array('data' => $newHtml);
            $setting = SsSetting::where('shop_id', $shopID)->first();

            $reply_to = '';
            $mailgun_method = $setting->mailgun_method;
            if( $mailgun_method == 'Basic' ){
                $domain = env('MAILGUN_DOMAIN');
            }elseif ( $mailgun_method == 'Safe' ){
                $reply_to = $from;
                $from = 'no-reply@mg.simplee.best';
                $domain = env('MAILGUN_DOMAIN');

            }elseif( $mailgun_method == 'Advanced' ){
                if( $setting->mailgun_verified ){
                    config(['services.mailgun.domain' => $setting->mailgun_domain]);
                }
            }
            $to = str_replace(' ', '', $to);
            Mail::send('mail.mail', $data, function ($message) use ($subject, $newHtml, $from, $to, $fromname, $reply_to) {
                $message->from($from, $fromname);
                $message->to($to);
                $message->subject($subject);
                ( $reply_to != '' ) ? $message->replyTo($reply_to) : '';
            });
            return 'success';
        }catch( \Exception $e ){
            return $e->getMessage();
        }
    }
}

if (!function_exists('getPaymentFailedMailHtml')) {

    /**
     * @return mixed
     * @return mixed
     */
    function getPaymentFailedMailHtml()
    {
        $html = '<p>&nbsp;</p>
            <!-- [if mso]>
                <style type="text/css">
                  .f-fallback  {
                    font-family: Arial, sans-serif;
                  }
                </style>
              <![endif]-->
            <p><span class="preheader" style="visibility: hidden; mso-hide: all; font-size: 1px; line-height: 1px; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; display: none;">We were unable to process your subscription payment. To avoid having this subscription expire, please update your payment information</span></p>
            <table class="email-wrapper" style="width: 100%; margin: 0; padding: 0; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; background-color: #f4f4f7;" role="presentation" width="100%" cellspacing="0" cellpadding="0" bgcolor="#F4F4F7">
            <tbody>
            <tr>
            <td style="word-break: break-word; font-family: \'Nunito Sans\', Helvetica, Arial, sans-serif; font-size: 16px;" align="center">
            <table class="email-content" style="width: 100%; margin: 0; padding: 0; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0;" role="presentation" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
            <td class="email-masthead" style="word-break: break-word; font-family: \'Nunito Sans\', Helvetica, Arial, sans-serif; font-size: 16px; padding: 25px 0; text-align: center;" align="center">[STORE_NAME]</td>
            </tr>
            <!-- Email Body -->
            <tr>
            <td class="email-body" style="word-break: break-word; font-family: \'Nunito Sans\', Helvetica, Arial, sans-serif; font-size: 16px; width: 100%; margin: 0; padding: 0; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; background-color: #ffffff;" bgcolor="#FFFFFF" width="100%">
            <table class="email-body_inner" style="width: 570px; margin: 0 auto; padding: 0; -premailer-width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; background-color: #ffffff;" role="presentation" width="570" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF"><!-- Body content -->
            <tbody>
            <tr>
            <td class="content-cell" style="word-break: break-word; font-family: \'Nunito Sans\', Helvetica, Arial, sans-serif; font-size: 16px; padding: 35px;">
            <div class="f-fallback">
            <h1 style="margin-top: 0; color: #333333; font-size: 22px; font-weight: bold; text-align: left;">Hello!</h1>
            <p style="margin: .4em 0 1.1875em; font-size: 16px; line-height: 1.625; color: #51545e;">We just tried processing a payment for your subscription, but this attempt failed. Sometimes this is due to temporary issues or unavailable credit on your card.</p>
            <p style="margin: .4em 0 1.1875em; font-size: 16px; line-height: 1.625; color: #51545e;">If you have recently received a replacement credit card, or had to cancel a card for any reason, please login to your account to update your payment information:</p>
            <!-- Action -->
            <table class="body-action" style="width: 100%; margin: 30px auto; padding: 0; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; text-align: center;" role="presentation" width="100%" cellspacing="0" cellpadding="0" align="center">
            <tbody>
            <tr>
            <td style="word-break: break-word; font-family: \'Nunito Sans\', Helvetica, Arial, sans-serif; font-size: 16px;" align="center">
            <table role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
            <td style="word-break: break-word; font-family: \'Nunito Sans\', Helvetica, Arial, sans-serif; font-size: 16px;" align="center"><a class="f-fallback button" style="background-color: #3869d4; border-top: 10px solid #3869D4; border-right: 18px solid #3869D4; border-bottom: 10px solid #3869D4; border-left: 18px solid #3869D4; display: inline-block; color: #fff; text-decoration: none; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); -webkit-text-size-adjust: none; box-sizing: border-box;" href="[STORE_URL]" target="_blank" rel="noopener">Login to your account</a></td>
            </tr>
            </tbody>
            </table>
            </td>
            </tr>
            </tbody>
            </table>
            <p style="margin: .4em 0 1.1875em; font-size: 16px; line-height: 1.625; color: #51545e;">This is the card we attempted to process:</p>
            <table class="attributes" style="margin: 0 0 21px;" role="presentation" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
            <td class="attributes_content" style="word-break: break-word; font-family: \'Nunito Sans\', Helvetica, Arial, sans-serif; font-size: 16px; background-color: #f4f4f7; padding: 16px;" bgcolor="#F4F4F7">
            <table role="presentation" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
            <td class="attributes_item" style="word-break: break-word; font-family: \'Nunito Sans\', Helvetica, Arial, sans-serif; font-size: 16px; padding: 0;"><span class="f-fallback"> <strong>Cart Type:</strong> [CARD_TYPE] </span></td>
            </tr>
            <tr>
            <td class="attributes_item" style="word-break: break-word; font-family: \'Nunito Sans\', Helvetica, Arial, sans-serif; font-size: 16px; padding: 0;"><span class="f-fallback"> <strong>Expiry Date:</strong> [EXPIRY_DATE] </span></td>
            </tr>
            </tbody>
            </table>
            </td>
            </tr>
            </tbody>
            </table>
            <!-- Sub copy -->
            <table class="body-sub" style="margin-top: 25px; padding-top: 25px; border-top: 1px solid #EAEAEC;" role="presentation">
            <tbody>
            <tr>
            <td style="word-break: break-word; font-family: \'Nunito Sans\', Helvetica, Arial, sans-serif; font-size: 16px;">
            <p class="f-fallback sub" style="margin: .4em 0 1.1875em; line-height: 1.625; font-size: 13px; color: #6b6e76;">If you would prefer not to click any buttons in this email, simply copy our store URL below and paste it into any browser.</p>
            <p class="f-fallback sub" style="margin: .4em 0 1.1875em; line-height: 1.625; font-size: 13px; color: #6b6e76;">[STORE_URL]</p>
            </td>
            </tr>
            </tbody>
            </table>
            </div>
            </td>
            </tr>
            </tbody>
            </table>
            </td>
            </tr>
            <tr>
            <td style="word-break: break-word; font-family: \'Nunito Sans\', Helvetica, Arial, sans-serif; font-size: 16px;"><!-- ADD ADDRESS BELOW IF YOU WISH
                            <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
                              <tr>
                                <td class="content-cell" align="center">
                                  <p class="f-fallback sub align-center">
                                    [Company Name, LLC]
                                    <br>1234 Street Rd.
                                    <br>Suite 1234
                                  </p>
                                </td>
                              </tr>
                            </table>
                          --></td>
            </tr>
            </tbody>
            </table>
            </td>
            </tr>
            </tbody>
            </table>';

        return $html;

    }
}

if (!function_exists('getNewSubscriptioMailHtml')) {

    /**
     * @return mixed
     * @return mixed
     */
    function getNewSubscriptioMailHtml()
    {
        $html = '<p>&nbsp;</p>
                <!-- [if mso]>
                    <style type="text/css">
                      .f-fallback  {
                        font-family: Arial, sans-serif;
                      }
                    </style>
                  <![endif]-->
                <p><span class="preheader" style="visibility: hidden; mso-hide: all; font-size: 1px; line-height: 1px; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; display: none;">We were unable to process your subscription payment. To avoid having this subscription expire, please update your payment information</span></p>
                <table class="email-wrapper" style="width: 100%; margin: 0; padding: 0; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; background-color: #f4f4f7;" role="presentation" width="100%" cellspacing="0" cellpadding="0" bgcolor="#F4F4F7">
                <tbody>
                <tr>
                <td style="word-break: break-word; font-family: \'Nunito Sans\', Helvetica, Arial, sans-serif; font-size: 16px;" align="center">
                <table class="email-content" style="width: 100%; margin: 0; padding: 0; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0;" role="presentation" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                <td class="email-masthead" style="word-break: break-word; font-family: \'Nunito Sans\', Helvetica, Arial, sans-serif; font-size: 16px; padding: 25px 0; text-align: center;" align="center">[STORE_NAME]</td>
                </tr>
                <!-- Email Body -->
                <tr>
                <td class="email-body" style="word-break: break-word; font-family: \'Nunito Sans\', Helvetica, Arial, sans-serif; font-size: 16px; width: 100%; margin: 0; padding: 0; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; background-color: #ffffff;" bgcolor="#FFFFFF" width="100%">
                <table class="email-body_inner" style="width: 570px; margin: 0 auto; padding: 0; -premailer-width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; background-color: #ffffff;" role="presentation" width="570" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF"><!-- Body content -->
                <tbody>
                <tr>
                <td class="content-cell" style="word-break: break-word; font-family: \'Nunito Sans\', Helvetica, Arial, sans-serif; font-size: 16px; padding: 35px;">
                <div class="f-fallback">
                <h1 style="margin-top: 0; color: #333333; font-size: 22px; font-weight: bold; text-align: left;">Hello!</h1>
                <p style="margin: .4em 0 1.1875em; font-size: 16px; line-height: 1.625; color: #51545e;">We wanted to confirm that we\'ve received your order, and your new subscription has begun.</p>
                <p style="margin: .4em 0 1.1875em; font-size: 16px; line-height: 1.625; color: #51545e;">If you need to make any updates to this subscription, you can always login to your account using the link below. If you don\'t already have an account with our store, simply create a new account using this email address, and you will be able to manage your subscription.</p>
                <!-- Action -->
                <table class="body-action" style="width: 100%; margin: 30px auto; padding: 0; -premailer-width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; text-align: center;" role="presentation" width="100%" cellspacing="0" cellpadding="0" align="center">
                <tbody>
                <tr>
                <td style="word-break: break-word; font-family: \'Nunito Sans\', Helvetica, Arial, sans-serif; font-size: 16px;" align="center">
                <table role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                <td style="word-break: break-word; font-family: \'Nunito Sans\', Helvetica, Arial, sans-serif; font-size: 16px;" align="center"><a class="f-fallback button" style="background-color: #3869d4; border-top: 10px solid #3869D4; border-right: 18px solid #3869D4; border-bottom: 10px solid #3869D4; border-left: 18px solid #3869D4; display: inline-block; color: #fff; text-decoration: none; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); -webkit-text-size-adjust: none; box-sizing: border-box;" href="[STORE_URL]" target="_blank" rel="noopener">Login to your account</a></td>
                </tr>
                </tbody>
                </table>
                </td>
                </tr>
                </tbody>
                </table>
                <!-- Sub copy -->
                <table class="body-sub" style="margin-top: 25px; padding-top: 25px; border-top: 1px solid #EAEAEC;" role="presentation">
                <tbody>
                <tr>
                <td style="word-break: break-word; font-family: \'Nunito Sans\', Helvetica, Arial, sans-serif; font-size: 16px;">
                <p class="f-fallback sub" style="margin: .4em 0 1.1875em; line-height: 1.625; font-size: 13px; color: #6b6e76;">If you would prefer not to click any buttons in this email, you can copy our store URL below and paste it into any browser.</p>
                <p class="f-fallback sub" style="margin: .4em 0 1.1875em; line-height: 1.625; font-size: 13px; color: #6b6e76;">[STORE_URL]</p>
                </td>
                </tr>
                </tbody>
                </table>
                </div>
                </td>
                </tr>
                </tbody>
                </table>
                </td>
                </tr>
                <tr>
                <td style="word-break: break-word; font-family: \'Nunito Sans\', Helvetica, Arial, sans-serif; font-size: 16px;"><!-- ADD ADDRESS BELOW IF YOU WISH
                                <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
                                  <tr>
                                    <td class="content-cell" align="center">
                                      <p class="f-fallback sub align-center">
                                        [Company Name, LLC]
                                        <br>1234 Street Rd.
                                        <br>Suite 1234
                                      </p>
                                    </td>
                                  </tr>
                                </table>
                              --></td>
                </tr>
                </tbody>
                </table>
                </td>
                </tr>
                </tbody>
                </table>';

        return $html;

    }
}

if (!function_exists('currencyH')) {

    /**
     * @return mixed
     * @return mixed
     */
    function currencyH($c)
    {
        return Currencies::getSymbol($c);
    }
}

if (!function_exists('noImagePathH')) {
    /**
     * @return mixed
     */
    function noImagePathH()
    {
        return asset('images/static/no-image-box.png');
    }
}

if (!function_exists('calculateCurrency')) {
    /**
     * currency converter
     */
    function calculateCurrency($fromCurrency, $toCurrency, $amount)
    {
        try{
            $db_rates = ExchangeRate::latest()->first();
            $rates = json_decode($db_rates->conversion_rates);

            $calculated = round((( $amount * $rates->$toCurrency ) / $rates->$fromCurrency), 4);
            return $calculated;
        }catch(\Exception $e){
            logger('=========== calculateCurrency ===========');
            logger($e);
        }

    }

}

if (!function_exists('installThemeH')) {
    /**
     * @return mixed
     */
    function installThemeH($theme_id, $user_id)
    {
        addSnippetH($theme_id, $user_id, true);
        addCSSAsset($theme_id, $user_id);
    }
}

//simplee.liquid snippet
if (!function_exists('addSnippetH')) {
    /**
     * @param $theme_id
     */
    function addSnippetH($theme_id, $user_id, $is_asset){
        try {
            \Log::info('-----------------------START :: addSnippet -----------------------');
            $user = User::find($user_id);

            $type = 'add';
            if ($type == 'add') {

                if ($is_asset) {
                    $value = <<<EOF
{% if customer %}
  <script type="text/javascript">
    sessionStorage.setItem("X-shopify-customer-ID",{{ customer.id }});
  </script>
{% endif %}
{{ 'simplee.css' | asset_url | stylesheet_tag }}
{{ 'simplee.js' | asset_url | script_tag }}
EOF;
                }else{
                    $value = <<<EOF
{% if customer %}
  <script type="text/javascript">
    sessionStorage.setItem("X-shopify-customer-ID",{{ customer.id }});
  </script>
{% endif %}
EOF;
                }
            }
            $parameter['asset']['key'] = 'snippets/'. config('const.SNIPPETS.SIMPLEE') .'.liquid';
            $parameter['asset']['value'] = $value;
            $asset = $user->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);

            updateThemeLiquidH('simplee', $theme_id, $user_id);
        } catch (\Exception $e) {
            \Log::info('-----------------------ERROR :: addSnippet -----------------------');
            \Log::info(json_encode($e));
        }
    }
}

//simplee-widget.liquid snippet
if (!function_exists('addSimpleeWidgetSnippetH')) {
    /**
     * @param $theme_id
     */
    function addSimpleeWidgetSnippetH($theme_id, $user_id){
        try {
            \Log::info('-----------------------START :: addSimpleeWidgetSnippetH -----------------------');
            $user = User::where('id', $user_id)->first();

            $value = simpleeWidgetTextH();

            $parameter['asset']['key'] = 'snippets/' . config('const.SNIPPETS.SIMPLEE_WIDGET') . '.liquid';
            $parameter['asset']['value'] = $value;
            $asset = $user->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);

//            updateThemeLiquidH('simplee-widget', $theme_id);
        } catch (\Exception $e) {
            \Log::info('-----------------------ERROR :: addSimpleeWidgetSnippetH -----------------------');
            \Log::info(json_encode($e));
        }
    }
}

//simplee-cart.liquid snippet
if (!function_exists('addSimpleeCartWidgetSnippetH')) {
    /**
     * @param $theme_id
     */
    function addSimpleeCartWidgetSnippetH($theme_id, $user_id){
        try {
            \Log::info('-----------------------START :: addSimpleeCartWidgetSnippetH -----------------------');
            $user = User::where('id', $user_id)->first();

            $value = <<<EOF
{%- comment -%} Simplee Storefront Widget - Cart Page - Version 0.1 {%- endcomment -%}
{%- comment -%} For questions visit http://support.simplee.best  {%- endcomment -%}

{% unless item.selling_plan_allocation == nil  %}
  <span class="simplee-selling-plan-details cart__option cart__option--single" data-simplee-item-key="{{item.key}}">
    Subscription: {{item.selling_plan_allocation.selling_plan.name}}
  </span>
{% endunless %}
EOF;

            $parameter['asset']['key'] = 'snippets/' . config('const.SNIPPETS.CART') . '.liquid';
            $parameter['asset']['value'] = $value;
            $asset = $user->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);
        } catch (\Exception $e) {
            \Log::info('-----------------------ERROR :: addSimpleeCartWidgetSnippetH -----------------------');
            \Log::info(json_encode($e));
        }
    }
}
//update theme.liquid
if (!function_exists('updateThemeLiquidH')) {
    /**
     * @param $snippet_name
     * @param $theme_id
     */
    function updateThemeLiquidH($snippet_name, $theme_id, $user_id){
        try {
            \Log::info('-----------------------START :: updateThemeLiquidH -----------------------');
            $user = User::find($user_id);

            $asset = getLiquidAssetH($theme_id, $user_id, config('const.FILES.THEME'));
            if ($asset != '') {
                // add after <body>
                if (!strpos($asset, "{% render '$snippet_name' %}")) {
                    $asset = str_replace('</head>', "{% render '$snippet_name' %}</head>", $asset);
                }

                $parameter['asset']['key'] = config('const.FILES.THEME');
                $parameter['asset']['value'] = $asset;
                $result = $user->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);
            }
        } catch (\Exception $e) {
            \Log::info('-----------------------ERROR :: updateThemeLiquidH -----------------------');
            \Log::info(json_encode($e));
        }
    }
}

//update cart.liquid
if (!function_exists('updateCartLiquidH')) {
    /**
     * @param $snippet_name
     * @param $theme_id
     */
    function updateCartLiquidH($snippet_name, $theme_id, $user_id){
        try {
            \Log::info('----------------------- START :: updateCartLiquidH -----------------------');
            $user = User::find($user_id);

            $asset = getLiquidAssetH($theme_id, $user_id, config('const.FILES.CART'));

            logger($asset);
//            if ($asset != '') {
//                if (!strpos($asset, config('const.FILES.CART_FIND'))) {
//                    if (!strpos($asset, "{% render '$snippet_name' %}")) {
//                        $asset = str_replace('</tbody>', "{% render '$snippet_name' %}</tbody>", $asset);
//                    }
//                }
//
//                $parameter['asset']['key'] =  config('const.FILES.CART');
//                $parameter['asset']['value'] = $asset;
//                $result = $user->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);
//            }
        } catch (\Exception $e) {
            \Log::info('-----------------------ERROR :: updateCartLiquidH -----------------------');
            \Log::info(json_encode($e));
        }
    }
}

//simplee css
if (!function_exists('addCSSAsset')) {
    /**
     * @param $theme_id
     */
    function addCSSAsset($theme_id, $user_id){
        try {
            \Log::info('-----------------------START :: addCSSAsset -----------------------');
            $user = User::where('id', $user_id)->first();
            $cssCode = getCSSCode();

            $parameter['asset']['key'] = 'assets/'. config('const.ASSETS.CSS') .'.css';
            $parameter['asset']['value'] = $cssCode;
            $asset = $user->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);

            \Log::info(json_encode($asset));
        } catch (\Exception $e) {
            \Log::info('-----------------------ERROR :: addCSSAsset -----------------------');
            \Log::info(json_encode($e));
        }
    }
}

// simplee css code
if (!function_exists('getCSSCode')) {
    /**
     * @return string
     */
    function getCSSCode()
    {
        // Asset Simplee CSS
        return '/* Simplee Storefront Widget - CSS - Version 0.1
   For questions visit http://support.simplee.best */

.simplee-widget__input input {
    margin-right: 10px;
}
.simplee-widget__input-main, .simplee-widget__input-inner{
    display: flex;
    align-items: center;
}
.simplee-widget__input{
	padding: 15px 10px;
}
.simplee-widget__input-content {
    padding: 0 20px;
}
.simplee-widget__Label{
  	margin-bottom: 10px;
}
.simplee-widget__wrapper-inner{
  	border: 1px solid #dddddd;
}
.simplee-widget__hr{
  	border-bottom: 1px solid #dddddd;
}
.simplee-widget__hidden{
  	display: none;
}
.simplee-widget__visible{
  	display: block;
}

.fieldset--nested {
    display: block;
    animation: fadeInFromNone 100ms ease-in-out;
    margin-left: 1.8em;
    margin-bottom: 1em;
    padding: 0;
}
fieldset.simplee-widget{
    border: none;
    margin: 0px;
    padding: 27.5px 0px;
}
.simplee-widget fieldset{
	border:none;
  	margin-bottom:0;
}
.fieldset.simplee-widget .fieldset__legend {
    font-size: 0.73333em;
    letter-spacing: 0.3px;
    text-transform: uppercase;
    padding: 10px 0;
}
.simplee-widget input[type="radio"]:checked::before {
    background-image: radial-gradient(#000 50%, #fff 60%);
}
.simplee-widget input[type="radio"]::before {
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
    border-radius: 50%;
    border: 0.05em solid #000;
    box-sizing: border-box;
}
.simplee-widget .simplee-widget__input-inner {
    margin-bottom: 10px;
}
.simplee-widget input[type="radio"]:focus{
	outline:none;
}
.simplee-widget input[type="radio"] {
    color: #000;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    position: relative;
    padding: 0;
    border: 0;
    width: 45px;
    height: 45px;
    min-height: 45px;
    cursor: pointer;
    zoom: 0.35;
    margin-right: 25px;
}
.simplee-widget select {
    width: 100% !important;
}


/* Styles specific to themes */
.simplee_express_msl {
	margin:10px 0px;
}
.simplee_msl_box {
	border: 1px solid currentColor;
  	padding: 15px 30px;
  	display: inline-flex;
  	margin: 10px 0px;
}';
    }
}

//simplee js
if (!function_exists('addJSAsset')) {
    /**
     * @param $theme_id
     */
    function addJSAsset($theme_id, $user_id){
        try {
            \Log::info('-----------------------START :: addJSAsset -----------------------');
            $user = User::where('id', $user_id)->first();
            $cssCode = getJSCode();

            $parameter['asset']['key'] = 'assets/'. config('const.ASSETS.JS') .'.js';
            $parameter['asset']['value'] = $cssCode;
            $asset = $user->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);

            \Log::info(json_encode($asset));
        } catch (\Exception $e) {
            \Log::info('-----------------------ERROR :: addJSAsset -----------------------');
            \Log::info(json_encode($e));
        }
    }
}

// simplee js code
if (!function_exists('getJSCode')) {
    /**
     * @return string
     */
    function getJSCode()
    {
        // Asset Simplee JS
        return '// Simplee Storefront Widget - Script - Version 0.1
// For questions visit http://support.simplee.best

var simpleeSelectors = {};
var simpleeClasses = {};

var simpleeWidget = (function () {
    function simpleeWidget() {
      	console.log(\'Initializing simpleeWidget...\');

        simpleeSelectors = {
          sellingPlanGroupContainer: \'.simplee-widget__planGroup--container\',
          sellingPlanOptions: \'.simplee-widget__sellingPlan-options\',
          widget: \'.simplee-widget\',
          sellingPlanIdInput: \'.simplee-selling-plan-id-input\',
          productForm: \'form[action="/cart/add"]\',
          variantIdInput: \'[name="id"]\',
          variantSelector: [\'#shappify-variant-id\', \'.single-option-selector\', \'select[name=id]\', \'input[name=id]\'],
          pageTemplate: \'.simplee-page-template\',
          productJson: \'.simplee-product-json\',
          sellingPlanOptionName: \'.simplee-widget_sellingPlan_option_name\',
        };

    simpleeClasses = {
        hidden: \'simplee-widget__hidden\',
           visible: \'simplee-widget__visible\',

        };

      	this.products = {};
        this.variants = {};
        this.sellingPlanGroups = {};
        this.pageTemplate = "";
      	this.productId = {};
    }

simpleeWidget.prototype = Object.assign({}, simpleeWidget.prototype, {
    init: function () {
        this._parsePageTemplate();
        this._parseProductJson();
        this._addVariantChangeListener();
    },

    handleSellingPlanGroupChange: function (event) {
        let groupRadioEl = event.target;
          var groupId = groupRadioEl.value;
          let groupOptionWidget = groupRadioEl.parentNode.nextElementSibling;
          let planGroupContainer = document.querySelectorAll(simpleeSelectors.sellingPlanGroupContainer);
          var widget = groupRadioEl.closest(simpleeSelectors.widget);

          planGroupContainer.forEach(function(plansContainer){
            let hideSelector = plansContainer.querySelectorAll(simpleeSelectors.sellingPlanOptions);
            if(hideSelector.length){
                hideSelector[0].classList.add(simpleeClasses.hidden);
            }
          });
          (groupOptionWidget) ? groupOptionWidget.classList.remove("simplee-widget__hidden") : "";

          if (groupId === \'once\') {
              this._setSellingPlanIdInput(widget, "");
              return;
          }

            var sellingPlanId = this._getActiveSellingPlanId(widget, groupId);
            this._setSellingPlanIdInput(widget, sellingPlanId);
        },
    handleSellingPlanChange: function (event) {
        var planRadioEl = event.target;
        var widget = planRadioEl.closest(simpleeSelectors.widget);

        groupId = widget.querySelector(`input[name=simplee-sellingPlanGroup-radio]:checked`).value;
        var widget = planRadioEl.closest(simpleeSelectors.widget);
        var sellingPlanId = this._getActiveSellingPlanId(widget, groupId);
        this._setSellingPlanIdInput(widget, sellingPlanId);
    },

    _setSellingPlanIdInput: function (widget, sellingPlanId) {
        var sellingPlanIdInput = widget.querySelector(simpleeSelectors.sellingPlanIdInput);
        var variantId = this._getVariantId(widget);
        sellingPlanIdInput.value = sellingPlanId;
        if (/.*(product).*/.test(this.pageTemplate)) {
            this._updateHistoryState(variantId, sellingPlanId);
        }
        },

    _addVariantChangeListener: function () {
        var selectors = document.querySelectorAll(simpleeSelectors.variantSelector.join())
            selectors.forEach(function (select) {
            if (select) {
                select.addEventListener(\'change\', function (event) {
                    var productForm = event.target.closest(simpleeSelectors.productForm);
                    var widget = productForm.querySelector(simpleeSelectors.widget);

                    // NOTE: Variant change event needs to propagate to `input[name=id]`, so wait for that to happen...
                    setTimeout(function () {
                        this._renderPrices(widget);
                        this._renderGroupDiscountSummary(widget);
                    }.bind(this), 100)
                    }.bind(this));
                }
        }.bind(this));
        },
    _parsePageTemplate: function () {
        var pageTemplateInputEl = document.querySelector(simpleeSelectors.pageTemplate);
        if (pageTemplateInputEl === null) {
            return;
        }
        this.pageTemplate = pageTemplateInputEl.value;
    },

    _parseProductJson: function () {
        var productJsonElements = document.querySelectorAll(simpleeSelectors.productJson);
        productJsonElements.forEach(function (element) {
            var productJson = JSON.parse(element.innerHTML);
            this.productId = element.dataset.simpleeProductId;
            this.products[element.dataset.simpleeProductId] = productJson;

            productJson.selling_plan_groups.forEach(function (sellingPlanGroup) {
                this.sellingPlanGroups[sellingPlanGroup.id] = sellingPlanGroup;
            }.bind(this));

                productJson.variants.forEach(function (variant) {
                this.variants[variant.id] = variant;
            }.bind(this));
            }.bind(this));
        },

    _getVariantId: function (widget) {
        var productForm = widget.closest(simpleeSelectors.productForm);
        if (!productForm) {
            console.error(\'Error - no product form\');
            return null;
        }
        var variantIdInput = productForm.querySelector(simpleeSelectors.variantIdInput);

        return variantIdInput.value;
    },
    _getActiveSellingPlanId: function (widget, groupId) {
        var activePlanInputEl = widget.querySelector(
                `input[name=simplee-sellingPlan-Option-${groupId}]:checked`,
            );
        var activePlanValue = activePlanInputEl.value;
        if (!activePlanInputEl) {
            console.error(`Error - no plan for plangroup ${groupId}.`);
        }

        planName = activePlanInputEl.parentNode.parentNode.getAttribute("data-value");
        var selectedGroup = this._getSelectedSellingPlanGroup(groupId);
        var selectedPlanOptions = this._getCurrentSellingPlanOptions(activePlanInputEl, selectedGroup);
        var sellingPlan = selectedGroup.selling_plans.find(function(plan) {
                return plan.options.find(planOption => planOption.value == activePlanValue);
            });

        return sellingPlan.id;
    },
    _getCurrentSellingPlanOptions: function(activePlanInputEl, selectedGroup) {
        planName = activePlanInputEl.parentNode.parentNode.getAttribute("data-value");
        planOptions = selectedGroup.options;

        let currentOption = planOptions.find(option => option.name == planName)
            return currentOption;
      	},
    _getSelectedSellingPlanGroup: function(selectedGroupId){
        sellingPlanG = this.products[this.productId].selling_plan_groups;
        return found = sellingPlanG.find(planG => planG.id == selectedGroupId);
       },
    _updateHistoryState: function(variantId, sellingPlanId) {
        if (!history.replaceState || !variantId) {
            return;
        }

        var newurl =
            window.location.protocol +
            \'//\' +
            window.location.host +
            window.location.pathname +
            \'?\';

        if (sellingPlanId) {
            newurl += \'selling_plan=\' + sellingPlanId + \'&\';
        }

        newurl += \'variant=\' + variantId;
        window.history.replaceState({ path: newurl }, "", newurl);
      },

})

    return simpleeWidget;
})();

document.addEventListener(\'DOMContentLoaded\', function () {
    window.Simplee = window.Simplee || {};
    window.Simplee.simpleeWidget = new simpleeWidget();
    window.Simplee.simpleeWidget.init();
});
';
    }
}

if (!function_exists('paginateH')) {
    /**
     * @return array
     */
    function paginateH($entity)
    {
        return [
            'from' => $entity->firstItem(),
            'to' => $entity->lastItem(),
            'total' => $entity->total(),
            'count' => $entity->count(),
            'per_page' => $entity->perPage(),
            'current_page' => $entity->currentPage(),
            'total_pages' => $entity->lastPage(),
            'prev_page_url' => $entity->previousPageUrl(),
            'next_page_url' => $entity->nextPageUrl(),
        ];
    }
}

if (!function_exists('countryH')) {
    /**
     * @return array
     */
    function countryH()
    {
        $newsletter = app(CountryState::class);
        $countries = $newsletter->getCountries();
        asort($countries);
        return $countries;
    }
}

if (!function_exists('stateFromCountryH')) {
    /**
     * @return array
     */
    function stateFromCountryH($country)
    {
        $newsletter = app(CountryState::class);
        return $newsletter->getStates($country);
    }
}

if (!function_exists('installWidgetH')) {
    /**
     * @return array
     */
    function installWidgetH($theme_id, $user_id)
    {
        try {
            \Log::info('-----------------------START :: installWidgetH -----------------------');

            addSnippetH($theme_id, $user_id, true);
            addSimpleeWidgetSnippetH($theme_id, $user_id);
            addSimpleeCartWidgetSnippetH($theme_id, $user_id);
            addCSSAsset($theme_id, $user_id);
            addJSAsset($theme_id, $user_id);
            \Log::info('-----------------------END :: installWidgetH -----------------------');
        } catch (\Exception $e) {
            \Log::info('-----------------------ERROR :: installWidgetH -----------------------');
            \Log::info(json_encode($e));
        }
    }
}

if (!function_exists('simpleeWidgetTextH')) {
    /**
     * @return array
     */
    function simpleeWidgetTextH()
    {
        return <<<EOF
{%- comment -%} Simplee Storefront Widget - Product Page - Version 0.1 {%- endcomment -%}
{%- comment -%} For questions visit http://support.simplee.best  {%- endcomment -%}

{% if product.selling_plan_groups.size > 0 %}
<fieldset class="fieldset simplee-widget" role="{%- if product.requires_selling_plan == false or product.selling_plan_groups.size > 1 -%} radiogroup {%- else -%} group {%- endif -%}">
    <div class="simplee-widget__wrapper">

    <legend class="simplee-widget__Label">
      {%- if product.requires_selling_plan and product.selling_plan_groups.size == 1 -%}
      	{{ product.selling_plan_groups.first.name }}
      {%- else -%}
      	Purchase Options
      <!-- {{ 'products.product.purchase_options' | t }} -->
    {%- endif -%}
    </legend>

    <div class="simplee-widget__wrapper-inner">

  <!--     one time purchase -->
      {% unless product.requires_selling_plan == true %}
         <div class="simplee-widget__planGroup--container simplee-widget__input simplee-widget__hr">
            <div class="simplee-widget__input-main">
                <input
                       type="radio" name="simplee-sellingPlanGroup-radio" value="once"
                       class="simplee-widget__input-inner"
                       id="sellingPlan--{{section.id}}--onetimePurchase"
                       onchange="window.Simplee.simpleeWidget.handleSellingPlanGroupChange(event)"
                       {%- unless current_selling_plan_allocation -%} checked {%- endunless -%}/>
                <label for="sellingPlan--{{section.id}}--onetimePurchase">
                  One-time Purchase
<!--                   {{ 'products.product.one_time_purchase' | t }} -->
              </label>
            </div>
         </div>
      {% endunless %}

       <!-- selling plan group radio -->
      {% for group in product.selling_plan_groups %}
       <div class="simplee-widget__planGroup--container simplee-widget__input simplee-widget__hr">
          <div class="simplee-widget__input-main">
            <input type="radio" name="simplee-sellingPlanGroup-radio" value="{{group.id}}"
                   id="sellingPlan-Group-{{section.id}}-{{group.id}}"
                   class="simplee-widget__input-inner"
                   onchange="window.Simplee.simpleeWidget.handleSellingPlanGroupChange(event)"
                   {% if group.id == current_selling_plan_allocation.selling_plan.group_id %}checked{% endif %}/>
            <label for="sellingPlan-Group-{{section.id}}-{{group.id}}">{{- group.name -}}</label>
          </div>
         {% for option in group.options %}
         	{% assign forloopIndex = forloop.index0 %}
            <fieldset class="fieldset simplee-widget__input-content simplee-widget__sellingPlan-options
                             {% unless current_selling_plan_allocation.selling_plan.group_id == group.id %} simplee-widget__hidden {% endunless %}" data-value="{{option.name}}">
            	<legend class="fieldset__legend simplee-widget_sellingPlan_option_name">
              		{{ option.name }}
              	</legend>
                {% for value in option.values %}
                  <div class="simplee-widget__input-inner">
                    <input
                           type="radio" name="simplee-sellingPlan-Option-{{ group.id }}" value="{{value}}"
                           id="sellingPlan-Option-{{section.id}}-{{group.id}}-{{ forloopIndex }}-{{ value | handleize }}"
                           class="simplee-widget__input-inner"
                           onchange="window.Simplee.simpleeWidget.handleSellingPlanChange(event)"
                           {%- if option.selected_value == nil and forloop.first -%} checked {%- endif -%}
                           {%- if value == option.selected_value -%} checked {%- endif -%}
                     />
                    <label for="sellingPlan-Option-{{section.id}}-{{group.id}}-{{ forloopIndex }}-{{ value | handleize }}">
                       {{- value -}}
                    </label>
                  </div>
                {% endfor %}
           </fieldset>
         {% endfor %}
      	</div>
      {% endfor %}
    </div>

  </div>
<input
    type="hidden"
    name="selling_plan"
    class="simplee-selling-plan-id-input"
    value="{{ current_selling_plan_allocation.selling_plan.id }}"
  />

  <script
    type="application/json"
    class="simplee-product-json"
    data-simplee-product-id="{{ product.id }}"
  >
    {{ product | json }}
  </script>
</fieldset>

<script
  type="application/json"
  data-simplee-money-format="{{shop.money_format}}"
></script>

<input
  type="hidden"
  class="simplee-page-template"
  value="{{ template }}"
/>
{% endif %}
EOF;
    }
}

if (!function_exists('updateFilesH')) {
    /**
     * @return array
     */
    function updateFilesH($themeData, $user_id)
    {
        $constants = getConstantH($themeData);
//        update product-template.liquid
        updateProductLiquidH($themeData, $user_id, $constants);
//        update cart-template.liquid
        updateCartLiquidH(config('const.SNIPPETS.CART'), $themeData['id'], $user_id, $constants);
//        update customer/account.liquid
        updateCustomerLiquidH($themeData, $user_id, $constants);

    }
}
if (!function_exists('updateProductLiquidH')) {
    /**
     * @return array
     */
    function updateProductLiquidH($themeData, $user_id, $constants)
    {
        $user = User::find($user_id);

        $asset = getLiquidAssetH($themeData['id'], $user_id, $constants['PRODUCT_FILE']);
        if( $asset != '' ){
            if (!strpos($asset, "{% render '".config('const.SNIPPETS.SIMPLEE_WIDGET')."' %}")) {
                $asset = str_replace($constants['PRODUCT_PAGE_PLACE'], "{% render '".config('const.SNIPPETS.SIMPLEE_WIDGET')."' %}". ' ' . $constants['PRODUCT_PAGE_PLACE'], $asset);
            }

            $parameter['asset']['key'] = $constants['PRODUCT_FILE'];
            $parameter['asset']['value'] = $asset;
            $result = $user->api()->rest('PUT', 'admin/themes/'.$themeData['id'].'/assets.json', $parameter);
        }
    }
}

if (!function_exists('updateCustomerLiquidH')) {
    /**
     * @return array
     */
    function updateCustomerLiquidH($themeData, $user_id, $constants)
    {
        $user = User::find($user_id);

        $asset = getLiquidAssetH($themeData['id'], $user_id, config('const.FILES.ACCOUNT'));
        if( $asset != '' ){
            if (!strpos($asset, $constants['ACCOUNT_PAGE_URL'])) {
                $asset = str_replace($constants['ACCOUNT_PAGE_PLACE'], $constants['ACCOUNT_PAGE_PLACE']. ' ' . $constants['ACCOUNT_PAGE_URL'], $asset);
            }

            $parameter['asset']['key'] = config('const.FILES.ACCOUNT');
            $parameter['asset']['value'] = $asset;
            $result = $user->api()->rest('PUT', 'admin/themes/'.$themeData['id'].'/assets.json', $parameter);
        }
    }
}

if (!function_exists('getConstantH')) {
    /**
     * @return array
     */
    function getConstantH($theme)
    {
        $name = strtoupper($theme['name']);
        $version = str_replace('.', '_', $theme['version']);
        $const = 'const.THEME';

        $constants = (config("$const.$name.$version")) ? config("$const.$name.$version") : config("$const.$name.*");
        logger($constants);
        return ($constants) ? $constants : (config("$const.*"));
    }
}

if (!function_exists('getLiquidAssetH')) {
    /**
     * @return string
     */
    function getLiquidAssetH($theme_id, $user_id, $file)
    {
        $user = User::find($user_id);

        $asset = $user->api()->rest('GET', 'admin/themes/'.$theme_id.'/assets.json',
            ["asset[key]" => $file]);

        return (@$asset['body']->container['asset']['value']) ? $asset['body']->container['asset']['value'] : '';
    }
}
