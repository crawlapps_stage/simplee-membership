<?php

namespace App\Exports;

use App\Models\SsContract;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SubscribersExport implements FromCollection, WithHeadings
{
    private $shopID;
    private $type;
    private $s;
    public function __construct($type, $s, $id)
    {
        \Log::info('Export maatwebsite');
        $this->shopID = $id;
        $this->type = $type;
        $this->s = $s;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $entities = $this->getData();
        $entity = $this->createFile($entities);
        return collect($entity);
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        $s = $this->s;
        $f = $this->type;

        $subscriber = collect([]);

        if ($f == 'all') {
            $subscriber = SsContract::select('ss_contracts.id', 'ss_contracts.shop_id', 'ss_contracts.ss_customer_id', 'ss_contracts.status', 'ss_contracts.order_count', 'ss_customers.first_name', 'ss_customers.last_name', 'ss_customers.email', 'ss_customers.phone')->join('ss_customers', 'ss_contracts.ss_customer_id', '=', 'ss_customers.id')->where(function ($query) use ($s) {
                $query->where('ss_contracts.status', 'LIKE', '%' . $s . '%')->orWhere('order_count', 'LIKE', '%' . $s . '%')->orWhere('first_name', 'LIKE', '%' . $s . '%')->orWhere('last_name', 'LIKE', '%' . $s . '%')->orWhere('email', 'LIKE', '%' . $s . '%')->orWhere('phone', 'LIKE', '%' . $s . '%');
            })->where('ss_contracts.shop_id', $this->shopID)->where('ss_customers.shop_id', $this->shopID)->orderBy('ss_contracts.created_at', 'desc')->get();

        } else {
            $subscriber = SsContract::select('ss_contracts.id', 'ss_contracts.shop_id', 'ss_contracts.ss_customer_id', 'ss_contracts.status', 'ss_contracts.order_count', 'ss_customers.first_name', 'ss_customers.last_name', 'ss_customers.email', 'ss_customers.phone')->join('ss_customers', 'ss_contracts.ss_customer_id', '=', 'ss_customers.id')
                ->where(function ($query) use ($f) {
                    $query->where('ss_contracts.status', 'LIKE', '%' . $f . '%');
                })
                ->where(function ($query) use ($s) {
                    $query->Where('order_count', 'LIKE', '%' . $s . '%')->orWhere('first_name', 'LIKE', '%' . $s . '%')->orWhere('last_name', 'LIKE', '%' . $s . '%')->orWhere('email', 'LIKE', '%' . $s . '%')->orWhere('phone', 'LIKE', '%' . $s . '%');
                })
                ->where('ss_contracts.shop_id', $this->shopID)->where('ss_customers.shop_id', $this->shopID)->orderBy('ss_contracts.created_at', 'desc')->get();
        }

        // if( $t == 'all' ){
        //     $subscriber = SsCustomer::where('shop_id', $this->shopID)
        //         ->where(function ($query) use ($s) {
        //             $query->where('first_name', 'LIKE', '%'.$s.'%')->orWhere('last_name', 'LIKE', '%'.$s.'%')->orWhere('email', 'LIKE', '%'.$s.'%')->orWhere('phone', 'LIKE', '%'.$s.'%');
        //         })->orderBy('created_at', 'desc')->get();
        // } elseif( $t == 'active' || $t == 'inactive' ){
        //     $a = ( $t == 'active' ) ? 1 : 0;
        //     $subscriber = SsCustomer::where('shop_id', $this->shopID)->where('active', $a)
        //         ->where(function ($query) use ($s) {
        //             $query->where('first_name', 'LIKE', '%'.$s.'%')->orWhere('last_name', 'LIKE', '%'.$s.'%')->orWhere('email', 'LIKE', '%'.$s.'%')->orWhere('phone', 'LIKE', '%'.$s.'%');
        //         })->orderBy('created_at', 'desc')->get();
        // }else{
        //     $subscriber = collect([]);
        // }
        return $subscriber;
    }

    /**
     * @param $entities
     * @return mixed
     */
    public function createFile($entities)
    {
        \Log::info('----------create File--------------');
        if ($entities) {
            $entity = $entities->map(function ($name) {
                $data = [
                    'status' => ucfirst($name->status),
                    'name' => $name->first_name . ' ' . $name->last_name,
                    'email' => $name->email,
                    'phone' => $name->phone,
                    'total_orders' => ($name->order_count > 1) ? $name->order_count . ' orders' : $name->order_count . ' order',
                ];
                return $data;
            });
        }
        return $entity;
    }

    public function headings(): array
    {
        return [
            'status',
            'name',
            'email',
            'phone',
            'total_orders',
        ];
    }
}
