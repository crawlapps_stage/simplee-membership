<?php

namespace App\Listeners;

use App\Events\CheckCustomerPaymentMethodUpdate;
use App\Models\SsContract;
use App\Models\SsWebhook;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

class CustomerPaymentMethodUpdate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CheckCustomerPaymentMethodUpdate  $event
     * @return void
     */
    public function handle(CheckCustomerPaymentMethodUpdate $event)
    {
        logger('========== Listener:: CustomerPaymentMethodUpdate ==========');
        try {
            $ids = $event->ids;
            $webhookResonse = SsWebhook::find($ids['webhook_id']);
            if ($webhookResonse) {
                $data = json_decode($webhookResonse->body);
                // check for payment method and update in DB

                $db_contracts = SsContract::where('shop_id', $ids['shop_id'])->where('user_id', $ids['user_id'])->where('cc_id', $data->token)->where('shopify_customer_id', $data->customer_id)->pluck('id');

                logger($db_contracts);
                $payment_instrument = $data->payment_instrument;

                if ($data->instrument_type == 'CustomerCreditCard') {
                    SsContract::whereIn('id', $db_contracts)->update(['payment_method' => 'credit_card', 'cc_brand' => $payment_instrument->brand, 'cc_expiryMonth' => $payment_instrument->month, 'cc_expiryYear' => $payment_instrument->year, 'cc_lastDigits' => $payment_instrument->last_digits, 'cc_name' => $payment_instrument->name]);
                } elseif ($data->instrument_type == 'CustomerPaypalBillingAgreement') {
                    SsContract::whereIn('id', $db_contracts)->update(['payment_method' => 'paypal', 'paypal_account' => $payment_instrument->paypalAccountEmail, 'paypal_inactive' => $payment_instrument->inactive, 'paypal_isRevocable' => $payment_instrument->isRevocable]);
                }

//                if( count($db_contracts) > 0 ){

//                    foreach ( $db_contracts as $key=>$val ){
                //                        $val->cc_brand = $payment_instrument->brand;
                //                    }
                //                }
            }
        } catch (\Exception $e) {
            logger('========== ERROR:: CustomerPaymentMethodUpdate ==========');
            logger($e);
            Bugsnag::notifyException($e);
        }
    }
}
