<?php

namespace App\Listeners;

use App\Events\CheckSubscriptionContract;
use App\Models\Shop;
use App\Models\SsBillingAttempt;
use App\Models\SsContract;
use App\Models\SsContractLineItem;
use App\Models\SsEmail;
use App\Models\SsPlan;
use App\Models\SsSetting;
use App\Models\SsWebhook;
use App\Models\SsCustomer;
use App\Traits\ShopifyTrait;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;

class SubscriptionContract
{
    use ShopifyTrait;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CheckSubscriptionContract  $event
     * @return void
     */
    public function handle(CheckSubscriptionContract $event)
    {
        try {
            logger('========== Listener:: SubscriptionContract ==========');
            $ids = $event->ids;
            $user = User::find($ids['user_id']);
            $shop = Shop::find($ids['shop_id']);
            $webhookResonse = SsWebhook::find($ids['webhook_id']);
            if ($webhookResonse) {
                $data = json_decode($webhookResonse->body);

                $shopify_contract_id = str_replace('gid://shopify/SubscriptionContract/', '', $data->admin_graphql_api_id);

                $is_exist_db_contract = SsContract::where('shopify_contract_id', $shopify_contract_id)->where('shop_id', $shop->id)->first();

                // ADD FOR testing
                $ssContractQuery = $this->subscriptionContractLineItems($data->admin_graphql_api_id);
                $ssContractResult = $this->graphQLRequest($user->id, $ssContractQuery);
                logger(json_encode($ssContractResult));
                // remove after testing done

                if (!$is_exist_db_contract) {
                    // update or create customer in db
                    $shopify_customer_id = $data->customer_id;
                    $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/customers/' . $shopify_customer_id . '.json';
                    $customerEndPoint = $endPoint;
                    $result = $user->api()->rest('GET', $endPoint);
                    if (!$result['errors']) {
                        $sh_customer = $result['body']['customer'];

                        $is_existcustomer = SsCustomer::where('shop_id', $ids['shop_id'])->where('shopify_customer_id',
                            $shopify_customer_id)->first();

                        $customer = ($is_existcustomer) ? $is_existcustomer : new SsCustomer;
                        // $customer = new SsCustomer;
                        $customer->shop_id = $ids['shop_id'];
                        $customer->shopify_customer_id = $sh_customer->id;
                        $customer->active = 1;
                        $customer->first_name = $sh_customer->first_name;
                        $customer->last_name = $sh_customer->last_name;
                        $customer->email = $sh_customer->email;
                        $customer->phone = $sh_customer->phone;
                        $customer->notes = $sh_customer->note;
                        $customer->total_orders = ($is_existcustomer) ? $is_existcustomer->total_orders + 1 : 1;

                        $customer->total_spend_currency = $sh_customer->currency;
                        $customer->currency_symbol = currencyH($sh_customer->currency);
                        $customer->avg_order_value = ($sh_customer->orders_count > 0) ? preg_replace('/[^0-9 .]/s', '', number_format(($sh_customer->total_spent / $sh_customer->orders_count), 2)) : 0;
                        $customer->date_first_order = ($is_existcustomer && $is_existcustomer->date_first_order) ? $is_existcustomer->date_first_order : date('Y-m-d H:i:s');
                        $customer->save();
                    }

                    // get subscription contract from shopify

                    $ssContractQuery = $this->subscriptionContractLineItems($data->admin_graphql_api_id);
                    $ssContractResult = $this->graphQLRequest($user->id, $ssContractQuery);

                    logger(json_encode($ssContractResult));
                    if (!$ssContractResult['errors']) {
                        $subscriptionContract = $ssContractResult['body']->container['data']['subscriptionContract'];

                        $sh_lines = (@$subscriptionContract['lines']['edges']) ? $subscriptionContract['lines']['edges'] : [];

                        logger("======================= sh_lines ======================");
                        logger(json_encode($sh_lines));
                        logger("======================= sh_lines ======================");
                        $sh_cpm = (@$subscriptionContract['customerPaymentMethod']['instrument']) ? $subscriptionContract['customerPaymentMethod']['instrument'] : [];
                        $sh_deliveryMethod = (@$subscriptionContract['deliveryMethod']) ? $subscriptionContract['deliveryMethod'] : [];

                        // add contract

                        $billing_policy = $data->billing_policy->interval_count . $data->billing_policy->interval;
                        $delivery_policy = $data->delivery_policy->interval_count . $data->delivery_policy->interval;

                        $contract = new SsContract;
                        $contract->shop_id = $ids['shop_id'];
                        $contract->user_id = $user->id;
                        $contract->shopify_contract_id = $data->id;
                        $contract->shopify_customer_id = $shopify_customer_id;
                        $contract->ss_customer_id = $customer->id;
                        $contract->origin_order_id = $data->origin_order_id;
                        $contract->status = $data->status;
                        
                        // $addtime = $this->getSubscriptionTime($shop->id);
                        // Assuming this is regular subscription
                        // without anchor and cut-off days
                        $nextOrderDate = $this->getSubscriptionTimeDate(date("Y-m-d",
                            strtotime($subscriptionContract['nextBillingDate'])), $shop->id);

                        // $utc_next_date = $this->getSubscriptionTimeDate(date("Y-m-d",
                        //    strtotime($merchant_next_date)), $shop->id, date("H:i:s",
                        //    strtotime($merchant_next_date)), 'UTC');

                        // $next_order_date = date("Y-m-d H:i:s",
                        //     strtotime($subscriptionContract['nextBillingDate']));
                                                
                        $sellingPlanId = $subscriptionContract['lines']['edges'][0]['node']['sellingPlanId'];
                        $sellingPlanId = str_replace('gid://shopify/SellingPlan/', '', $sellingPlanId);

                        $sellingPlan = SsPlan::where([
                          ['shop_id', $shop->id],
                          ['user_id', $user->id],
                          ['shopify_plan_id', $sellingPlanId]
                        ])->first();
                        
                        if (isset($sellingPlan->delivery_cutoff) || isset($sellingPlan->billing_anchor_type)) {
                          $nextOrderDate = $this->calculateNextOrderDate($sellingPlan, $shop);
                        }

                        // Add next order date to contract
                        $contract->next_order_date = $nextOrderDate;
                        $contract->next_processing_date = $nextOrderDate;

                        $deliveryAnchors = $subscriptionContract['deliveryPolicy']['anchors'];
                        $billingAnchors = $subscriptionContract['billingPolicy']['anchors'];

                        $contract->is_prepaid = strcmp($billing_policy, $delivery_policy);
                        $contract->prepaid_renew = 0;
                        $contract->last_billing_order_number = $data->origin_order_id;

                        $contract->billing_interval = (@$data->billing_policy->interval) ? $data->billing_policy->interval : '';
                        $contract->billing_interval_count = (@$data->billing_policy->interval_count) ? $data->billing_policy->interval_count : 0;
                        $contract->billing_min_cycles = (@$data->billing_policy->min_cycles) ? $data->billing_policy->min_cycles : 0;
                        $contract->billing_max_cycles = (@$data->billing_policy->max_cycles) ? $data->billing_policy->max_cycles : 0;
                        $contract->billing_anchor_day = (!empty($billingAnchors)) ? $billingAnchors[0]['day'] : null;
                        $contract->billing_anchor_type = (!empty($billingAnchors)) ? $billingAnchors[0]['type'] : null;
                        $contract->billing_anchor_month = (!empty($billingAnchors)) ? $billingAnchors[0]['month'] : null;

                        $contract->delivery_anchor_day = (!empty($deliveryAnchors)) ? $deliveryAnchors[0]['day'] : null;
                        $contract->delivery_anchor_type = (!empty($deliveryAnchors)) ? $deliveryAnchors[0]['type'] : null;
                        $contract->delivery_anchor_month = (!empty($deliveryAnchors)) ? $deliveryAnchors[0]['month'] : null;

                        $contract->delivery_intent = (!empty($deliveryAnchors)) ? 'A fixed day each delivery cycle' : 'The initial day of purchase';
                        $contract->delivery_interval = $data->delivery_policy->interval;
                        $contract->delivery_interval_count = $data->delivery_policy->interval_count;

                        $contract->delivery_price = (@$subscriptionContract['deliveryPrice']['amount']) ? $subscriptionContract['deliveryPrice']['amount'] : 0;
                        $contract->delivery_price_currency_symbol = currencyH($subscriptionContract['deliveryPrice']['currencyCode']);

                        $contract->currency_code = $data->currency_code;
                        $contract->lastPaymentStatus = $subscriptionContract['lastPaymentStatus'];
                        $contract->order_count = 1;

                        // store payment method details

                        $contract->payment_method = (@$sh_cpm['paypalAccountEmail']) ? 'paypal' : 'credit_card';
                        $contract->paypal_account = (@$sh_cpm['paypalAccountEmail']) ? $sh_cpm['paypalAccountEmail'] : '';
                        $contract->paypal_inactive = (@$sh_cpm['paypal_inactive']) ? $sh_cpm['paypal_inactive'] : '';
                        $contract->paypal_isRevocable = (@$sh_cpm['paypal_isRevocable']) ? $sh_cpm['paypal_isRevocable'] : false;

                        $contract->cc_id = str_replace('gid://shopify/CustomerPaymentMethod/', '',
                            $subscriptionContract['customerPaymentMethod']['id']);
                        $contract->cc_brand = (@$sh_cpm['brand']) ? $sh_cpm['brand'] : '';
                        $contract->cc_expiryMonth = (@$sh_cpm['expiryMonth']) ? $sh_cpm['expiryMonth'] : 0;
                        $contract->cc_expires_soon = (@$sh_cpm['expiresSoon']) ? $sh_cpm['expiresSoon'] : 0;
                        $contract->cc_expiryYear = (@$sh_cpm['expiryYear']) ? $sh_cpm['expiryYear'] : 0;
                        $contract->cc_firstDigits = (@$sh_cpm['firstDigits']) ? $sh_cpm['firstDigits'] : 0;
                        $contract->cc_lastDigits = (@$sh_cpm['lastDigits']) ? $sh_cpm['lastDigits'] : 0;
                        $contract->cc_maskedNumber = (@$sh_cpm['maskedNumber']) ? $sh_cpm['maskedNumber'] : '';
                        $contract->cc_name = (@$sh_cpm['name']) ? $sh_cpm['name'] : '';

                        $ship_address = (@$sh_deliveryMethod['address']) ? $sh_deliveryMethod['address'] : [];

                        $contract->ship_company = (@$ship_address['company']) ? $ship_address['company'] : '';
                        $contract->ship_firstName = (@$ship_address['firstName']) ? $ship_address['firstName'] : '';
                        $contract->ship_lastName = (@$ship_address['lastName']) ? $ship_address['lastName'] : '';
                        $contract->ship_provinceCode = (@$ship_address['provinceCode']) ? $ship_address['provinceCode'] : '';
                        $contract->ship_name = (@$ship_address['name']) ? $ship_address['name'] : '';
                        $contract->ship_address1 = (@$ship_address['address1']) ? $ship_address['address1'] : '';
                        $contract->ship_address2 = (@$ship_address['address2']) ? $ship_address['address2'] : '';
                        $contract->ship_city = (@$ship_address['city']) ? $ship_address['city'] : '';
                        $contract->ship_province = (@$ship_address['province']) ? $ship_address['province'] : '';
                        $contract->ship_zip = (@$ship_address['zip']) ? $ship_address['zip'] : '';
                        $contract->ship_country = (@$ship_address['country']) ? $ship_address['country'] : '';
                        $contract->ship_phone = (@$ship_address['phone']) ? $ship_address['phone'] : '';

                        $ship_option = (@$sh_deliveryMethod['shippingOption']) ? $sh_deliveryMethod['shippingOption'] : [];

                        $contract->shipping_code = (@$ship_option['code']) ? $ship_option['code'] : '';
                        $contract->shipping_description = (@$ship_option['description']) ? $ship_option['description'] : '';
                        $contract->shipping_presentmentTitle = (@$ship_option['presentmentTitle']) ? $ship_option['presentmentTitle'] : '';
                        $contract->shipping_title = (@$ship_option['title']) ? $ship_option['title'] : '';

                        $contract->failed_payment_count = 0;

                        $contract->is_multicurrency = ($contract->currency_code != $shop->currency) ? true : false;
                        $contract->save();
                        // add contract line items

                        $is_added_cutoff = false;
                        if (is_array($sh_lines) && !empty($sh_lines)) {
                            foreach ($sh_lines as $key => $value) {
                                $node = $value['node'];

                                $db_plan = SsPlan::where('shop_id', $shop->id)->where('user_id',
                                    $user->id)->where('shopify_plan_id',
                                    str_replace('gid://shopify/SellingPlan/', '', $node['sellingPlanId']))->first();
                                $pricingPolicy = $node['pricingPolicy'];

                                if ($db_plan) {

                                    if (!$is_added_cutoff) {
                                        $contract->delivery_cutoff = $db_plan->delivery_cutoff;
                                        $contract->delivery_pre_cutoff_behaviour = $db_plan->delivery_pre_cutoff_behaviour;

                                        $contract->save();
                                        $is_added_cutoff = true;
                                    }

                                    $pricing_adjustment_type = $db_plan->pricing_adjustment_type;
                                    $pricing_adjustment_value = $db_plan->pricing_adjustment_value;

                                    $price = preg_replace('/[^0-9 .]/s', '', $pricingPolicy['basePrice']['amount']);

                                    $discount = 0;
                                    if ($pricing_adjustment_value != null && $pricing_adjustment_value != '') {
                                        $discount = ($pricing_adjustment_type == '%') ? (($price * $pricing_adjustment_value) / 100) : $pricing_adjustment_value;
                                    }
                                    $discounted_price = $price - $discount;
                                } else {
                                    $price = preg_replace('/[^0-9 .]/s', '', $pricingPolicy['basePrice']['amount']);
                                    $discounted_price = $price;
                                    $pricing_adjustment_value = ($price - $discounted_price);
                                }

                                $lineItems = new SsContractLineItem;
                                $lineItems->shopify_contract_id = $data->id;
                                $lineItems->ss_contract_id = $contract->id;
                                $lineItems->user_id = $user->id;
                                $lineItems->shopify_line_id = str_replace('gid://shopify/SubscriptionLine/', '', $node['id']);
                                $lineItems->shopify_product_id = str_replace('gid://shopify/Product/', '', $node['productId']);
                                $lineItems->shopify_variant_id = str_replace('gid://shopify/ProductVariant/', '',
                                    $node['variantId']);
                                $lineItems->price = preg_replace('/[^0-9 .]/s', '', number_format($pricingPolicy['basePrice']['amount'], 2));
                                $lineItems->price_discounted = preg_replace('/[^0-9 .]/s', '', number_format($discounted_price, 2));
                                $lineItems->currency = (@$node['currentPrice']['currencyCode']) ? $node['currentPrice']['currencyCode'] : '';
                                $lineItems->currency_symbol = currencyH($node['currentPrice']['currencyCode']);
                                $lineItems->discount_type = ($pricingPolicy['cycleDiscounts'][0]['adjustmentType'] == 'PERCENTAGE') ? '%' : currencyH($node['currentPrice']['currencyCode']);
                                $lineItems->discount_amount = $pricing_adjustment_value;
                                $lineItems->final_amount = $discounted_price;
                                $lineItems->quantity = (@$node['quantity']) ? $node['quantity'] : 0;
                                $lineItems->selling_plan_id = str_replace('gid://shopify/SellingPlan/', '',
                                    $node['sellingPlanId']);
                                $lineItems->selling_plan_name = $node['sellingPlanName'];
                                $lineItems->sku = (@$node['sku']) ? $node['sku'] : '';
                                $lineItems->taxable = (@$node['taxable']) ? $node['taxable'] : 0;
                                $lineItems->title = (@$node['title']) ? $node['title'] : '';
                                $lineItems->shopify_variant_image = (@$node['variantImage']['originalSrc']) ? $node['variantImage']['originalSrc'] : '';
                                $lineItems->shopify_variant_title = (@$node['variantTitle']) ? $node['variantTitle'] : '';
                                $lineItems->requiresShipping = (@$node['requiresShipping']) ? $node['requiresShipping'] : 0;

                                $lineItems->save();

                            }
                        }
                    }

                    //update customer
                    if ($customer) {
                        $sh_order = $this->getShopifyOrder($user, $data->origin_order_id);
                        $customer->total_spend = (@$sh_order['total_price']) ? $customer->total_spend + $sh_order['total_price'] : $customer->total_spend;
                        $customer->avg_order_value = ($customer->total_orders > 0) ? preg_replace('/[^0-9_ .]/s', '', number_format(($customer->total_spend / $customer->total_orders), 2)) : $customer->total_spend;
                        $customer->save();
                    }
                    // create order
                    $order = $this->createOrder($user->id, $shop->id, $data->origin_order_id, $customer->id, $contract->id);

                    //add billing attempt

                    $billingAttempt = new SsBillingAttempt;
                    $billingAttempt->shop_id = $shop->id;
                    $billingAttempt->ss_contract_id = $contract->id;
                    $billingAttempt->status = 'successful';
                    $billingAttempt->completedAt = date('Y-m-d H:i:s');
                    $billingAttempt->shopify_contract_id = $contract->shopify_contract_id;
                    $billingAttempt->shopify_order_id = $contract->origin_order_id;
                    $billingAttempt->save();

                    $setting = SsSetting::select('new_subscription_email_enabled', 'email_from_email', 'email_from_name')->where('shop_id', $shop->id)->first();
                    logger(json_encode($setting));
                    if ($setting->new_subscription_email_enabled) {
                        logger(json_encode($setting));
                        $email = SsEmail::where('shop_id', $shop->id)->where('category', 'new_subscription_to_customer')->first();

                        $res = sendMailH($email->subject, $email->html_body, $setting->email_from_email, $customer->email, $setting->email_from_name, $shop->id, $customer->id);
                        logger($res);
                    }

                    // Add customer and order tags
                    logger('============== START:: AddCustomerAndOrderTags ===========');
                    $settings = SsSetting::where('shop_id', $shop->id)->first();
                    $customer_tags = $settings->tag_customer;

                    $customerParameter = [
                        'customer' => [
                            'id' => $shopify_customer_id,
                            'tags' => $customer_tags,
                        ],
                    ];

                    $order_tags = $settings->tag_order;

                    $orderParameter = [
                        'order' => [
                            'id' => $data->origin_order_id,
                            'tags' => $order_tags,
                        ],
                    ];

                    $orderEndPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/orders/' . $data->origin_order_id . '.json';
                    $customerParameterResult = $user->api()->rest('PUT', $customerEndPoint, $customerParameter);

                    $orderParameterResult = $user->api()->rest('PUT', $orderEndPoint, $orderParameter);
                    logger('============== END:: AddCustomerAndOrderTags ===========');
                }
            }
        } catch (\Exception $e) {
            logger($e);
            Bugsnag::notifyException($e);
        }
    }

    public function subscriptionContractLineItems($contractId)
    {
        $query = '
                {
                        subscriptionContract(id: "' . $contractId . '") {
                          lastPaymentStatus
                          nextBillingDate
                          billingPolicy {
                              interval
                              intervalCount
                              maxCycles
                              minCycles
                              anchors {
                                day
                                month
                                type
                              }
                            }
                            deliveryPolicy {
                              interval
                              intervalCount
                              anchors {
                                day
                                month
                                type
                              }
                            }
                            deliveryPrice {
                              amount
                              currencyCode
                            }
                            lines (first: 200){
                              edges {
                                node {
                                  id
                                  productId
                                  variantId
                                  currentPrice {
                                    amount
                                    currencyCode
                                  }
                                  pricingPolicy {
                                    basePrice {
                                      amount
                                      currencyCode
                                    }
                                    cycleDiscounts {
                                      adjustmentType
                                      afterCycle
                                      computedPrice {
                                        amount
                                        currencyCode
                                      }
                                    }
                                  }
                                  variantImage {
                                    originalSrc
                                  }
                                  sku
                                  title
                                  quantity
                                  taxable
                                  sellingPlanName
                                  sellingPlanId
                                  requiresShipping
                                  variantTitle
                                  lineDiscountedPrice {
                                    amount
                                    currencyCode
                                  }
                                }
                              }
                            }
                            deliveryMethod {
                            ... on SubscriptionDeliveryMethodShipping {
                              __typename
                              address {
                                 address1
                                address2
                                city
                                company
                                country
                                countryCode
                                firstName
                                lastName
                                name
                                phone
                                province
                                provinceCode
                                zip
                              }
                              shippingOption {
                                code
                                description
                                presentmentTitle
                                title
                              }
                            }
                          }
                          customerPaymentMethod {
                              id
                              instrument {
                                ... on CustomerCreditCard {
                                  firstDigits
                                  brand
                                  expiresSoon
                                  expiryMonth
                                  expiryYear
                                  isRevocable
                                  lastDigits
                                  maskedNumber
                                  name
                                }
                                ... on CustomerPaypalBillingAgreement {
                                  paypalAccountEmail
                                  inactive
                                  isRevocable
                                }
                              }
                              revokedAt
                            }
                      }
                }
            ';
        return $query;
    }
}
