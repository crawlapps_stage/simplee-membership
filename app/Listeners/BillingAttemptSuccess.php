<?php

namespace App\Listeners;

use App\Events\CheckBillingAttemptSuccess;
use App\Models\Shop;
use App\Models\SsBillingAttempt;
use App\Models\SsContract;
use App\Models\SsSetting;
use App\Models\SsWebhook;
use App\Models\SsCustomer;
use App\Traits\ShopifyTrait;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

class BillingAttemptSuccess
{
    use ShopifyTrait;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CheckBillingAttemptSuccess  $event
     * @return void
     */
    public function handle(CheckBillingAttemptSuccess $event)
    {
        try {
            logger('========== Listener:: BillingAttemptSuccess ==========');
            $ids = $event->ids;
            $user = User::find($ids['user_id']);
            $shop = Shop::find($ids['shop_id']);
            $webhookResonse = SsWebhook::find($ids['webhook_id']);

            if ($webhookResonse) {
                $data = json_decode($webhookResonse->body);

//                update billing attempt
                $ssBillingAttempt = SsBillingAttempt::where('shop_id', $shop->id)->where('shopify_id', $data->id)->first();
                if ($ssBillingAttempt) {
                    $ssBillingAttempt->status = 'successful';
                    $ssBillingAttempt->completedAt = date('Y-m-d H:i:s');
                    $ssBillingAttempt->shopify_order_id = $data->order_id;
                    $ssBillingAttempt->updated_at = date('Y-m-d H') . ':01:' . date('s');
                    $ssBillingAttempt->save();
                }

//                update contract
                $ssContract = SsContract::where('shop_id', $shop->id)->where('shopify_contract_id', $data->subscription_contract_id)->first();
                if ($ssContract) {

//                    $nextDate = $this->getSubscriptionTimeDate(date("Y-m-d",
                    //                        strtotime($ssContract->next_order_date)), $shop->id);

                    $next_order_date = date("Y-m-d H:i:s", strtotime($ssContract->next_order_date . ' + ' . $ssContract->billing_interval_count . ' ' . strtolower($ssContract->billing_interval)));
//                    $next_order_date = date("Y-m-d $addtime", strtotime($ssContract->next_order_date . ' + ' . $ssContract->billing_interval_count . ' ' . strtolower($ssContract->billing_interval)));

                    if (isset($ssContract->delivery_cutoff) || isset($ssContract->billing_anchor_type)) {
                      $next_order_date = $this->calculateNextOrderDate($ssContract, $shop);
                    }

                    $ssContract->status_billing = 'successful';
                    $ssContract->error_state = null;
                    $ssContract->next_order_date = $next_order_date;
                    $ssContract->next_processing_date = $next_order_date;
                    $ssContract->last_billing_order_number = $data->order_id;
                    $ssContract->order_count = ($ssContract->order_count) ? $ssContract->order_count + 1 : 1;
                    $ssContract->failed_payment_count = 0;
                    $ssContract->save();

                    // update status to expired if maximum order count is reached

                    if ($ssContract->billing_max_cycles) {
                        if ($ssContract->order_count >= $ssContract->billing_max_cycles) {
                            $ssContract->status = 'expired';
                            $ssContract->save();

                            $result = $this->updateSubscriptionContract($shop->user_id, $ssContract->id);

                            $this->saveActivity($shop->user_id, $ssContract->ss_customer_id, $ssContract->id, 'system', 'This subscription reached the maximum number of orders and was set to Expired');
                        }
                    }

                    //update customer
                    $ssCustomer = SsCustomer::where('shop_id', $shop->id)->where('shopify_customer_id', $ssContract->shopify_customer_id)->first();
                    if ($ssCustomer) {
                        $sh_order = $this->getShopifyOrder($user, $data->order_id);
                        $ssCustomer->total_orders = $ssCustomer->total_orders + 1;
                        $ssCustomer->total_spend = (@$sh_order['total_price']) ? $ssCustomer->total_spend + $sh_order['total_price'] : '';
                        $ssCustomer->avg_order_value = preg_replace('/[^0-9_ .]/s', '', number_format(($ssCustomer->total_spend / $ssCustomer->total_orders), 2));
                        $ssCustomer->save();

                        // create order
                        $order = $this->createOrder($user->id, $shop->id, $data->order_id, $ssCustomer->id, $ssContract->id);

                        // Add order tags
                        logger('============== START:: AddOrderTags ===========');
                        $endPoint = 'admin/api/' . env('SHOPIFY_API_VERSION') . '/orders/' . $data->order_id . '.json';
                        $order_tags = SsSetting::where('shop_id', $shop->id)->first()->tag_order;

                        $parameter = [
                            'order' => [
                                'id' => $data->order_id,
                                'tags' => $order_tags,
                            ],
                        ];
                        $user->api()->rest('PUT', $endPoint, $parameter);
                        logger('============== END:: AddOrderTags ===========');
                    }

                    // update next_order_date in shopify
                    $result = $this->subscriptionContractSetNextBillingDate($shop->user_id, $ssContract->shopify_contract_id);
                }
            }
        } catch (\Exception $e) {
            logger('========== ERROR:: Listener:: BillingAttemptSuccess ==========');
            logger(json_encode($e));
            Bugsnag::notifyException($e);
        }
    }
}
