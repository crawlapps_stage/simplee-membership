<?php

namespace App\Listeners;

use App\Events\CheckBillingAttemptFailure;
use App\Models\Shop;
use App\Models\SsActivityLog;
use App\Models\SsBillingAttempt;
use App\Models\SsContract;
use App\Models\SsEmail;
use App\Models\SsSetting;
use App\Models\SsWebhook;
use App\Models\SsCustomer;
use App\Traits\ShopifyTrait;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

class BillingAttemptFailure
{
    use ShopifyTrait;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CheckBillingAttemptFailure  $event
     * @return void
     */
    public function handle(CheckBillingAttemptFailure $event)
    {
        try {
            logger('========== Listener:: BillingAttemptFailure ==========');
            $ids = $event->ids;
            $user = User::find($ids['user_id']);
            $shop = Shop::find($ids['shop_id']);
            $webhookResonse = SsWebhook::find($ids['webhook_id']);

            if ($webhookResonse) {
                $data = json_decode($webhookResonse->body);

                //update billing attempt
                $ssBillingAttempt = SsBillingAttempt::where('shop_id', $shop->id)->where('shopify_id', $data->id)->first();
                if ($ssBillingAttempt) {
                    $ssBillingAttempt->status = 'failed';
                    $ssBillingAttempt->errorMessage = $data->error_message;
                    $ssBillingAttempt->completedAt = date('Y-m-d H:i:s');
                    $ssBillingAttempt->save();
                }

                $setting = SsSetting::select('dunning_retries', 'dunning_daysbetween', 'dunning_failedaction', 'dunning_email_enabled', 'email_from_email', 'email_from_name')->where('shop_id', $shop->id)->first();
                if ($setting) {
                    $ssContract = SsContract::where('shop_id', $shop->id)->where('shopify_contract_id', $data->subscription_contract_id)->first();
                    $ssContract->status_billing = 'failed';
                    $contract_failed_payment_count = $ssContract->failed_payment_count;
                    $setting_dunning_retries = $setting->dunning_retries;


                    if ($contract_failed_payment_count == ($setting_dunning_retries - 1)) {
                        $ssContract->failed_payment_count = $ssContract->failed_payment_count + 1;

                        $ss_activity_log = new SsActivityLog;
                        $ss_activity_log->shop_id = $shop->id;
                        $ss_activity_log->user_id = $user->id;
                        $ss_activity_log->ss_contract_id = $ssContract->id;
                        $ss_activity_log->ss_customer_id = $ssContract->ss_customer_id;
                        $ss_activity_log->user_type = 'System';
                        $ss_activity_log->user_name = $shop->owner;
                        $ss_activity_log->user_name = $shop->owner;

                        if ($setting->dunning_failedaction == 'cancel') {
                            $ssContract->status = 'cancelled';
                            $ss_activity_log->message = "Subscription [contract #$data->subscription_contract_id] cancelled after reaching max number of failed billing attempts";
                        } elseif ($setting->dunning_failedaction == 'pause') {
                            $ssContract->status = 'paused';
                            $ss_activity_log->message = "Subscription [contract #$data->subscription_contract_id] paused after reaching max number of failed billing attempts";
                        } elseif ($setting->dunning_failedaction == 'skip') {
//                            $nextDate = $this->getSubscriptionTimeDate(date("Y-m-d", strtotime($ssContract->next_order_date)), $shop->id);

//                            $next_order_date = date("Y-m-d H:i:s", strtotime($nextDate . ' + ' . $ssContract->billing_interval_count . ' ' . strtolower($ssContract->billing_interval)));

                            $next_order_date = date('Y-m-d H:i:s', strtotime($ssContract->next_order_date . ' + ' . $ssContract->billing_interval_count . ' ' . strtolower($ssContract->billing_interval)));

                            $ssContract->next_order_date = $next_order_date;
                            $ssContract->next_processing_date = $next_order_date;

                            $ss_activity_log->message = "Subscription [contract #$data->subscription_contract_id] skipped an order after reaching max number of failed billing attempts";
                        }
                        $ssContract->save();
                        $ss_activity_log->save();

                        if ($setting->dunning_failedaction == 'skip') {
                            $result = $this->subscriptionContractSetNextBillingDate($shop->user_id, $ssContract->shopify_contract_id);
                        } else if ($setting->dunning_failedaction == 'cancel' || $setting->dunning_failedaction == 'pause') {
                            $result = $this->updateSubscriptionContract($shop->user_id, $ssContract->id);
                        }
                    } else if ($contract_failed_payment_count >= 0 && $contract_failed_payment_count < $setting_dunning_retries) {
                        $increaseDay = $setting->dunning_daysbetween + 1;
                        $ssContract->failed_payment_count = $ssContract->failed_payment_count + 1;
                        $ssContract->next_processing_date = date('Y-m-d H:i:s', strtotime($ssContract->next_processing_date . ' + ' . $increaseDay . ' day'));
                        $ssContract->save();

                        //send mail
                        if ($setting->dunning_email_enabled) {
                            $email = SsEmail::where('shop_id', $shop->id)->where('category', 'failed_payment_to_customer')->first();
                            $customer = SsCustomer::select('email')->where('shop_id', $shop->id)->where('shopify_customer_id', $ssContract->shopify_customer_id)->first();

                            $htmlBody = $email->html_body;
                            $newHtml = str_replace('[CARD_TYPE]', $ssContract->cc_brand, $htmlBody);
                            $newHtml = str_replace('[EXPIRY_DATE]', substr('0' . $ssContract->cc_expiryMonth, -2) . '/' . substr($ssContract->cc_expiryYear, 2), $newHtml);

                            $res = sendMailH($email->subject, $newHtml, $setting->email_from_email, $customer->email, $setting->email_from_name, $shop->id, $customer->id);
                            logger('============ Failed payment customer email ============');
                            logger(json_encode($res));
                        }
                    } else if (($contract_failed_payment_count >= $setting_dunning_retries) && $setting->dunning_failedaction == 'skip') {
                        $ssContract->failed_payment_count = $ssContract->failed_payment_count + 1;

                        $ss_activity_log = new SsActivityLog;
                        $ss_activity_log->shop_id = $shop->id;
                        $ss_activity_log->user_id = $user->id;
                        $ss_activity_log->ss_contract_id = $ssContract->id;
                        $ss_activity_log->ss_customer_id = $ssContract->ss_customer_id;
                        $ss_activity_log->user_type = 'System';
                        $ss_activity_log->user_name = $shop->owner;
                        $ss_activity_log->user_name = $shop->owner;

//                        $nextDate = $this->getSubscriptionTimeDate(date("Y-m-d", strtotime($ssContract->next_order_date)), $shop->id);

//                        $next_order_date = date('Y-m-d H:i:s', strtotime($nextDate . ' + ' . $ssContract->billing_interval_count . ' ' . strtolower($ssContract->billing_interval)));
                        $next_order_date = date('Y-m-d H:i:s', strtotime($ssContract->next_order_date . ' + ' . $ssContract->billing_interval_count . ' ' . strtolower($ssContract->billing_interval)));

                        $ssContract->next_order_date = $next_order_date;
                        $ssContract->next_processing_date = $next_order_date;

                        $ss_activity_log->message = "Subscription [contract #$data->subscription_contract_id] skipped an order after reaching max number of failed billing attempts";

                        $ssContract->save();
                        $ss_activity_log->save();

                        $result = $this->subscriptionContractSetNextBillingDate($shop->user_id, $ssContract->shopify_contract_id);
                    }
                }
            }
        } catch (\Exception $e) {
            logger('========== ERROR:: Listener:: BillingAttemptFailure ==========');
            logger(json_encode($e));
            Bugsnag::notifyException($e);
        }
    }
}
