<?php

namespace App\Listeners;

use App\Events\CheckProductUpdate;
use App\Models\Shop;
use App\Models\SsContractLineItem;
use App\Models\SsDeletedProduct;
use App\Models\SsWebhook;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ProductUpdate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CheckProductUpdate  $event
     * @return void
     */
    public function handle(CheckProductUpdate $event)
    {
        try{
            logger('========== START:: Listener:: ProductUpdate ==========');
            $ids = $event->ids;
            $user = User::find($ids['user_id']);
            $shop = Shop::find($ids['shop_id']);
            $webhookResonse = SsWebhook::find($ids['webhook_id']);

            $data = json_decode($webhookResonse->body);

            $variants = $data->variants;
            $sh_variantsId = [];
            if( !empty( $variants ) ){
                foreach ( $variants as $vkey=>$vval ){
                    $sh_variantsId[] = $vval->id;
                }
            }

            $products = SsContractLineItem::select('shopify_variant_id')->distinct()->where('user_id',  $user->id)->where('shopify_product_id', $data->id)->get();

            foreach ($products as $key => $value) {
                if( !in_array($value->shopify_variant_id, $sh_variantsId) ){
                    $cnt = SsContractLineItem::select('ss_contract_id')->distinct()->where('user_id',  $user->id)->where('shopify_product_id', $data->id)->where('shopify_variant_id', $value->shopify_variant_id)->count();
                    $deleted_product = new SsDeletedProduct;
                    $deleted_product->shop_id = $shop->id;
                    $deleted_product->user_id = $user->id;
                    $deleted_product->shopify_product_id =  $data->id;
                    $deleted_product->shopify_variant_id = $value->shopify_variant_id;
                    $deleted_product->subscriptions_impacted = $cnt;
                    $deleted_product->active = 1;
                    $deleted_product->save();
                }
            }
            logger('========== END:: Listener:: ProductUpdate ==========');
        }catch ( \Exception $e ){
            logger('========== ERROR:: Listener:: ProductUpdate ==========');
            logger(json_encode($e));
            Bugsnag::notifyException($e);
        }
    }
}
