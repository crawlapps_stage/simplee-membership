<?php

namespace App\Observers;

use App\Models\Install;
use App\Models\Shop;
use App\Models\SsSetting;
use App\User;
use Osiset\ShopifyApp\Storage\Models\Charge;
use Osiset\ShopifyApp\Storage\Models\Plan;

class UserObserver
{
    public function saving(User $user)
    {
        logger('============ START :: user observer ============');
        $old_plan = $user->getOriginal('plan_id');
        $new_plan = $user->plan_id;
//        logger('Old plan :: ' .$old_plan);
//        logger('new plan :: ' .$new_plan);
        if( $old_plan != $new_plan ){
            logger('user observer');
            $plan = Plan::find($user->plan_id);

            $shop = Shop::where('user_id', $user->id)->first();
            $setting = SsSetting::where('shop_id', $shop->id)->first();
            $setting->billing_plan_id = $user->plan_id;
            $setting->transaction_fee = ($plan) ? $plan->transaction_fee : 0.0025;
            $setting->save();

            if( ($old_plan == '' || is_null($old_plan)) && env('APP_ENV') == 'production'){
                $header = 'Content-Type: application/json';
                $url = 'https://maker.ifttt.com/trigger/notification/with/key/kUcdtO7EJKsd4COR7AcoJmZbRyVu3hkrP5t22xO91GW';
                $data = array(
                    'value1' => 'New Install',
                    'value2' => $shop->name.' installed with '.$plan->name.' plan',
                    'value3' => $shop->domain
                );
                triggerCURL($data, $header, $url);
            }
//
//            $charge = Charge::where('status', 'ACTIVE')->where('user_id', $shop->user_id)->first();
//            $install->billing_plan_id = $charge['plan_id'];
//            $install->recurring_charge_id = $charge['charge_id'];
//            $install->active = true;
//            $install->trial_expires = $charge['trial_ends_on'];
//            $install->billing_plan_current_cap = $charge['capped_amount'];
////            $install->transaction_fee_rate = $charge['capped_amount'];
//            $install->shopify_charge_status = $charge['status'];
//            $install->deleted = false;
//            $install->save();
        }
    }
}
