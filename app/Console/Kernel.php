<?php

namespace App\Console;

use App\Jobs\CalculateShopMetrics;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command('usage:charge')->dailyAt('10:00')->timezone('UTC');

         if( env('APP_ENV') == 'production' ){
             $schedule->command('exchange:rates')->hourly();
         }else{
             $schedule->command('exchange:rates')->daily();
         }

         $schedule->command('update:balance')->hourlyAt(30)->withoutOverlapping();
//          $schedule->command('exchange:rates')->cron('0 */1 * * *');

        $schedule->command('billing:attempt')->everyTenMinutes();

        // Dispatch the job for calculating installed shops' metrics
        $schedule->job(new CalculateShopMetrics)->dailyAt('00:15')->timezone('UTC');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
