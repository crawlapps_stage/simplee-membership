<!DOCTYPE html>
<html>
<head>
    <!-- Meta tag -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- style custom css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">

    <!-- Polaris css cdn -->
    <link rel="stylesheet" href="https://unpkg.com/@shopify/polaris@5.1.0/dist/styles.css"/>

    <!-- font-awesome link -->
    <script src="https://kit.fontawesome.com/7945acc650.js" crossorigin="anonymous"></script>

    <title>Subscriptions</title>
    @if(config('shopify-app.appbridge_enabled'))
        <script src="https://unpkg.com/@shopify/app-bridge"></script>
        <script>
            var AppBridge = window['app-bridge'];
            var createApp = AppBridge.default;

            window.shopify_app_bridge = createApp({
                apiKey: '{{ config('shopify-app.api_key') }}',
                shopOrigin: '{{ Auth::user()->name }}',
                forceRedirect: true,
            });
        </script>
    @endif
</head>

<body>
<div class="main">
    <div class="main-inner">
        <div class="plan-main">
            <main class="Polaris-Frame__Main" id="AppFrameMain" data-has-global-ribbon="false">
                <a id="AppFrameMainContent" tabindex="-1"></a>
                <div class="Polaris-Frame__Content">
                    <div class="Polaris-Page">
                        <div class="Polaris-Page__Content">
                            <div class="Polaris-Layout">
                                <div class="plan-main-inner">
                                    <div class="plan-top-header">
                                        <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">Choose the plan
                                            that’s right for you</h2>
                                        <h6>7 DAY TRIAL ON ALL PLANS</h6>
                                    </div>

                                    <div class="plan-bottom-box">
                                        <div class="row justify-content-center">
                                            <div class="col-lg-6 col-md-6 col-sm-12" style="height: 100%">
                                                <div class="plan-box-main">
                                                    <div class="plan-top-bar">
                                                        <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">
                                                            {{ $plan['data'][0]['name'] }}</h2>
                                                    </div>
                                                    <div class="plan-bottom-part">
                                                        <div class="plan-bottom-bar">
                                                            <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">
                                                                ${{$plan['data'][0]['price']}}</h2>
                                                            <h5>PER MONTH</h5>
                                                        </div>

                                                        <div class="plan-mid-bar">
                                                            <h5>+ ${{ number_format(($plan['data'][0]['transaction_fee'] * 100), 2) }}% PER MEMBER</h5>
                                                        </div>

                                                        <div class="plan-discription">
                                                            <ul>
                                                                <li>
                                                                    <div class="check-icon">
                                                                        <i class="fas fa-check"></i>
                                                                    </div>
                                                                    <span class="text-uppercase">WORKS WITH SHOPIFY PAYMENTS, AUTHORIZE.NET, PAYPAL EXPRESS
                                                                    </span>
                                                                </li>
                                                                <li>
                                                                    <div class="check-icon">
                                                                        <i class="fas fa-check"></i>
                                                                    </div>
                                                                    <span class="text-uppercase">SELL MEMBERSHIPS</span>
                                                                </li>

                                                                <li>
                                                                    <div class="check-icon">
                                                                        <i class="fas fa-check"></i>
                                                                    </div>
                                                                    <span class="text-uppercase">ADD CUSTOMER TAGS</span>
                                                                </li>

                                                                <li>
                                                                    <div class="check-icon">
                                                                        <i class="fas fa-check"></i>
                                                                    </div>
                                                                    <span class="text-uppercase">SHOW / HIDE STOREFRONT CONTENT</span>
                                                                </li>

                                                                <li>
                                                                    <div class="check-icon">
                                                                        <i class="fas fa-check"></i>
                                                                    </div>
                                                                    <span class="text-uppercase">DUNNING MANAGEMENT</span>
                                                                </li>
                                                            </ul>
                                                        </div>

                                                        <div class="plan-btn-main">
                                                            @if( $plan['active_plan_id'] == 2 || !$plan['active_plan_id'])
                                                                <a href="/billing/1" class="Polaris-Button">
																			<span class="Polaris-Button__Content">
																				<span class="Polaris-Button__Text">Start Free Trial</span>
																			</span>
                                                                </a>
                                                            @else
                                                                <button type="button" class="Polaris-Button"
                                                                        style="cursor: none;">
																			<span class="Polaris-Button__Content">
																				<span class="Polaris-Button__Text">Current Plan</span>
																			</span>
                                                                </button>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-sm-12" style="height: 100%">
                                                <div class="plan-box-main">
                                                    <div class="plan-top-bar">
                                                        <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">
                                                            {{ $plan['data'][1]['name'] }}</h2>
                                                    </div>

                                                    <div class="plan-bottom-part">
                                                        <div class="plan-bottom-bar">
                                                            <h2 class="Polaris-DisplayText Polaris-DisplayText--sizeMedium">
                                                                ${{$plan['data'][1]['price']}}</h2>
                                                            <h5>PER MONTH</h5>
                                                        </div>

                                                        <div class="plan-mid-bar">
                                                            <h5>+ ${{ number_format(($plan['data'][1]['transaction_fee'] * 100), 2) }}% PER MEMBER</h5>
                                                        </div>

                                                        <div class="plan-discription">
                                                            <ul>
                                                                <li>
                                                                    <div class="check-icon">
                                                                        <i class="fas fa-check"></i>
                                                                    </div>
                                                                    <span class="text-uppercase">SAME FEATURES AS STARTER PLAN</span>
                                                                </li>

                                                                <li>
                                                                    <div class="check-icon">
                                                                        <i class="fas fa-check"></i>
                                                                    </div>
                                                                    <span class="text-uppercase">BEST FOR STORES WITH MORE THAN 2,500 MEMBERS</span>
                                                                </li>
                                                            </ul>
                                                        </div>

                                                        <div class="plan-btn-main">
                                                            @if( $plan['active_plan_id'] == 1 || !$plan['active_plan_id'] )
                                                                <a href="/billing/2" class="Polaris-Button">
                                                                    <span class="Polaris-Button__Content">
                                                                        <span class="Polaris-Button__Text">Start Free Trial</span>
                                                                    </span>
                                                                </a>
                                                            @else
                                                                <button type="button" class="Polaris-Button"
                                                                        style="cursor: none;">
                                                                        <span class="Polaris-Button__Content">
                                                                            <span class="Polaris-Button__Text">Current Plan</span>
                                                                        </span>
                                                                </button>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="plan-info">
                                <h3>IMPORTANT:<span> YOU WILL BE ASKED TO ACCEPT A CAPPED MONTHLY AMOUNT. THIS IS REQUIRED FOR US TO CHARGE ADDITIONAL MEMBER FEES</span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
        </main>
    </div>
</div>
</div>

<!-- jQuery and bootstrap css -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<!-- jQuery and bootstrap css end-->
<script type="text/javascript">
    var simplee_membership = {
        init: function () {
            this.addEvent();
        },
        addEvent: function () {
            let url = '{{ route('event')}}';
            $.ajax({
                url: url,
                type: "post",
                data: {'_method': 'post', '_token': "{{ csrf_token() }}", 'category': 'Install', 'description': 'Billing page loaded'},
                success: function (data) {
                    console.log(data);
                },
            });
        },
    };

    $(document).ready(function(){
        simplee_membership.init();
    });
</script>
</body>

</html>
