<html>
<head class="subscription-app">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Subscription App') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- style custom css -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">

    <!-- Polaris css cdn -->
    <link rel="stylesheet" href="https://unpkg.com/@shopify/polaris@5.1.0/dist/styles.css"/>

    <!-- font-awesome link -->
    <script src="https://kit.fontawesome.com/7945acc650.js" crossorigin="anonymous"></script>

    <!-- Google Tag Manager
    <script>
        var gtm_tag = '{{ config('const.gtm_tag') }}';

        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});
            var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';
            j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer',gtm_tag);
    </script>
     End Google Tag Manager -->

<!-- Hotjar Tracking Code for https://subscriptions.simplee.best -->
    <script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:2341885,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>



    <script>
        //Set your APP_ID
        var APP_ID = "a7xla5ct";

        (function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/' + APP_ID;var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);};if(document.readyState==='complete'){l();}else if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();

        window.Intercom("boot", {
            app_id: APP_ID,
            user_id: <?php echo $current_user['user_id'] ?>, // id from users table
            user_hash: "<?php echo $current_user['hmac'] ?>", // HMAC using SHA-256
            name: <?php echo json_encode($current_user['name']) ?>, // Full name
            email: <?php echo json_encode($current_user['email']) ?>, // Email address
            created_at: "<?php echo strtotime($current_user['created_at']) ?>", // Signup date as a Unix timestamp
            "myshopify_domain": "<?php echo $current_user['myshopify_domain'] ?>",
            "simplee_shop_id": "<?php echo $current_user['simplee_shop_id'] ?>",
            "simplee_plans_created": "<?php echo $current_user['simplee_plans_created'] ?>",
            "simplee_subscriptions": "<?php echo $current_user['simplee_subscriptions'] ?>",
            "simplee_install_tried": "<?php echo $current_user['simplee_install_tried'] ?>",
            "simplee_plan": "<?php echo $current_user['simplee_plan'] ?>"
        });
    </script>

    @if(config('shopify-app.appbridge_enabled'))
        <script src="https://unpkg.com/@shopify/app-bridge"></script>
        <script>
            var AppBridge = window['app-bridge'];
            var createApp = AppBridge.default;

            window.shopify_app_bridge = createApp({
                apiKey: '{{ config('shopify-app.api_key') }}',
                shopOrigin: '{{ Auth::user()->name }}',
                forceRedirect: true,
            });
        </script>
    @endif
    <script>
        window.contractID = {{$id}};
    </script>
</head>
<body class="">

<!-- Google Tag Manager (noscript)
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id={{config('const.gtm_tag')}}"
            height="0" width="0" style="display:none;visibility:hidden">
    </iframe>
</noscript>
End Google Tag Manager (noscript) -->

<div id="app">
    <router-view></router-view>
</div>
</body>

<!--     select 2 link -->
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script src="{{  asset(mix('js/app.js'))  }}"></script>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
</html>

