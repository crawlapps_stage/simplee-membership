<html>
<head></head>
<body>
<div class="container" style="width: 20%;margin: auto;">
	<div>Verifying customer</div>
	<div><p>Please wait...</p></div>
</div>

<script type="text/javascript">

</script>
<!-- jQuery and bootstrap css -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
  <script src="{{ asset('js/portal.js') }}" defer></script>
<script src="{{ asset('js/portal.js') }}" defer></script>

<!-- jQuery and bootstrap css end-->
<script type="text/javascript">
    var simplee_subscription = {
        init: function () {
            this.getSubscriber();
        },
        getSubscriber: function () {
            let CustomerID = sessionStorage.getItem('X-shopify-customer-ID');
            let shop = '{{ $data['shop'] }}'

            let url = '{{ route('portalsubscriber')}}';
            $.ajax({
                url: url,
                type: "post",
                data: {'_method': 'post', '_token': "{{ csrf_token() }}", 'CustomerID': CustomerID, 'shop': shop},
                success: function (data) {
                	var main = document.getElementsByTagName("main")[0];
                    main.innerHTML = data;
                },
            });
        },
    };

    $(document).ready(function(){
        simplee_subscription.init();
    });
</script>
</body>
</html>
