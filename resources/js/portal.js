const host = process.env.MIX_APP_URL;
const apiEndPoint = host + '/api';

var self = "";
var crawlapps_simplee = {
    init: function () {
         this.subscription();   
    },
    subscription: function(id, type){
        $(document).on('click','.contract_status',function(){
            let id = $(this).data('id');
            let custid = $(this).data('custid');
            let type = $(this).data('type');
            let shopifyDomain = $(this).data('shop');

            let data = {
                 contract_id: id,
                 type: type,
                 customer_id: custid,  
                 shop: shopifyDomain,
                 mode: 'api',
                 ship_add1: $('#new_ship_add').val(),
            };

            let aPIEndPoint = `${apiEndPoint}/subscriber`;
            console.log(aPIEndPoint);
                $.ajax({
                    method: "post",
                    url: aPIEndPoint,
                    data: {data: data},
                    success:function (response,success,header) {
                        console.log(response);
                        // if( response.data ){
                            window.location.reload();
                        // }
                    },
                });

            });

        $('#edit-ship_add').on('click', function () {
            let custid = $(this).data('custid');
            let contract_id = $(this).data('id');
            $('#ship_add_save').data('custid', custid );
            $('#ship_add_save').data('id', contract_id);
            let add = $(this).data('add');
          $('#new_ship_add').val(add);
        })
    }
}
$(document).ready(function () {
    crawlapps_simplee.init();
});