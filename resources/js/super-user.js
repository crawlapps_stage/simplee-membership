window.Vue = require('vue');

import VTooltip from 'v-tooltip';
import PolarisVue from '@eastsideco/polaris-vue';
import '@eastsideco/polaris-vue/lib/polaris-vue.css';

Vue.use(VTooltip);
Vue.use(PolarisVue);

import helper from './helper';
const plugin = {
    install() {
        Vue.prototype.$helpers = helper
    }
}
Vue.use(plugin);

Vue.component('SuperUser', require('./components/pages/user/SuperUser.vue').default);

const appPortal = new Vue({
    el: '#superuser'
});
