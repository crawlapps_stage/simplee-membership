require('./bootstrap');
window.Vue = require('vue');

import VTooltip from 'v-tooltip';


Vue.use(VTooltip)
import helper from './helper';
const plugin = {
    install() {
        Vue.prototype.$helpers = helper
    }
}
Vue.use(plugin);

// plugin toast
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-default.css';
Vue.use(VueToast);

Vue.component('index', require('./components/pages/portal/index.vue').default);


const appPortal = new Vue({
    el: '#portal'
});
