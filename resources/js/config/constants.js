

export const csvMapFields = [
    "customer_gateway_id", 
    "customer_shopify_id", 
    "customer_email", 
    "customer_firstname", 
    "customer_lastname", 
    "subscription_number", 
    // "next_billing_date", 
    // "currency_code", 
    "subscription_status", 
    "billing_interval_type", 
    "billing_interval_count", 
    "delivery_interval_type", 
    "delivery_interval_count", 
    "is_prepaid", 
    "prepaid_orders_completed", 
    "prepaid_renew", 
    "min_cycles", 
    "max_cycles", 
    "order_count", 
    "shipping_firstname", 
    "shipping_lastname", 
    "shipping_address1", 
    "shipping_address2", 
    "shipping_city", 
    "shipping_state", 
    "shipping_country_code", 
    "shipping_zip", 
    "shipping_price", 
    "line_item_qty", 
    "line_item_price", 
    "line_item_product_id", 
    "line_item_variant_id"
];

export const migrationResourceList = [
    {
        url: '#',
        attributeOne: 'Stripe Gateway Connected?',
        attributeTwo: 'Yes'
    },
    {
        url: '#',
        attributeOne: 'Migration In Progress?',
        attributeTwo: 'Yes'
    },
    {
        url: '#',
        attributeOne: 'Total # of Rows?',
        attributeTwo: '598'
    },
    {
        url: '#',
        attributeOne: 'Total # of Unique Contracts',
        attributeTwo: '385'
    },
    {
        url: '#',
        attributeOne: 'All Customers on Shopify?',
        attributeTwo: 'Yes'
    }
];