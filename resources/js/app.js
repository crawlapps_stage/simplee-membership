require('./bootstrap');
window.Vue = require('vue');

import App from './components/layouts';
import VTooltip from 'v-tooltip';

Vue.use(VTooltip)
import Bugsnag from '@bugsnag/js'
import BugsnagPluginVue from '@bugsnag/plugin-vue'

Bugsnag.start({
    apiKey: '8c4c71c5479ac9ce80c7d370eaaee278',
    plugins: [new BugsnagPluginVue()]
})
Bugsnag.notify(new Error('Test error'))
import router from './routes';
import helper from './helper';
const plugin = {
    install() {
        Vue.prototype.$helpers = helper
    }
}
Vue.use(plugin);

// setInterval(() => {
//     axios.get('/keep-alive')
//         .then(() => {})
//         .catch(() => {})
// }, 600000)

const app = new Vue({
    el: '#app',
    router,
    render: h => h(App),
});
