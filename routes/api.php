<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['cors']], function () {
	Route::group(['namespace' => 'Api'], function () {
		Route::post('subscriber', 'PortalController@index');
		Route::post('subscriber/store', 'PortalController@store');

        // product subscription extension routes
        Route::group(['middleware' => ['extension.auth']], function () {
       		Route::get('plan-groups', 'ExtensionController@planGroups');
       	    Route::post('get-plan', 'ExtensionController@getPlan');
            Route::post('create', 'ExtensionController@createPlan');
            Route::post('remove', 'ExtensionController@removeProduct');
            Route::post('add', 'ExtensionController@add');
       });
	});
	Route::group(['namespace' => 'Subscriber'], function () {
		Route::get('country/{country?}', 'SubscriberController@getCountry')->name('country');
	});
});
 // webhook call from aws
  Route::post('/webhooks', 'Api\WebhookController@index')->name('aws-webhooks');