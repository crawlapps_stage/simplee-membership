<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Dashboard\DashboardController@appBladeindex')->middleware(['check.plan'])->name('home');
//Route::get('/', 'Dashboard\DashboardController@appBladeindex')->middleware(['auth.shopify', 'check.plan'])->name('home');

Route::get('/subscriptions', 'Dashboard\DashboardController@subscriptionsBladeindex')->middleware(['auth.shopify', 'billable']);

Route::get('/extension-plan', 'Dashboard\DashboardController@plansBladeindex')->middleware(['auth.shopify', 'billable']);


Route::group(['namespace' => 'Auth'], function () {
    Route::get('/login', 'AuthController@index')->name('login');
//    Route::match(
//        ['get', 'post'],
//        '/authenticate',
//        'AuthController@authenticate'
//    )->name('authenticate');
});

Route::get('/keep-alive', function () {
    return response()->json(['ok' => true]);
})->middleware('auth');

Route::group(['middleware' => 'auth.shopify', 'namespace' => 'Plan'], function () {
    Route::get('/app-plan', 'PlanController@appPlanIndex')->name('app-plan');
});

Route::group(['middleware' => 'shopifysession.auth', 'namespace' => 'Plan'], function () {
    Route::get('/mbilling/{plan?}', 'PlanController@appPlanChange')->name('mbilling');
});

//change plan
Route::get('change-plan-db/{id?}', 'Plan\PlanController@changePlanDB')->name('change-plan-db');

//event
Route::post('/event', 'Plan\PlanController@addEvent')->name('event');

Route::group(['middleware' => ['shopifysession.auth', 'check.plan']], function () {

    //dashboard
    Route::group(['namespace' => 'Dashboard'], function () {
        Route::resource('dashboard', 'DashboardController');
        Route::post('replace-lineitems', 'DashboardController@replaceLineItem')->name('replace-lineitems');

    });

    //subscriber
    Route::group(['namespace' => 'Subscriber'], function () {
        Route::resource('subscriber', 'SubscriberController');
        Route::get('subscribers/export/{type}/{s?}', 'SubscriberController@export');
        Route::post('save-comment', 'SubscriberController@saveComment')->name('save-comment');
        Route::post('save-lineitems', 'SubscriberController@saveLineItem')->name('save-lineitems');
        Route::get('country/{country?}', 'SubscriberController@getCountry')->name('country');
    });

    //plan
    Route::group(['namespace' => 'Plan'], function () {
        //plan group
        Route::get('/plan-group', 'PlanController@planGroupIndex')->name('plan-group');
        Route::post('/plan-group', 'PlanController@planGroupStore')->name('plan-group');
        Route::get('/plan-group.edit/{id?}', 'PlanController@planGroupEdit')->name('plan-group.edit');
        Route::delete('/plan-group.delete/{id?}', 'PlanController@planGroupDestroy')->name('plan-group.delete');

        // plan
        Route::post('/plan', 'PlanController@planStore')->name('plan.store');
        Route::get('/plan.edit/{planGid?}/{id?}', 'PlanController@planEdit')->name('plan.edit');
        Route::delete('/plan.delete/{id?}', 'PlanController@planDestroy')->name('plan.delete');

        //product
        Route::post('/assign-product', 'PlanController@assignProduct')->name('assign-product');

        //position
        Route::post('/position', 'PlanController@position')->name('position');
    });

    Route::get('/test', 'Test\TestController@test');

    //settings
    Route::group(['namespace' => 'Setting'], function () {
        Route::resource('setting', 'SettingController');

        //mail
        Route::post('/mail', 'SettingController@sendMail')->name('mail');
        Route::post('/email-body', 'SettingController@emailBody')->name('email-body');

        //images
        Route::post('/upload-image', 'SettingController@uploadImage')->name('upload-image');

        //customer portal
        Route::get('/portal-status', 'SettingController@changePortal')->name('portal-status');

        //theme install
        Route::post('/install-theme', 'SettingController@installTheme')->name('install-theme');
    });

    //Installations
    Route::group(['namespace' => 'Installation'], function () {
        Route::get('installation', 'InstallationController@index');
        Route::post('install-widget', 'InstallationController@installWidget');
    });

    //Shipping
    Route::group(['namespace' => 'Shipping'], function () {
        Route::resource('shipping', 'ShippingController');
    });
});

//feature
Route::group(['namespace' => 'Feature', 'prefix' => 'feature'], function () {
    Route::get('/add/{name}', 'FeatureController@add');
    Route::get('/enable/{name}/{id}', 'FeatureController@enableFor');
    Route::get('/disable/{name}/{id}', 'FeatureController@disableFor');
    Route::get('/remove/{name}/{id}', 'FeatureController@remove');
});

//proxy

Route::group(['prefix' => 'portal', 'namespace' => 'Portal', 'middleware' => 'proxy.auth'], function () {
    Route::get('/', 'PortalController@index');
});

//super user

Route::get('/super-user', 'User\SuperUserController@index')->name('super-user');
Route::group(['prefix' => 'superuser', 'namespace' => 'User'], function () {
    Route::get('get-shop', 'SuperUserController@getShop');
    Route::post('upload-csv', 'SuperUserController@uploadCsv');
});

Route::get('/test', 'Test\TestController@test');

//flush session
Route::get('flush', function(){
    request()->session()->flush();
});

